import time
import threading
import ctypes

from multiprocessing.pool import ThreadPool


class test:

    def __init__(self):
        self.pool = ThreadPool()
        self.pool.apply_async(self.fn)
        self.id = 0


    def fn(self):

        try:
            self.id = threading.get_ident()
            print(self.id)

            count = 0
            while True:
                print(count)
                count += 1                
                time.sleep(1)

        except Exception as e:
            print("EXCEPTION - ", e)


    def kill(self):

        print("KILLING")
        __id = self.pool.current_thread().ident
        ctypes.pythonapi.PyThreadState_SetAsyncExc(
            ctypes.c_long(__id),
            ctypes.py_object(SystemExit)
        )


if __name__ == '__main__':
    obj = test()
    time.sleep(5)
    obj.kill()