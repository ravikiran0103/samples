from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot
from Security.Security import Security  


class HomePresenter(QObject):
    def __init__(self, security):
        QObject.__init__(self)
        self.security = security

    liftMoved = pyqtSignal(bool, arguments=['direction'])
    currentUserChanged = pyqtSignal(str, arguments=['UserName'])
    
    @pyqtSlot()
    def liftUpClicked(self):
        print("Lift up clicked")
        self.liftMoved.emit(True)

    @pyqtSlot()
    def liftDownClicked(self):
        print("Lift down clicked")
        self.liftMoved.emit(False)
  

    @pyqtSlot(str)
    def onPinAvailable(self, pin):
        print("onPinAvailable", pin)
        self.security.ValidatePin(pin.encode())     

    @pyqtSlot(str)
    def onUserChanged(self, userName):
        if(userName != None and userName != ""):
            print("UserChanged", userName)
            self.currentUserChanged.emit(userName)
        