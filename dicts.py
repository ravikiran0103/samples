test = [
	{
		"queueid": "33",
		"recordid": "12132",
		"syncid": "203"
	},
	{
		"queueid": "34",
		"recordid": "1sdf32",
		"syncid": "203"
	}
]

def insert(list_of_dicts):
	for item in list_of_dicts:
		if item not in test:
			test.append(item.copy())
			print(test)

insert(
	[
		{
			"queueid": "33",
			"recordid": "12132",
			"syncid": "203"
		},
		{
			"queueid": "33",
			"recordid": "12133",
			"syncid": "203"
		},
	]
)

print("######################")
print(test.pop())