import serial

ser = serial.Serial(
    port='/dev/ttyUSB1',
    baudrate=115200
)

print("connected to: " + ser.portstr)
count=1

while True:
    
	line = ser.readline()
	print(str(count) + str(': ') + chr(line))
	count = count+1

ser.close()