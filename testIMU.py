from ctypes import cdll
import time
import ctypes
lib_nm = cdll.LoadLibrary('/home/root/libcarelink.so')
imu_data = lib_nm.getIMUAccelData

imu_data.restype = ctypes.c_int
x_data =  ctypes.c_float()
y_data =  ctypes.c_float()
z_data =  ctypes.c_float()

while(1): 
	imu_data(ctypes.byref(x_data),ctypes.byref(y_data),ctypes.byref(z_data))
	
	x_str =str("%.2f" % round(x_data.value,2))
	y_str =str("%.2f" % round(y_data.value,2))
	z_str =str("%.2f" % round(z_data.value,2))

	print("x=",x_str," y=",y_str,"z=",z_str)
	time.sleep(1);

