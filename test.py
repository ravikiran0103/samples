from datetime import datetime
import math
from Crypto.Cipher import AES
import binascii
import decimal

class Celsius:
	def __init__(self, temp):
		self._temperature = temp

	def to_fahrenheit(self):
		return (self.temperature * 1.8) + 32

	def get_temperature(self):
		print("Getting value")
		return self._temperature

	def set_temperature(self, value):
		if value < -273:
			raise ValueError("Temperature below -273 is not possible")
		print("Setting value")
		self._temperature = value

	def log(self, test,):
		print("##############")
		print(test)
		print("##############")
		print(datetime(2000, 1, 1))
		print(bytes([100]))
		print(datetime.utcnow())
		print(min(50, 100))

		if True:
			pass
	
	temperature = property(get_temperature, set_temperature)

def convert():
	time = datetime.strptime("1/1/2000", "%m/%d/%Y")
	print(time)
	print(time.strftime("%Y-%m-%d %H:%M:%S %p"))
	if time > datetime(2000, 1, 1):
		print("time is greater")
	elif time == datetime(2000, 1, 1):
		print("time is equal")
	else:
		print("time is lesser")

KEY = binascii.unhexlify('BE0135F28F01AE69C608A869D13812E1')
IV = binascii.unhexlify('BBD72E890181D83E0049BB32A95A2CB0')

def test_decrypted_pin():
	element="35AAF7342AB17AD529CB1BF04BA4F1EF"
	rijnd = AES.new(KEY, AES.MODE_CBC, IV)
	pt = rijnd.decrypt(binascii.unhexlify(element))
	pin = pt.decode('utf-8').rstrip()
	assert pin == "1234"

def get_valid_date():
	date_from_nsight = "/Date(1554958872835+0530)/"
	# date_from_nsight = "/Date(1554958872835)/"
	# date_from_nsight = "/Date(15549588)/"

	if "+" in date_from_nsight:
		last_index = date_from_nsight.rfind("+")
	else:
		last_index = date_from_nsight.rfind(")")

	first_index = date_from_nsight.find("(") + 1

	print(date_from_nsight[first_index:last_index])
	print(len(date_from_nsight[first_index:last_index]))
	return date_from_nsight[first_index:last_index]

def crop():
	version = "3.5.3"

	last_index = version.rfind(".")

	version = version[:last_index] + "" + version[last_index + 1:]

	num = "2.10"
	flt = float(num)
	flt = format(flt, '.2f')
	print(flt)


def flip(c):
	if c == '0':
		return '1'
	else:
		return '0'


def get_checksome():
	before = chr(2) + "p"
	pin = "00023342"
	after = "0010101" + chr(3)

	actual = before + pin + after
#	actual = "\x02p111111110010101\x03AF"
	
	print("BEFORE " + before)
	print("PIN " + pin)
	print("AFTER " + after)

	sum = 0
	for x in range(0, len(actual)):
	    sum += ord(actual[x])

	print("SUM = ", sum)

	binary = bin(sum).replace("0b","")
	print("BINARY = ", binary)

	if len(binary) < 8:
		pad = 8 - len(binary)
		binary = ('0' * pad) + binary

	ones = ""
	for x in range(0, len(binary)):
		ones += flip(binary[x])

	print("ONES COMPLIMENT = ", ones)
	decimal = int(ones, 2)
	print("DECIMAL = ", decimal)
	ck = hex(decimal)
	print("checksum = ", ck)
	return ck


def decrypt():
	rijnd = AES.new(KEY, AES.MODE_CBC, IV)
	pt = rijnd.decrypt(binascii.unhexlify("E2476FA243D3A9030968BD7BBE20C0DD"))
	pin = pt.decode('utf-8')
	print(pin)


if __name__ == "__main__":

	#number = "2.0499999999999998"
	# length = len(number) - number.find(".") - 1
	# float_val = float(number)
	# number = round(float_val, length)
	# print("ROUNDED")
	# length = len(number) - number.find(".") - 1
	# float_val = float(number)
	#number = round(float_val, length)
	#number = str(number)
	#print(number)
	test = ""
	if not test:
	    print("EMPTY")
	    
	test = None
	if not test:
	    print("NONE")	   

    #thisdict =	{}
#      1: "Ford",
#      2: "Mustang",
#      3: 1964
#    }
    #thisdict[1] = "Ford"
    #thisdict[2] = "Mustang"
    #thisdict[3] = "1964"
    #print(thisdict)
    #print(type(thisdict))
    #print(len(thisdict))

	# c = Celsius(10)
	# print(c._temperature)
	# c1 = Celsius(5)
	# print(c1._temperature)
	# print(c._temperature)
	# print(c.temperature)
	# c.temperature = 100000	
	# print(c.temperature)
	# convert()
	# test_decrypted_pin()
	# get_valid_date()
	# crop()
    # get_checksome()
    #decrypt()

	# c.log(f"the current temperature is {c.temperature}.")
