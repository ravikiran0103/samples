import json
from PyQt5.QtQml import QJSValue

# test = '[{ \
# 	"comment": "SDFSDFSDf", \
# 	"receiverName": "request_type_cbx.currentText", \
# 	"requestType": 0 \
# }]'
# data = json.loads(test)

test = {
	"comment": "SDFSDFSDf",
	"receiverName": "request_type_cbx.currentText",
	"requestType": 0
}
data = json.dumps(test)

# test = '[{ \
# 	"comment": "SDFSDFSDf", \
# 	"receiverName": "request_type_cbx.currentText", \
# 	"requestType": 0 \
# }]'
# data = json.parse(test)

print(data, type(data))

jsval = QJSValue(data)
print(jsval, type(jsval))

variant = jsval.toVariant()
print(variant, type(variant))