import QtQuick 2.12
import QtQuick.Window 2.12

Window {

    id: mainWnd

    visible: true
    width: 640
    height: 480

    color: "#005c99"
    property string font: "Courier"

    property int batteryLevel: 0

    Item {

        id: name

        anchors.fill: parent

        SummaryTape {

            id: summaryTape

            tapeWidth: parent.width
            tapeHeight: parent.height

            fontFamily: "arial"
            fontSize: 10

            valueTapeText: "THIS IS A SAMPLE TEST \n 2 \n 3 \n 4 \n 5 \n 6 \n 7 \n 8 \n 9 \n 10 \n 11 \n 12 \n 13 \n 14 \n 15 \n 16 \n 17"
            valueTapeMaxLineCount: 10

            signTapeText: "TEST"
            signTapeMaxLineCount: 10

            Component.onCompleted: {

                console.log("value tape line count = " + valueTapeLineCount)
                console.log("sign tape line count = " + signTapeLineCount)
            }
        }
    }

//    // BATTERY INDICATOR COMPONENT
//    Timer {

//        id: batteryLevelUpdateTimer

//        interval: 300
//        running: true
//        repeat: true
//        onTriggered: {

//            batteryLevel++
//            if (batteryLevel > 100) {

//                batteryLevel = 0
//            }

//            mainWnd.update()
//        }
//    }

//    BatteryIndicator  {

//        id: powerIndicator
//    }

//    function update() {

//        powerIndicator.currentValue = batteryLevel
//        powerIndicator.text = batteryLevel.toString() + "%"
//    }

//    function delay(duration) {

//        var timeStart = new Date().getTime();

//        while (new Date().getTime() - timeStart < duration) {
//            // Do nothing
//        }
//    }
}
