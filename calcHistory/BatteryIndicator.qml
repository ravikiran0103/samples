import QtQml 2.2
import QtQuick 2.0

Canvas {

    id: canvas

    width: parent.width
    height: parent.height
    antialiasing: true

    property string fontFamily: parent.font

    property color primaryColor: "#66c2ff"
    property color secondaryColor: "white"

    property real centerWidth: width / 2
    property real centerHeight: height / 2
    property real radius: Math.min(canvas.width, canvas.height) / 2 - 15

    property real minimumValue: 0
    property real maximumValue: 100
    property real currentValue: 90

    // this is the angle that splits the circle in two arcs
    // first arc is drawn from 0 radians to angle radians
    // second arc is angle radians to 2*PI radians
    property real angle: (currentValue - minimumValue) / (maximumValue - minimumValue) * 2 * Math.PI

    // we want both circle to start / end at 12 o'clock
    // without this offset we would start / end at 9 o'clock
    property real angleOffset: -Math.PI / 2

    property string text: "90%"

    onPrimaryColorChanged: {

        requestPaint()
    }

    onSecondaryColorChanged: {

        requestPaint()
    }

    onMinimumValueChanged: {

        requestPaint()
    }

    onMaximumValueChanged: {

        requestPaint()
    }

    onCurrentValueChanged: {

        requestPaint()
    }

    onPaint: {

        var ctx = getContext("2d");
        ctx.save();

        ctx.clearRect(0, 0, canvas.width, canvas.height);

        // First, thinner arc
        // From angle to 2*PI
        ctx.beginPath();
        ctx.lineWidth = canvas.width * 0.03;
        ctx.strokeStyle = secondaryColor;
        ctx.arc(canvas.centerWidth,
                canvas.centerHeight,
                canvas.radius,
                angleOffset + canvas.angle,
                angleOffset + 2*Math.PI);
        ctx.stroke();


        // Second, thicker arc
        // From 0 to angle
        ctx.beginPath();
        ctx.lineWidth = canvas.width * 0.03;;
        ctx.strokeStyle = canvas.primaryColor;
        ctx.arc(canvas.centerWidth,
                canvas.centerHeight,
                canvas.radius,
                canvas.angleOffset,
                canvas.angleOffset + canvas.angle);
        ctx.stroke();
    }

    Text {

        anchors.centerIn: canvas

        text: canvas.text

        font.family: canvas.fontFamily
        font.pixelSize: canvas.height * 0.3
        color: canvas.secondaryColor
    }
}
