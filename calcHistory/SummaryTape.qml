import QtQuick 2.12
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Rectangle {

    id: summaryTape

    width: parent.width
    height: parent.height

    property alias tapeWidth: summaryTape.width
    property alias tapeHeight: summaryTape.height

    property alias valueTapeText: valueTape.text
    property alias valueTapeLineCount: valueTape.lineCount
    property alias valueTapeMaxLineCount: valueTape.maximumLineCount

    property alias signTapeText: signTape.text
    property alias signTapeLineCount: signTape.lineCount
    property alias signTapeMaxLineCount: signTape.maximumLineCount

    property string fontFamily: "arial"
    property double fontSize: height * 0.03

    Text {

        id: valueTape

        x: 0
        y: 0
        width: summaryTape.width * 0.9
        height: summaryTape.height

        text: qsTr("value")
        font {

            family: summaryTape.fontFamily
            pixelSize: summaryTape.fontSize
        }
        verticalAlignment: Text.AlignBottom
        horizontalAlignment: Text.AlignRight
    }

    Text {

        id: signTape

        x: summaryTape.width * 0.9
        y: 0
        width: summaryTape.width * 0.1
        height: summaryTape.height

        text: qsTr("sign")
        font{

            family: summaryTape.fontFamily
            pixelSize: summaryTape.fontSize
        }
        verticalAlignment: Text.AlignBottom
        horizontalAlignment: Text.AlignHCenter
    }
}
