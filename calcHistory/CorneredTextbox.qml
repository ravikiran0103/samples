import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

TextField {

    id: corneredTextbox

    property alias x: corneredTextbox.x
    property alias y: corneredTextbox.y
    property alias width: corneredTextbox.width
    property alias height: corneredTextbox.height

    property alias text: corneredTextbox.text

}
