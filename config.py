import os
from configparser import ConfigParser

from enum import IntEnum, unique, Enum

configfile_name = "last_net_config.ini"

@unique
class NetworkConfig(Enum):

    WiredConfig = "WiredConfig"
    WirelessConfig = "WirelessConfig"
    DHCP = "DHCPEnable"
    IP_Address = "IP"
    Gateway = "Gateway"
    Netmask = "SubNetmask"
    PrimaryDNS = "PrimaryDNS"
    SecondaryDNS = "SecondaryDNS"


os.remove(configfile_name)
# Check if there is already a configurtion file
if not os.path.isfile(configfile_name):
	
	# Create the configuration file as it doesn't exist yet
	cfg_file = open(configfile_name, 'w')

	# Add content to the file
	config = ConfigParser()
	config.optionxform = str

	# Add content to the file
	# WIRED
	config.add_section(NetworkConfig.WiredConfig.value)
	config[NetworkConfig.WiredConfig.value] = {
		NetworkConfig.DHCP.value: '1',
		NetworkConfig.IP_Address.value: '0.0.0.0',
		NetworkConfig.Gateway.value: '0.0.0.0',
		NetworkConfig.Netmask.value: '0.0.0.0',
		NetworkConfig.PrimaryDNS.value: '0.0.0.0',
		NetworkConfig.SecondaryDNS.value: '0.0.0.0',
	}

	# WIRELESS
	config.add_section(NetworkConfig.WirelessConfig.value)
	config[NetworkConfig.WirelessConfig.value] = {
		NetworkConfig.DHCP.value: '1',
		NetworkConfig.IP_Address.value: '0.0.0.0',
		NetworkConfig.Gateway.value: '0.0.0.0',
		NetworkConfig.Netmask.value: '0.0.0.0',
		NetworkConfig.PrimaryDNS.value: '0.0.0.0',
		NetworkConfig.SecondaryDNS.value: '0.0.0.0',
	}

	# # WIRED
	# config.add_section(NetworkConfig.WiredConfig.value)
	# config.set(
	# 	NetworkConfig.WiredConfig.value,
	# 	NetworkConfig.DHCP.value,
	# 	'1'
	# )
	# config.set(
	# 	NetworkConfig.WiredConfig.value,
	# 	NetworkConfig.IP_Address.value,
	# 	'0.0.0.0'
	# )
	# config.set(
	# 	NetworkConfig.WiredConfig.value,
	# 	NetworkConfig.Gateway.value,
	# 	'0.0.0.0'
	# )
	# config.set(
	# 	NetworkConfig.WiredConfig.value,
	# 	NetworkConfig.Netmask.value,
	# 	'0.0.0.0'
	# )
	# config.set(
	# 	NetworkConfig.WiredConfig.value,
	# 	NetworkConfig.PrimaryDNS.value,
	# 	'0.0.0.0'
	# )
	# config.set(
	# 	NetworkConfig.WiredConfig.value,
	# 	NetworkConfig.SecondaryDNS.value,
	# 	'0.0.0.0'
	# )

	# # WIRELESS
	# config.add_section(NetworkConfig.WirelessConfig.value)
	# config.set(
	# 	NetworkConfig.WirelessConfig.value,
	# 	NetworkConfig.DHCP.value,
	# 	'1'
	# )
	# config.set(
	# 	NetworkConfig.WirelessConfig.value,
	# 	NetworkConfig.IP_Address.value,
	# 	'0.0.0.0'
	# )
	# config.set(
	# 	NetworkConfig.WirelessConfig.value,
	# 	NetworkConfig.Gateway.value,
	# 	'0.0.0.0'
	# )
	# config.set(
	# 	NetworkConfig.WirelessConfig.value,
	# 	NetworkConfig.Netmask.value,
	# 	'0.0.0.0'
	# )
	# config.set(
	# 	NetworkConfig.WirelessConfig.value,
	# 	NetworkConfig.PrimaryDNS.value,
	# 	'0.0.0.0'
	# )
	# config.set(
	# 	NetworkConfig.WirelessConfig.value,
	# 	NetworkConfig.SecondaryDNS.value,
	# 	'0.0.0.0'
	# )

	config.write(cfg_file)
	cfg_file.close()
