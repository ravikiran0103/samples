from data_context import DataContext
from PyQt5.QtCore import QObject, QTimer, pyqtSignal, pyqtSlot, QThread
from PyQt5.QtGui import QGuiApplication
import sys
import os,signal
from repo1 import UserRepo
from repo2 import SettingRepo
from PyQt5 import QtNetwork
import signal
import atexit
import socket

class SignalWakeupHandler(QtNetwork.QAbstractSocket):
    
    def __init__(self, parent=None):
        super().__init__(QtNetwork.QAbstractSocket.UdpSocket, parent)
        self.old_fd = None
        self.wsock, self.rsock = socket.socketpair(type=socket.SOCK_DGRAM)
        self.setSocketDescriptor(self.rsock.fileno())
        self.wsock.setblocking(False)
        self.old_fd = signal.set_wakeup_fd(self.wsock.fileno())

        self.readyRead.connect(lambda : None)
        self.readyRead.connect(self._readSignal)

    def __del__(self):
        if self.old_fd is not None and signal and signal.set_wakeup_fd:
            signal.set_wakeup_fd(self.old_fd)

    def _readSignal(self):
        data = self.readData(1)
        self.signalReceived.emit(data[0])

    signalReceived = pyqtSignal(int)

class SampleThread_1(QThread):
    def __init__(self,data_context):
        QThread.__init__(self)  
        self.data_context = data_context
        self.start()
    def run(self):
        user_repo = UserRepo(self.data_context)
        while True:
            user_repo.updating()
            QThread.sleep(1)

class SampleThread_2(QThread):
    def __init__(self,data_context):
        QThread.__init__(self)  
        self.data_context = data_context
        self.start()
    def run(self):
        setting_repo = SettingRepo(self.data_context)
        while True:
            setting_repo.updating()
            QThread.sleep(1)

def sig_handler(*args):
    print('sig_handler!', args)
    os.kill(os.getpid(), signal.SIGKILL)

if __name__ == "__main__":

    app = QGuiApplication(sys.argv)
    SignalWakeupHandler(app)
    signal.signal(signal.SIGINT, sig_handler)

    data_context = DataContext("capsaDB.db")
    sample_obj1 = SampleThread_1(data_context)
    sample_obj2 = SampleThread_2(data_context)
    
    

    a = app.exec_()  