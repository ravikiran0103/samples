from pysqlcipher3 import dbapi2 as sqlcipher
from PyQt5.QtCore import QObject
import threading

class SettingRepo:

    def __init__(self, data_context):      
        self.data_context = data_context       
        self.connection_lock = threading.Lock()
        # self.connection_lock = data_context.connection_lock

    def updating(self):
        try:
            self.connection_lock.acquire(True)
            print("updating  setting repo")
            # self.data_context.data_base_connection.commit()
            result = self.data_context.data_base_connection.execute("update setting set settingid=18 where settingid=17")
            self.data_context.data_base_connection.commit()
            self.connection_lock.release()
            print("updated setting repo")
        except Exception as ex:
            print("exception" + str(ex))