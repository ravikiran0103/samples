import threading

from pysqlcipher3 import dbapi2 as sqlcipher

class DataContext:

    '''DataContext Class

    This class is used to connect to the database.
    '''

    def __init__(self, database_name):
        self.db_open(database_name)
        self.connection_lock = threading.Lock()

    def db_open(self,database_name):
        self.data_base_connection = sqlcipher.connect(database_name, check_same_thread = False)
        self.data_base_connection.execute('pragma key="c@ps@"')
        if self.data_base_connection:
            return True
        else:
            return False
    def dispose(self):
        self.connection_lock.acquire(True)
        self.data_base_connection.close()    
        self.connection_lock.release()
        return True