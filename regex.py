import re

pattern = "(([2][5][0-5]\.)|([2][0-4][0-9]\.)|([0-1]?[0-9]?[0-9]\.)){3}(([2][5][0-5])|([2][0-4][0-9])|([0-1]?[0-9]?[0-9]))"
	
test_string = '192.165.154.45'
result = re.match(pattern, test_string)

if result:
	print("Search successful.")
else:
	print("Search unsuccessful.")
