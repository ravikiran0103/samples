import atexit

names = ['Ashok', 'Radha', 'Abdul', 'John']

def hello(name):
   print ("Hello",name)
   
for name in names:
   atexit.register(hello,name)