import pprp
import base64

# CBC-Adapter
def rjindael_decrypt_gen_CBC(key, s, iv, block_size=pprp.config.DEFAULT_BLOCK_SIZE_B):
    r = pprp.crypto_3.rijndael(key, block_size=block_size)

    i = 0
    for block in s:

        decrypted = r.decrypt(block)
        decrypted = xor(decrypted, iv)  
        iv = block

        yield decrypted
        i += 1

def xor(block, iv):
    resultList = [ (a ^ b) for (a,b) in zip(block, iv) ]
    return bytes(resultList)

cipherText = "QiGoOrV4edGFUWvhc0Yup5OpubZdir4amXyvOybalt5O71AuegQUAN/WZJZJCc6n"
passphrase = "some-random-key"

# This constant is used to determine the keysize of the encryption algorithm in bits.
# We divide this by 8 within the code below to get the equivalent number of bytes.
keysize = 128

# This constant determines the number of iterations for the password bytes generation function.
derivationIterations = 1000

# Get the complete stream of bytes that represent:
# [32 bytes of Salt] + [32 bytes of IV] + [n bytes of CipherText]

cipherTextBytesWithSaltAndIv = base64.b64decode(cipherText)
print(type(cipherTextBytesWithSaltAndIv))
cipherTextBytesWithSaltAndIv=list(bytearray(cipherTextBytesWithSaltAndIv))

# Get the saltbytes by extracting the first 32 bytes from the supplied cipherText bytes.
saltStringBytes = cipherTextBytesWithSaltAndIv[:int(keysize / 8)]

# Get the IV bytes by extracting the next 32 bytes from the supplied cipherText bytes.
ivStringBytes = cipherTextBytesWithSaltAndIv[int(keysize / 8):int((keysize / 8) * 2)]

# Get the actual cipher text bytes by removing the first 64 bytes from the cipherText string.    
cipherTextBytes = cipherTextBytesWithSaltAndIv[int((keysize / 8) * 2):]

keyBytes = pprp.pbkdf2(passphrase.encode('utf-8'), bytes(saltStringBytes), int(keysize / 8))

print("--ciphertextbytes--",cipherTextBytes)

print(ivStringBytes)

# Decryption
blocksize = 16
sg = pprp.data_source_gen(cipherTextBytes, blocksize)
dg = rjindael_decrypt_gen_CBC(keyBytes, sg, ivStringBytes, blocksize);
decrypted = pprp.decrypt_sink(dg, blocksize)
print("Decrypted data: " + str(decrypted))




