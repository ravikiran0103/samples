from Crypto.Cipher import AES
import base64
import binascii
'''
KEY = binascii.unhexlify('BE0135F28F01AE69C608A869D13812E1')
KEY="tY7U7JoS0xl74uTfgKBrOg=="
print("Key:", KEY)
IV = binascii.unhexlify('BBD72E890181D83E0049BB32A95A2CB0')
IV = 'c0Yup5OpubZdir4a'
print("IV:", IV)
Pin = b'0652'
Pin = b'1234abcdefg'
HexPin = binascii.hexlify(Pin)
HexPin += b'0505050505'
PlainText = binascii.unhexlify(HexPin)
print(HexPin)
print("PlainText:", PlainText)
rijn = AES.new(KEY, AES.MODE_CBC, IV)
#rijn = AES.new(KEY, AES.MODE_EBC, IV)
CipherText = rijn.encrypt(PlainText)
print("-----------------------------",CipherText)
'''
key = "MI2htK6IDx2RWTLUFs84MA==" #base64 encoded
IV = "n6DXusxCIEo1bbYMRSWp3A=="
keysize = 128
CipherText = binascii.unhexlify('1F98708E787EE69B9D7460FAB3FD8ECD')
CipherText = "hCo5VaCR++wurOBg5XUleZ+g17rMQiBKNW22DEUlqdx8WtbjpWkq8YR6B/fYE8Oj"
CipherText = CipherText[int((keysize / 8) * 2):]
#CipherText = base64.b64decode(CipherText)
#print("CipherText:", binascii.hexlify(CipherText).decode('utf-8'))
rijnd = AES.new(key, AES.MODE_CBC, IV)
pt = rijnd.decrypt(CipherText)
print(pt,str(pt))



