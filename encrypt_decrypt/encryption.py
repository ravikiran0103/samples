import pprp
import base64
import random
from pysqlcipher3 import dbapi2 as sqlcipher
import binascii
import time


db = sqlcipher.connect('capsaDB.db')
db.execute('pragma key="c@ps@"')

def xor(block, iv):
    resultList = [ (a ^ b) for (a,b) in zip(block, iv) ]
    return bytes(resultList)


def rjindael_encrypt_gen_CBC(key, s, iv, block_size=pprp.config.DEFAULT_BLOCK_SIZE_B):
    r = pprp.crypto_3.rijndael(key, block_size=block_size)

    padded = False
    i = 0
    for block in s:
        len_ = len(block)
        if len_ < block_size:
            padding_size = block_size - len_
            block += (chr(padding_size) * padding_size).encode('ASCII')
            padded = True

        block = xor(block,iv)
        encrypted = r.encrypt(block)
        iv = encrypted

        yield encrypted
        i += 1

    if padded is False:
        block = (chr(block_size) * block_size).encode('ASCII')
        yield r.encrypt(block)


def rjindael_decrypt_gen_CBC(key, s, iv, block_size=pprp.config.DEFAULT_BLOCK_SIZE_B):
    r = pprp.crypto_3.rijndael(key, block_size=block_size)

    i = 0
    for block in s:

        decrypted = r.decrypt(block)
        decrypted = xor(decrypted, iv)  
        iv = block

        yield decrypted
        i += 1


def encrypt(
        plainText,
        saltStringBytes,
        ivStringBytes,
        key,        
        keysize = 128
    ):

    keyBytes = pprp.pbkdf2(key.encode('utf-8'), bytes(saltStringBytes), int(keysize / 8))

    plainTextBytes = plainText.encode('ASCII')

    # Encryption
    blocksize = 16
    sg = pprp.data_source_gen(plainTextBytes, blocksize)
    dg = rjindael_encrypt_gen_CBC(keyBytes, sg, ivStringBytes, blocksize);
    encrypted = pprp.encrypt_sink(dg)

    return encrypted


def decrypt(
        cipherTextBytes,
        saltStringBytes,
        ivStringBytes,
        key,
        blocksize = 16
    ):
    
    keyBytes = pprp.pbkdf2(key.encode('utf-8'), bytes(saltStringBytes), int(keysize / 8))
    sg = pprp.data_source_gen(cipherTextBytes, blocksize)
    dg = rjindael_decrypt_gen_CBC(keyBytes, sg, ivStringBytes, blocksize);
    decrypted = pprp.decrypt_sink(dg, blocksize)
    return str(decrypted)


if __name__ == "__main__":
    
    cipherText = "QiGoOrV4edGFUWvhc0Yup5OpubZdir4amXyvOybalt5O71AuegQUAN/WZJZJCc6n"
    keysize = 128

    # Get the complete stream of bytes that represent:
    # [32 bytes of Salt] + [32 bytes of IV] + [n bytes of CipherText]
    cipherTextBytesWithSaltAndIv = base64.b64decode(cipherText)
    # cipherTextBytesWithSaltAndIv=list(bytearray(cipherTextBytesWithSaltAndIv))

    # Get the saltbytes by extracting the first 32 bytes from the supplied cipherText bytes.
    saltStringBytes = cipherTextBytesWithSaltAndIv[:int(keysize / 8)]
    # saltStringBytes = base64.b64decode(cipherText[:int(keysize / 8)])
    # print(str(base64.b64encode(bytes(saltStringBytes))))

    # Get the IV bytes by extracting the next 32 bytes from the supplied cipherText bytes.
    ivStringBytes = cipherTextBytesWithSaltAndIv[int(keysize / 8):int((keysize / 8) * 2)]
    # ivStringBytes = base64.b64decode(cipherText[int(keysize / 8):int((keysize / 8) * 2)])
    # print(str(base64.b64encode(bytes(ivStringBytes))))

    # POPULATE
    # ENCRYPT AND ADD DATA TO DB
    db.execute("delete from user")
    db.commit()
    value_count = [1, 100, 2000]
    for i in value_count:
        pin_list = []
        count = 0

        # RESETTING TABLE
        query = "delete from user where userid > " + str(i)
        db.execute(query)
        db.commit()

        while len(pin_list) < i:
            pin = random.randrange(1000, 9999)
            if pin not in pin_list:
                pin_list.append(pin)
                encrypted_pin = encrypt(
                    str(pin),
                    saltStringBytes,
                    ivStringBytes,
                    "some-random-key"
                )
                cipher_txt = saltStringBytes
                cipher_txt += ivStringBytes
                cipher_txt += encrypted_pin
                cipher_txt = str(base64.b64encode(cipher_txt))

                query = "insert or replace into user values (?,?,?,?,?,?,?,?)"
                items = [
                    count, "User " + str(count), cipher_txt,
                    1, 1, 2, "2020-01-28 11:13:59", 0
                ]
                db.execute(query, items)
                db.commit()

                count += 1

        start = time.process_time()
        # READ DATA FROM DB
        query = "SELECT * FROM User"
        records = db.cursor().execute(query).fetchall()
        if records:
            count = 0
            for rows in records:
                # DECRYPT
                cipher_text = rows[2]
                cipherTextBytesWithSaltAndIv = base64.b64decode(cipher_text[2:-1])
                cipherTextBytes = cipherTextBytesWithSaltAndIv[int((keysize / 8) * 2):]
                pin = decrypt(
                    cipherTextBytes,
                    saltStringBytes,
                    ivStringBytes,
                    "some-random-key"
                )

                # ENCRYPT
                encrypted_pin = encrypt(
                    str(pin),
                    saltStringBytes,
                    ivStringBytes,
                    "some-random-new-key"
                )
                cipher_txt = saltStringBytes
                cipher_txt += ivStringBytes
                cipher_txt += encrypted_pin
                cipher_txt = str(base64.b64encode(cipher_txt))

                # WRITE TO DB
                query = "insert or replace into user values (?,?,?,?,?,?,?,?)"
                items = [
                    count, "User " + str(count), cipher_txt,
                    1, 1, 2, "2020-01-28 11:13:59", 0
                ]
                db.execute(query, items)
                db.commit()
                count += 1
                # print(count, cipher_txt)

        time_taken = time.process_time() - start
        print("TIME TAKEN FOR " + str(i) + " RECORD(S) = ", time_taken)