import sys

from PyQt5.QtGui import QGuiApplication
from PyQt5.QtQml import QQmlComponent, QQmlApplicationEngine
from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot, QUrl, QEvent
from PyQt5.QtQuick import QQuickView

class Example(QGuiApplication):
    keyPressed = pyqtSignal(QEvent)
    def __init__(self):
        QGuiApplication.__init__(self, sys.argv)


    def keyPressEvent(self, event):
        super(Example, self).keyPressEvent(event)
        self.keyPressed.emit(event) 

    def on_key(self, event):
        if event.key() == QtCore.Qt.Key_Enter and self.ui.continueButton.isEnabled():
            self.proceed()  # this is called whenever the continue button is pressed
        elif event.key() == QtCore.Qt.Key_Q:
            print("Killing")
            self.deleteLater()  # a test I implemented to see if pressing 'Q' would close the window

    def proceed(self):
        print("Call Enter Key")

def main():
	
	app = QGuiApplication(sys.argv)
	ex = Example()
	
	# app.setGeometry(300, 300, 250, 150)
	app.show()
	app.keyPressed.connect(ex.on_key)
	sys.exit(app.exec_())

if __name__ == '__main__':
    main()