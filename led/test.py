from ctypes import cdll
import time
import random

lib_nm = cdll.LoadLibrary('./libcan.so')
lib_nm.canOpen()
lib_led = cdll.LoadLibrary('./libled.so')

random_val = lambda: random.randint(0,255)

while True:
	color = '%02X%02X%02X' % (random_val(),random_val(),random_val())
	rgb = list(int(color[i:i+2], 16) for i in (0, 2, 4))
	for intensity in range(0, 255, 5):
		r = int(rgb[0] * (intensity / 255))
		g = int(rgb[1] * (intensity / 255))
		b = int(rgb[2] * (intensity / 255))
		code = '%02X%02X%02X' % (r,g,b)
		code = int(code, 16)
		print(code)

		lib_nm.groundLightIntensityChangeRequest(code)
		lib_led.keyboardLEDSetColor(code)