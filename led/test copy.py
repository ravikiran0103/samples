from ctypes import cdll
import time
import random

lib_nm = cdll.LoadLibrary('./libcan.so')
lib_nm.canOpen()
lib_led = cdll.LoadLibrary('./libled.so')

# r = lambda: random.randint(0,255)

# def get_color_code_from_intensity(color, intensity):
	
# 	'''This method gives the code for the currently set color 
# 	for the given intensity
# 	'''

# 	try:
# 		if type(intensity) == int:
# 			if (intensity >= 0 and intensity <= 255):
# 				replace_str = str(hex(intensity))[2:4]
# 				if len(replace_str) < 2:
# 					replace_str = "0" + replace_str

# 				color = color.replace('FF', replace_str.upper())
# 				color = color.replace('#', '')
# 				return int(color, 16)

# 		return int(color, 16)

# 	except Exception as ex:
# 		print("In get_color_code_from_intensity - " + str(ex))
# 		return int(color, 16)

# def getTintedColor(color, v):
# 	rgb = parseInt(color, 16)
# 	r = Math.abs(((rgb >> 16) & 0xFF)+v)
# 	if (r>255):
# 		r=r-(r-255)
		
# 	g = Math.abs(((rgb >> 8) & 0xFF)+v)
# 	if (g>255):
# 		g=g-(g-255);

# 	b = Math.abs((rgb & 0xFF)+v)
# 	if (b>255):
# 		b=b-(b-255)

# 	r = Number(r < 0 || isNaN(r)) ? 0 : ((r > 255) ? 255 : r).toString(16)
# 	if (r.length == 1):
# 		r = '0' + r
# 	g = Number(g < 0 || isNaN(g)) ? 0 : ((g > 255) ? 255 : g).toString(16)
# 	if (g.length == 1):
# 		g = '0' + g;
# 	b = Number(b < 0 || isNaN(b)) ? 0 : ((b > 255) ? 255 : b).toString(16)
# 	if (b.length == 1):
# 		b = '0' + b;
# 	return "#" + r + g + b

while True:
	# lib_nm.groundLightIntensityChangeRequest(0xff0000)
	# color = '#%02X%02X%02X' % (r(),r(),r())
	color = random.choice(["FF0000", "00FF00", "0000FF"])
	rgb = list(int(color[i:i+2], 16) for i in (0, 2, 4))
	print(rgb)
	for intensity in range(0, 255, 5):
		# code = get_color_code_from_intensity(color, intensity)
		# print(code)
		# r = (intensity * rgb[0] + (100 - intensity) * 255) / 100
		r = int(rgb[0] * ((100 * intensity) / 255))
		# g = (intensity * rgb[1] + (100 - intensity) * 255) / 100
		g = int(rgb[1] * ((100 * intensity) / 255))
		# b = (intensity * rgb[2] + (100 - intensity) * 255) / 100
		b = int(rgb[2] * ((100 * intensity) / 255))
		code = '%02X%02X%02X' % (r,g,b)
		code = int(code, 16)
		print(code)
		lib_nm.groundLightIntensityChangeRequest(code)
		time.sleep(0.2)
	# lib_nm.groundLightIntensityChangeRequest(0xffff00)
	# time.sleep(0.001)

	# lib_led.keyboardLEDSetColor(0xff0000)
	# lib_led.keyboardLEDSetColor(color)
	# time.sleep(0.2)
	# lib_led.keyboardLEDSetColor(0xffff00)
	# time.sleep(0.010)