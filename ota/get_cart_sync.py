import requests 
import json

# url = "https://cap-us-usce-dev-as-001.azurewebsites.net/api/device/carelink/100178/sync-queue"
# payload = {}

# print(payload)

# headers = {'content-type': 'application/json'}


# headers = {'content-type': 'application/json','Authorization': 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IkNhcnRBY2NvdW50RUNPTlRFU1QiLCJFbnRlcnByaXNlSWQiOiIxMDAiLCJVc2VySWQiOiJhMjMzNzYxYi1hYjk4LTQzYTktOTM3ZC00NWI0MzY1OGFhODkiLCJVc2VyTmFtZSI6IkNhcnRBY2NvdW50RUNPTlRFU1QiLCJUaW1lWm9uZSI6IlBhY2lmaWNTdGFuZGFyZFRpbWUiLCJFbnRlcnByaXNlQWRtaW4iOiIxIiwibmJmIjoxNTc5OTQyMjM5LCJleHAiOjE4OTU1NjE0MzksImlhdCI6MTU3OTk0MjIzOSwiaXNzIjoiQ2Fwc2FBcGkiLCJhdWQiOiJDYXBzYUNsaWVudCJ9.VnOQjebfU1JxVYhiqDb5pBSCzSjWo9ESvN3jxdINCPzjQ38DuRYIs0nyCqtIx9UH9TCX5rfo-z7Y7ldTK-yZNXqi_4WszzTfMVuHhzVtNw5FJfp4hBQXjOjGhZglq436ecKtsRKCvXWslB7wkFkCqiPX312D9tP01Kkuc4TILOruSHpjXaGDY5REASIQJ0TbhlyebhYBwjo7LwquoRgfhQA7uq4fYreSVSZMWNqfzVc64pzzWKnxSH3MQJX_kox7rWe2GpOApCThgjiDb5daUnooqhtUs_6_ZrxFeHv-nc5Dk3-ywkoxT_u__2g4IZQRygPYWIUpeQrDevGPyl78Hw'}
# result = requests.get(url, data=json.dumps(payload), headers=headers)
# print(result.json())

def get_sync_queue(self, cart_id):

	url = Endpoints.CAPSA_AP.value + "api/device/carelink/"+ str(cart_id) + "/sync-queue"
	# url = "https://cap-us-usce-dev-as-001.azurewebsites.net/api/device/carelink/100178/sync-queue"
	payload = {}
	headers = {
			'content-type': 'application/json',
			# 'Authorization': 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IkNhcnRBY2NvdW50RUNPTlRFU1QiLCJFbnRlcnByaXNlSWQiOiIxMDAiLCJVc2VySWQiOiJhMjMzNzYxYi1hYjk4LTQzYTktOTM3ZC00NWI0MzY1OGFhODkiLCJVc2VyTmFtZSI6IkNhcnRBY2NvdW50RUNPTlRFU1QiLCJUaW1lWm9uZSI6IlBhY2lmaWNTdGFuZGFyZFRpbWUiLCJFbnRlcnByaXNlQWRtaW4iOiIxIiwibmJmIjoxNTc5OTQyMjM5LCJleHAiOjE4OTU1NjE0MzksImlhdCI6MTU3OTk0MjIzOSwiaXNzIjoiQ2Fwc2FBcGkiLCJhdWQiOiJDYXBzYUNsaWVudCJ9.VnOQjebfU1JxVYhiqDb5pBSCzSjWo9ESvN3jxdINCPzjQ38DuRYIs0nyCqtIx9UH9TCX5rfo-z7Y7ldTK-yZNXqi_4WszzTfMVuHhzVtNw5FJfp4hBQXjOjGhZglq436ecKtsRKCvXWslB7wkFkCqiPX312D9tP01Kkuc4TILOruSHpjXaGDY5REASIQJ0TbhlyebhYBwjo7LwquoRgfhQA7uq4fYreSVSZMWNqfzVc64pzzWKnxSH3MQJX_kox7rWe2GpOApCThgjiDb5daUnooqhtUs_6_ZrxFeHv-nc5Dk3-ywkoxT_u__2g4IZQRygPYWIUpeQrDevGPyl78Hw'
			'Authorization': 'Bearer ' + self.user_token
		}
	response = requests.get(url, data=json.dumps(payload), headers=headers)
	response = response.json()

	print(response)
	
	if 'Result' in response.keys():
		results  = response['Result']
		sync_json_txt_list = []
		for result in results:
			if 'SyncJsonText' in result.keys():
				sync_json_dict = eval(result['SyncJsonText'])
				if 'SyncType' in sync_json_dict.keys() and \
						'RecordId' in sync_json_dict.keys():
					sync_json_txt_list.append(sync_json_dict)
		return sync_json_txt_list

	return None


print(get_sync_queue(100178))