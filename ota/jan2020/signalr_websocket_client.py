import websocket
import json

ws = None

#Function to encode json so that it is terminated by the ASCII character 0x1E (record separator)
def encode_json(obj):
    return json.dumps(obj) + chr(0x1E)

def ws_on_message(ws, message: str):
    print("--",message)
    ignore_list = ['{"type":6}', '{}']
    # Split using record seperator, as records can be received as one message
    for msg in message.split(chr(0x1E)):
        if msg and msg not in ignore_list:
            pass
            # Everything else not on ignore list
            #print("Signal from server is received:",message)
        
#Function called on any error occurrence in web socket
def ws_on_error(ws, error):
    print("error in web socket")
    print(error,type(error))
    
#Function called on closing the web socket connection
def ws_on_close(ws):
    print("Disconnected from SignalR Server")

#Function called on opening the web socket connection
def ws_on_open(ws):
    print("Connected to SignalR Server via WebSocket")
    
    # Do a handshake request
    print("Performing handshake request")
    ws.send(encode_json({
        "protocol": "json",
        "version": 1
    }))
    
    ws.send(encode_json({
        "type": 1,
        "target": "JoinGroup",
        "arguments": [1]
    }))

    # Handshake completed
    print("Handshake request completed")


if __name__ == "__main__":
    websocket.enableTrace(True)
    ws = websocket.WebSocketApp("wss://cap-us-usce-dev-as-002.azurewebsites.net/carelink",
                              on_message = ws_on_message,
                              on_error = ws_on_error,
                              on_close = ws_on_close)
    ws.on_open = ws_on_open
    ws.run_forever()
