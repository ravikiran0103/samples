import requests 
import json

def get_auth():

	try:		
		url = "https://capsa-api-qa.capsahealthcare.com/api/auth/getauth"
		payload = {
			"ClientId":"8r{AqE6*F9161",
			"ClientSecret": "A?c3o8-SY",
			"UserName": "Carelink_User",
			"Password": "c@rel!nk2019"
		}

		headers = {'content-type': 'application/json'}
		response = requests.post(
			url, 
			data=json.dumps(payload),
			headers=headers
		)
		response = json.loads(response.text)
		# print(response)
		print(response["Result"]["Token"])
		return response["Result"]["Token"]

	except Exception as ex:
		print(ex)

def get_package_info(auth_token):
	
	'''This method fetches the update package's
	information from GetDeviceVersionInfo endpoint
	'''

	try:
		headers = {
			'content-type': 'application/json',
			'Authorization': 'Bearer ' + auth_token
		}

		print("************")
		print(auth_token)

		response = requests.get(
			"https://cap-us-usce-dev-as-001.azurewebsites.net/api/device/9/version?updateType=Accessory",
			headers=headers
		)
		response = json.loads(response.text)
		print("##########")
		print(response)

		# self.__pkg_size = 0
		# self.__pkg_version_id = 0

		# if 'Result' in response.keys():
		# 	if 'FileSize' in response['Result'][0].keys() and \
		# 			'UpdateVersionId' in response['Result'][0].keys():
		# 		self.__pkg_size = response["Result"][0]["FileSize"]
		# 		self.__pkg_version_id = \
		# 			response["Result"][0]["UpdateVersionId"]
		# 		return True

		return False

	except Exception as ex:
		print("get_package_info - " + str(ex))
		return False


auth_token = get_auth()
# get_package_info(auth_token)