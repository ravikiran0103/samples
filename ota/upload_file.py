import requests
import json


def upload():

	try:
		url = "https://cap-us-usce-dev-as-001.azurewebsites.net/api/device/version/create"

		payload = "------d74496d66958873e\r\nContent-Disposition: form-data;" \
			" name=\"devicetypeid\"\r\n\r\n9\r\n------d74496d66958873e\r\n" \
			"Content-Disposition: form-data; name=\"version\"\r\n\r\nv1.0.17\r\n" \
			"------d74496d66958873e\r\nContent-Disposition: form-data; name=\"file\"; " \
			"filename=\"CCS_OTA_A19_Test7.zip\"\r\nContent-Type: application/zip\r\n\r\n" \
			"\r\n------d74496d66958873e--"
		headers = {
			'content-type': "multipart/form-data; boundary=----d74496d66958873e",
			'Authorization': "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFt" \
				"ZSI6IkNhcmVsaW5rMiIsIkVudGVycHJpc2VJZCI6IjEiLCJVc2VySWQiOiI4NjY3NzNjZi0xNzhk" \
				"LTRhZDctYTIzYy1iZjJiZDdmMzQxODUiLCJVc2VyTmFtZSI6IkNhcmVsaW5rMiIsIlRpbWVab25lIj" \
				"oiUGFjaWZpY1N0YW5kYXJkVGltZSIsIm5iZiI6MTU3MzA3OTU2NywiZXhwIjoxODg4Njk4NzY3LCJp" \
				"YXQiOjE1NzMwNzk1NjcsImlzcyI6IkNhcHNhQXBpIiwiYXVkIjoiQ2Fwc2FDbGllbnQifQ.Eyy1kNs" \
				"AafLOlFwCIxW9rkSe7D3pQONUyN37rdn-gHPxAJAkY7O5o9bQIosdExZDkiKPgV5GWPsAt2I-VTD8Q" \
				"Yolz1YyVFGBwz7k5uCNY11l1hyzrc_Ov15rvkC66i_wOKdD_13h-kvUg-KwJJzEhcyHEAjWsY1-Kkc" \
				"alTbcfz6yGnX0I_nVgHJuY799OlyfsoOu10VsnBptheos0zEidkTeOV9kAY5Oi7T9syV6S9lJtknChcB"\
				"yaEcrtu2XOXZ-vWPEPf4YI0Sezu9nPyZt7dwGz0KFyrnrYws_OslSwrl53v0zRHS7BuP8aWCKfgZFZ0RpIPa77B_n9lh8FO_pnQ",
		}
		print("UPLOADING")
		response = requests.request(
			"POST",
			url,
			data=payload,
			headers=headers,
		)
		print(response.json())
	
	except Exception as ex:
		print("EXCEPTION ", ex)


upload()