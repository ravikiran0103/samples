import requests
import json

def __update_status_to_nsight(self, status=False):

	try:
		headers = {
			'content-type': 'application/json',
			'Authorization': "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFt" \
				"ZSI6IkNhcmVsaW5rMiIsIkVudGVycHJpc2VJZCI6IjEiLCJVc2VySWQiOiI4NjY3NzNjZi0xNzhk" \
				"LTRhZDctYTIzYy1iZjJiZDdmMzQxODUiLCJVc2VyTmFtZSI6IkNhcmVsaW5rMiIsIlRpbWVab25lIj" \
				"oiUGFjaWZpY1N0YW5kYXJkVGltZSIsIm5iZiI6MTU3MzA3OTU2NywiZXhwIjoxODg4Njk4NzY3LCJp" \
				"YXQiOjE1NzMwNzk1NjcsImlzcyI6IkNhcHNhQXBpIiwiYXVkIjoiQ2Fwc2FDbGllbnQifQ.Eyy1kNs" \
				"AafLOlFwCIxW9rkSe7D3pQONUyN37rdn-gHPxAJAkY7O5o9bQIosdExZDkiKPgV5GWPsAt2I-VTD8Q" \
				"Yolz1YyVFGBwz7k5uCNY11l1hyzrc_Ov15rvkC66i_wOKdD_13h-kvUg-KwJJzEhcyHEAjWsY1-Kkc" \
				"alTbcfz6yGnX0I_nVgHJuY799OlyfsoOu10VsnBptheos0zEidkTeOV9kAY5Oi7T9syV6S9lJtknChcB"\
				"yaEcrtu2XOXZ-vWPEPf4YI0Sezu9nPyZt7dwGz0KFyrnrYws_OslSwrl53v0zRHS7BuP8aWCKfgZFZ0RpIPa77B_n9lh8FO_pnQ",
		}

		payload = {
			'SerialNumber': 1,
			'DeviceTypeId': 9,
			'UpdateVersionId': 4,
			'Success': status
		}

		response = requests.post(
			"https://cap-us-usce-dev-as-001.azurewebsites.net/api/device/version/update-status",
			data=payload,
			headers=headers
		)
		response = json.loads(response.text)
		print(type(response))
		print(response)

		print("##########")
		if 'Result' in response.keys():
			print("present")
		else:
			print("not present")
		print(hasattr(response, 'Result'))
		print(hasattr(response['Validation'], 'InvalidFields'))
		if 'InvalidFields' in response['Validation'].keys():
			print("present")
		else:
			print("not present")
		print("##########")

		print(response["Result"])

	except Exception as ex:
		print(ex)

__update_status_to_nsight(True)