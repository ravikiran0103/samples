import requests
import json


def get_info():

	try:
		url = 'https://cap-us-usce-dev-as-001.azurewebsites.net/api/device/version/9'
		headers = {
			'content-type': 'application/json',
			'Authorization': 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IkNhcmVsaW5rMiIsIkVudGVycHJpc2VJZCI6IjEiLCJVc2VySWQiOiI4NjY3NzNjZi0xNzhkLTRhZDctYTIzYy1iZjJiZDdmMzQxODUiLCJVc2VyTmFtZSI6IkNhcmVsaW5rMiIsIlRpbWVab25lIjoiUGFjaWZpY1N0YW5kYXJkVGltZSIsIm5iZiI6MTU3MzA3OTU2NywiZXhwIjoxODg4Njk4NzY3LCJpYXQiOjE1NzMwNzk1NjcsImlzcyI6IkNhcHNhQXBpIiwiYXVkIjoiQ2Fwc2FDbGllbnQifQ.Eyy1kNsAafLOlFwCIxW9rkSe7D3pQONUyN37rdn-gHPxAJAkY7O5o9bQIosdExZDkiKPgV5GWPsAt2I-VTD8QYolz1YyVFGBwz7k5uCNY11l1hyzrc_Ov15rvkC66i_wOKdD_13h-kvUg-KwJJzEhcyHEAjWsY1-KkcalTbcfz6yGnX0I_nVgHJuY799OlyfsoOu10VsnBptheos0zEidkTeOV9kAY5Oi7T9syV6S9lJtknChcByaEcrtu2XOXZ-vWPEPf4YI0Sezu9nPyZt7dwGz0KFyrnrYws_OslSwrl53v0zRHS7BuP8aWCKfgZFZ0RpIPa77B_n9lh8FO_pnQ',
		}

		response = requests.get(url, headers=headers)
		print(response.json())

	except Exception as ex:
		print("EXCEPTION ", ex)


def download():

	try:
		for x in range(0,100):
			# url = "https://cap-us-usce-dev-as-001.azurewebsites.net/api/device/9/version/19"
			url = "https://cap-us-usce-dev-as-001.azurewebsites.net/api/device/9/version/23"

			headers = {
				# 'Content-Type': "application/x-www-form-urlencoded",
				'Content-Type': "application/json",
				'Authorization': "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IkNhcmVMaW5rU2V0dXAxMDAiLCJFbnRlcnByaXNlSWQiOiIxMDAiLCJVc2VySWQiOiI2N0Q5MDkwNi0wOEFCLTQ2NEEtQUE1RS0wQTcwOEY2QjUzMTQiLCJuYmYiOjE1Nzk2MDEyNDQsImV4cCI6MTg5NTIyMDQ0NCwiaWF0IjoxNTc5NjAxMjQ0LCJpc3MiOiJDYXBzYUFwaSIsImF1ZCI6IkNhcHNhQ2xpZW50In0.WKXbP6Fj6yhV9FAfgMRq1253gGfapDV45wNKOrBWj8afZFHmhVC7G52yBx9wq3TTG9kgEmnbS2aYntYVNRvbcgY_G93WfzopOxc2MFPqQz3sqyiXuv-UiMVuoPE6aEJO0i0BiJ8NEnosT6YtNAUTInOrVsVt9lm_ZQff7Ory9cn-V3BGULUx-IyB4wkWYre3rNOIKnBe4Gt4StcXTap_N2FW_A-i9lzyhdllnTyBv49MJhi8qD_rCCEQVo-bZE4UkfDjZWotGZ8getXzlNKWZJTsa-76Z2Uf82RIfC-nEUZtH2ZjX5KPJnBKtg2_dZec0p86pmYAgjjXuslyoE-C6w",
			}

			print("DOWNLOADING Count ", x)
			with open("./test.zip", 'wb') as file:
				response =  requests.get(
					url,
					headers=headers,
					stream=True
				)
				# raise HTTP errors if any
				response.raise_for_status()

				# download and write in chunks of 1MB
				for chunk in response.iter_content(chunk_size=1*1024*1024):
					# filter out keep-alive new chunks
					if chunk:
						file.write(chunk)
			print("DOWNLOADED Count ", x)

	except Exception as ex:
		print(ex)


# get_info()
download()