import requests 
import json

url = "https://capsa-api-qa.capsahealthcare.com/api/device/carelink/event"
payload = [{
'EventId':1,
'EventTypeId': 12,
'EventDateTimeUtc':'2010-06-06'
}]

print(payload)

headers = {'content-type': 'application/json','Authorization': 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IkNhcmVsaW5rX1VzZXIiLCJVc2VySWQiOiI2MTA1OTMyNC02MjAwLTQxREYtQkZFQy04MjJCQzJDNDg3ODgiLCJFbnRlcnByaXNlSWQiOiI5OSIsIlRpbWVab25lIjoiUGFjaWZpY1N0YW5kYXJkVGltZSIsIlVzZXJOYW1lIjoiQ2FyZWxpbmtfVXNlciIsIm5iZiI6MTU2MDE1MjEwMywiZXhwIjoxODc1NzcxMzAzLCJpYXQiOjE1NjAxNTIxMDMsImlzcyI6IkNhcHNhQXBpIiwiYXVkIjoiQ2Fwc2FDbGllbnQifQ.Pe6KBDPpGSE5ypH7iYp_-7H3Q1SqMQgUFvjeks06fpjIbKlDEbs6OtJSah47UDsCdOrs_zWSq334NU1DT3OG2_KNK0L6V1ywLEDXKHoB21LTxv6z-du0Pl9O1BOz6sdtqJ4FvtTN9i0sZ1Zo7Bf0ELiayYqhiIUY1NJCeY4zJMRUAie6poN9UHfhfYLJu02mety5LIZvtSwXoKor8ZIoMIxLiMyMURABc0Ot6-gsy-d6uaj4qgPt4wrSr02fxviL1ljbvA1KodsSxM5coLot_I3TVxFoo5zpyJ1YQe2a6Cfi751GUke-NU3bFRSK0n1I0mQpy3jcHT3Ua5d9XhwmeQ'}

result = requests.put(url, data=json.dumps(payload), headers=headers)
print(result.reason)
print(result.json())