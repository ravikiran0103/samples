from Crypto.Cipher import AES
import binascii
import math

KEY = 'BE0135F28F01AE69C608A869D13812E1'
IV = binascii.unhexlify('BBD72E890181D83E0049BB32A95A2CB0')

def Encrypt(pin):
    KEY = binascii.unhexlify('BE0135F28F01AE69C608A869D13812E1')
    # print("Key:", KEY)
    IV = binascii.unhexlify('BBD72E890181D83E0049BB32A95A2CB0')
    # # print("IV:", IV)
    # Pin = pin #b'0652'
    # HexPin = binascii.hexlify(Pin)

    # HexPin = _pad(HexPin.decode('utf-8'))
    # print(HexPin)   
    # PlainText = binascii.unhexlify(HexPin)
    # # print("PlainText:", PlainText)
    # rijn = AES.new(KEY, AES.MODE_CBC, IV)
    # CipherText = rijn.encrypt(PlainText)
    # EncryptedPin = binascii.hexlify(CipherText).decode('utf-8')
    # print("CipherText:", EncryptedPin)
    rijnd = AES.new(KEY, AES.MODE_CBC, IV)
    pt = rijnd.decrypt("1F98708E787EE69B9D7460FAB3FD8ECD")
    print("PT ", pt)
    # print("EncryptedPin Type", type(EncryptedPin))
    # return EncryptedPin

def _pad(text):
    block_size = 32
    text_length = len(text)

    amount_to_pad = int((block_size - text_length) / 2)
    if amount_to_pad == 0:
        return text

    pad = (hex(amount_to_pad)[2:]).upper()
    if len(pad) != 2:
        pad = "0" + pad

    # print(len(text + pad * amount_to_pad))
    return text + pad * amount_to_pad


def decrypt(cipher_text, key=KEY):
	
    '''This method decrypts the given cipher text.'''

    cipher_text = binascii.unhexlify(cipher_text)
    key = binascii.unhexlify(key)
    rijnd = AES.new(key, AES.MODE_CBC, IV)
    plain_text = rijnd.decrypt(cipher_text)
    print(plain_text.decode().strip())

decrypt("E2476FA243D3A9030968BD7BBE20C0DD")