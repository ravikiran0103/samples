from pysqlcipher3 import dbapi2 as sqlcipher

db = sqlcipher.connect('capsaDB.db')
db.execute('pragma key="c@ps@"')

# LANGUAGE TABLE
db.execute(
	'create table Language( \
		LanguageId		INTEGER PRIMARY KEY, \
		EnglishName		TEXT	NULL, \
		FrenchName		TEXT	NULL, \
		GermanName		TEXT	NULL, \
		Alpha3			TEXT	NULL, \
		UpdatedUtc		TEXT	NULL, \
		DataChanged		INTEGER NULL \
	)'
)

# TRANSLATION TABLE
db.execute(
	'create table Translation( \
		TranslationId	INTEGER	PRIMARY KEY, \
		LanguageId		INTEGER	NOT NULL, \
		ReferenceIndex	TEXT	NOT NULL, \
		LanguageText	TEXT	NOT NULL, \
		UpdatedUtc		TEXT	NULL, \
		DataChanged		INTEGER NULL, \
		FOREIGN KEY(LanguageId) REFERENCES Language(LanguageId) ON DELETE CASCADE \
	)'
)

# USER TABLE
db.execute(
	'create table User( \
		UserId		INTEGER	PRIMARY KEY, \
		FirstName	TEXT	NOT NULL, \
		MiddleName 	TEXT	NOT NULL, \
		LastName 	TEXT	NOT NULL, \
		UserName 	TEXT 	NOT NULL, \
		Pin 		TEXT 	NOT NULL, \
		IsActive 	INTEGER NOT NULL, \
		LanguageId	INTEGER NULL, \
		UpdatedUtc	TEXT	NULL, \
		DataChanged	INTEGER NULL, \
		FOREIGN KEY(LanguageId) REFERENCES Language(LanguageId) ON DELETE CASCADE \
	)'
)

# SETTING TABLE
db.execute(
	'create table Setting( \
		SettingId 		INTEGER	PRIMARY KEY, \
		SettingName		TEXT	NOT NULL, \
		DefaultValue	TEXT	NOT NULL, \
		IsOverridable	INTEGER NOT NULL, \
		UpdatedUtc		TEXT	NULL, \
		DataChanged		INTEGER NULL \
	)'
)

# USER SETTING TABLE
db.execute(
	'create table UserSetting( \
		UserSettingId	INTEGER	PRIMARY KEY, \
		UserId			INTEGER NOT NULL, \
		SettingId		INTEGER NOT NULL, \
		SettingValue	TEXT 	NULL, \
		UpdatedUtc		TEXT	NULL, \
		DataChanged		INTEGER NULL, \
		FOREIGN KEY(UserId) REFERENCES User(UserId) ON DELETE CASCADE, \
		FOREIGN KEY(SettingId) REFERENCES Setting(SettingId) ON DELETE CASCADE \
	)'
)

# EVENT CATAGORY TABLE
db.execute(
	'create table EventCatagory( \
		EventCatagoryId	INTEGER	PRIMARY KEY, \
		Description		TEXT 	NOT NULL, \
		UpdatedUtc		TEXT	NULL, \
		DataChanged		INTEGER NULL \
	)'
)

# EVENT TYPE TABLE
db.execute(
	'CREATE TABLE EventType( \
		EventTypeId		INTEGER	PRIMARY KEY, \
		EventCatagoryId	INTEGER NOT NULL, \
		Description		TEXT 	NOT NULL, \
		UpdatedUtc		TEXT	NULL, \
		DataChanged		INTEGER NULL, \
		FOREIGN KEY(EventCatagoryId) REFERENCES EventCatagory(EventCatagoryId) ON DELETE CASCADE \
	)'
)	

# EVENT TABLE
db.execute(
	'create table Event( \
		EventId			INTEGER	PRIMARY KEY, \
		EventDateUtc	TEXT 	NOT NULL, \
		EventTypeId		INTEGER NOT NULL, \
		FOREIGN KEY(EventTypeId) REFERENCES EventType(EventTypeId) ON DELETE CASCADE \
	)'
)

# EVENT DETAIL TABLE
db.execute(
	'create table EventDetail( \
		EventDetailId	INTEGER PRIMARY KEY, \
		EventId			INTEGER	NOT NULL, \
		Description		TEXT	NOT NULL, \
		Message			TEXT	NULL, \
		FOREIGN KEY(EventId) REFERENCES Event(EventId) ON DELETE CASCADE \
	)'
)

# MESSAGE TYPE
db.execute(
	'create table MessageType( \
		MessageTypeId	INTEGER	PRIMARY KEY, \
		Description		TEXT	NOT NULL, \
		UpdatedUtc		TEXT	NULL, \
		DataChanged		INTEGER NULL \
	)'
)

# MESSAGE QUEUE TABLE
db.execute(
	'create table MessageQueue( \
		MessageQueueId		INTEGER	PRIMARY KEY, \
		SenderName			Text 	NOT NULL, \
		SenderTypeId		INTEGER NOT NULL, \
		UserId				INTEGER NOT NULL, \
		Subject				TEXT	NULL, \
		Body				TEXT	NULL, \
		BackgroundColor		INTEGER	NOT NULL, \
		ForegroundColor		INTEGER	NOT NULL, \
		SentDateTime 		TEXT	NULL, \
		ReceivedDateTime	TEXT 	NULL, \
		ExpirationDateTime	TEXT	NULL, \
		MessageTypeId		INTEGER	NOT NULL, \
		UpdatedUtc			TEXT	NULL, \
		DataChanged			INTEGER NULL, \
		FOREIGN KEY(UserId) REFERENCES User(UserId) ON DELETE CASCADE, \
		FOREIGN KEY(MessageTypeId) REFERENCES MessageType(MessageTypeId) ON DELETE CASCADE \
	)'
)

# WIRELESS NETWORKS
db.execute(
	'create table WirelessNetwork( \
		WirelessNetworkID					INTEGER	PRIMARY KEY, \
		SSID								TEXT 	NOT NULL, \
		Encryption							TEXT	NOT NULL, \
		Authentication						TEXT	NOT NULL, \
		NetworkKey							TEXT	NULL, \
		KeyIndex							TEXT	NOT NULL, \
		EAPType								TEXT	NOT NULL, \
		WirelessNetworkName					TEXT 	NULL, \
		EightZeroTwoAuthenticationEnable	INTEGER	NOT NULL, \
		UpdatedUtc							TEXT	NULL, \
		DataChanged							INTEGER NULL \
	)'
)

"""
INSERT
"""
# EVENT CATAGORY
db.execute(
	"INSERT OR REPLACE INTO EventCatagory ( \
		EventCatagoryId, Description, UpdatedUtc, DataChanged) \
		VALUES \
		(1, 'CartAccessEvent', DATETIME('now'), 0), \
		(2, 'BPMEvent (Battery)', DATETIME('now'), 0), \
		(3, 'CANDeviceEvent', DATETIME('now'), 0), \
		(4, 'FaultEvent', DATETIME('now'), 0 \
	)"
)

# EVENT TYPE
db.execute(
	"INSERT OR REPLACE INTO EventType VALUES \
	(1, 1, 'Login', DATETIME('now'), 0), \
	(2, 1, 'Logout', DATETIME('now'), 0), \
	(3, 1, 'DrawerLock', DATETIME('now'), 0), \
	(4, 1, 'DrawerUnlock', DATETIME('now'), 0), \
	(5, 1, 'FailedLogin', DATETIME('now'), 0), \
	(6, 1, 'Lockout', DATETIME('now'), 0), \
	(7, 2, 'Unplugged', DATETIME('now'), 0), \
	(8, 2, 'PluggedIn', DATETIME('now'), 0), \
	(9, 2, 'Charging', DATETIME('now'), 0), \
	(10, 2, 'Discharging', DATETIME('now'), 0), \
	(11, 2, 'Disconnected', DATETIME('now'), 0), \
	(12, 2, 'DisconnectImminent', DATETIME('now'), 0), \
	(13, 2, 'Battery unavailable', DATETIME('now'), 0), \
	(14, 2, 'BulkCharge', DATETIME('now'), 0), \
	(15, 2, 'FloatCharge', DATETIME('now'), 0), \
	(16, 2, 'OverCharge', DATETIME('now'), 0), \
	(17, 2, 'TrickleCharge', DATETIME('now'), 0), \
	(18, 3, 'Unknown Error', DATETIME('now'), 0), \
	(19, 3, 'Corrupted packet data', DATETIME('now'), 0), \
	(20, 3, 'Device was reset', DATETIME('now'), 0), \
	(21, 3, 'Requested operation failed', DATETIME('now'), 0), \
	(22, 3, 'Power fault', DATETIME('now'), 0), \
	(23, 3, 'Unknown hardware fault', DATETIME('now'), 0), \
	(24, 3, 'Missing feedback', DATETIME('now'), 0), \
	(25, 3, 'Undercurrent fault', DATETIME('now'), 0), \
	(26, 3, 'Overcurrent fault', DATETIME('now'), 0), \
	(27, 3, 'Incorrect hardware fault', DATETIME('now'), 0), \
	(28, 3, 'Watchdog reset fault', DATETIME('now'), 0), \
	(29, 3, 'Brownout fault', DATETIME('now'), 0), \
	(30, 3, 'Unexpected software fault', DATETIME('now'), 0), \
	(31, 3, 'Not calibrated', DATETIME('now'), 0), \
	(32, 3, 'Invalid command data content', DATETIME('now'), 0), \
	(33, 3, 'Invalid command data length', DATETIME('now'), 0), \
	(34, 3, 'Busy', DATETIME('now'), 0), \
	(35, 4, 'Boot event', DATETIME('now'), 0), \
	(36, 4, 'TouchScreenReprogramming', DATETIME('now'), 0), \
	(37, 4, 'FailedLogin', DATETIME('now'), 0)"
)

# MESSAGE TYPE
db.execute(
	"INSERT OR REPLACE INTO MessageType VALUES \
	(1, 'Alert', DATETIME('now'), 0), \
	(14, 'Battery', DATETIME('now'), 0), \
	(15, 'IT-Cart', DATETIME('now'), 0), \
	(16, 'IT-Software', DATETIME('now'), 0), \
	(17, 'IT-HelpDesk', DATETIME('now'), 0), \
	(34, 'Request', DATETIME('now'), 0)"
)

# LANGUAGE
db.execute(
	"INSERT OR REPLACE INTO Language VALUES \
	(1, 'English', 'anglais', 'Englisch', 'eng', DATETIME('now'), 0)"
)

# TRANSLATION
db.execute(
	"INSERT OR REPLACE INTO Translation VALUES \
	(1, 1, '[SETTING_LanguageQuestion]', 'What language would you prefer?', DATETIME('now'), 0), \
	(2, 1, '[BATTERY_Voltage]', 'Voltage', DATETIME('now'), 0), \
	(3, 1, '[USER_FirstName]', 'First Name', DATETIME('now'), 0), \
	(4, 1, '[USER_MiddleName]', 'Middle Initial', DATETIME('now'), 0), \
	(5, 1, '[USER_LastName]', 'Last Name', DATETIME('now'), 0), \
	(6, 1, '[SETTING_Height]', 'Height', DATETIME('now'), 0), \
	(7, 1, '[SETTING_Brightness]', 'Brightness', DATETIME('now'), 0)"
)

# USER
db.execute(
	"INSERT OR REPLACE INTO User VALUES \
	(1, 'Steve', 'E', 'Rodgers', 'captain', '1F98708E787EE69B9D7460FAB3FD8ECD', 1, 1, DATETIME('now'), 0), \
	(2, 'Tony', 'H', 'Stark', 'ironman', 'CE9482DC5E095DBD34FAF38536A23EAF', 1, 1, DATETIME('now'), 0), \
	(3, 'Natalia', '', 'Romanova', 'blackwidow', '49DB0153857F86D9D8598DB3B1171F7E', 1, 1, DATETIME('now'), 0), \
	(4, 'Clint', '', 'Barton', 'hawkeye', '844E102979A0DA83949A1DB4926DAEC6', 1, 1, DATETIME('now'), 0), \
	(5, 'Bruce', '', 'Banner', 'thehulk', '1F98708E787EE69B9D7460FAB3FD8ECD', 1, 1, DATETIME('now'), 0), \
	(6, 'Thor', '', 'Odinson', 'lightninggod', 'F5C209B87BF164108721B935F7561539', 1, 1, DATETIME('now'), 0)"
)

# SETTING
db.execute(
	"INSERT OR REPLACE INTO Setting VALUES \
	(1, 'Height', '50', 1, DATETIME('now'), 0), \
	(2, 'Brightness', '50', 1, DATETIME('now'), 0), \
	(3, 'Battery Warning Level', '20', 0, DATETIME('now'), 0), \
	(4, 'CartName', 'LDWOW04', 0, DATETIME('now'), 0), \
	(5, 'ActiveCartUserID', '50', 0, DATETIME('now'), 0), \
	(6, 'CartAccessSessionID', '50', 0, DATETIME('now'), 0), \
	(7, 'PinLockoutAttempts', '5', 0, DATETIME('now'), 0), \
	(8, 'PinLockoutTimestamp', '2', 0, DATETIME('now'), 0), \
	(9, 'LockoutOverrideEnable', '1', 0, DATETIME('now'), 0), \
	(10, 'UTCOffsetMinutes', '0', 0, DATETIME('now'), 0), \
	(11, 'StatusBarShowUsername', '1', 0, DATETIME('now'), 0), \
	(12, 'StatusBarShowLogin', '1', 0, DATETIME('now'), 0), \
	(13, 'StatusBarShowCartname', '1', 0, DATETIME('now'), 0), \
	(14, 'DateTimeFormat', 'MM-dd-yy h:mm tt', 0, DATETIME('now'), 0), \
	(15, 'PartialPinTimeStamp', '3', 0, DATETIME('now'), 0), \
	(16, 'PINLength', '5', 0, DATETIME('now'), 0)"
)

# USER SETTING
db.execute(
	"INSERT OR REPLACE INTO UserSetting VALUES \
	(1, 2, 1, '62', DATETIME('now'), 0), \
	(2, 2, 2, '75', DATETIME('now'), 0), \
	(3, 5, 1, '100', DATETIME('now'), 0), \
	(4, 5, 2, '100', DATETIME('now'), 0), \
	(5, 3, 1, '35', DATETIME('now'), 0)"
)

# EVENT
db.execute(
	"INSERT OR REPLACE INTO Event VALUES \
	(1, DATETIME('now') - 3, 1), \
	(2, DATETIME('now') - 3, 9), \
	(3, DATETIME('now') - 3, 35), \
	(4, DATETIME('now') - 2, 5), \
	(5, DATETIME('now') - 2, 5), \
	(6, DATETIME('now') - 1, 23)"
)

# EVENT DETAILS
db.execute(
	"INSERT OR REPLACE INTO EventDetail VALUES \
	(1, 1, 'login lightninggod', 'Successful login'), \
	(2, 2, 'Charging', 'lightninggod started charging'), \
	(3, 2, 'Charging', 'lightninggod battery at 5000%'), \
	(4, 3, 'Bootup', 'System Restart'), \
	(5, 4, 'Failed Login', 'thehulk fat fingered'), \
	(6, 5, 'Failed Login', 'thehulk fat fingered'), \
	(7, 6, 'login ironman', 'Incompatable device')"
)

# MESSAGE QUEUE
db.execute(
	"INSERT OR REPLACE INTO MessageQueue VALUES \
	( \
		1, 'IT department staff', 0, 6, 'Charging', \
		'Please refrane from charging cart batteries yourself. Allow another member to plug the unit in themselves.', \
		-1, 5000, '2019-01-01 12:30:00.001', null, null, 15, DATETIME('now'), 0 \
	)"
)

db.commit()