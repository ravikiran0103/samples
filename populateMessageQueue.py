from pysqlcipher3 import dbapi2 as sqlcipher
import time


db = sqlcipher.connect('capsaDB.db')
cur = db.cursor()

cur.execute('pragma key="c@ps@"')

# delete table if already exists
cur.execute("DROP TABLE IF EXISTS MessageQueue")

# create table
cur.execute(
	"CREATE TABLE MessageQueue( \
		MessageQueueId INTEGER, \
		AlternateKey INTEGER PRIMARY KEY AUTOINCREMENT, \
		SenderName Text NOT NULL, \
		ReceiverName Text NOT NULL, \
		SenderTypeId INTEGER NOT NULL, \
		ReceiverTypeId INTEGER NOT NULL, \
		SenderId INTEGER NOT NULL, \
		UserId INTEGER NOT NULL, \
		Subject TEXT NULL, \
		Body TEXT NULL, \
		BackgroundColor INTEGER NOT NULL, \
		ForegroundColor INTEGER NOT NULL, \
		SentDateTime TEXT NULL, \
		ReceivedDateTime TEXT NULL, \
		ExpirationDateTime TEXT NULL, \
		MessageTypeId INTEGER NOT NULL, \
		UpdatedUtc TEXT NULL, \
		DataChanged INTEGER NULL, \
		AlertPopped INTEGER NULL, \
		FOREIGN KEY(UserId) REFERENCES User(UserId) ON DELETE CASCADE, \
		FOREIGN KEY(MessageTypeId) REFERENCES MessageType(MessageTypeId) ON DELETE CASCADE \
	)"
)


# insert dummy values for NOTIFICATIONS
for i in range(0, 50, 2):

	max_val = 0

	query = "SELECT IFNULL(MAX(MessageQueueId), 0) + 1 FROM MessageQueue"
	result = cur.execute(query)
	if result != None:
		records = cur.fetchone()
		max_val = records[0]

	sub = 'TEST SUB - ' + str(i)
	body = 'TEST BODY - ' + str(i)
	sub1 = 'TEST SUB - ' + str(i + 1)
	body1 = 'TEST BODY - ' + str(i + 1)

	query = 'INSERT OR REPLACE INTO MessageQueue VALUES' + \
		"(?, null, 'House Keeping', 'NSIGHT', 1, 0, 100, 96, ?, \
		?, 4284322279, 4278190080, '2019-01-01 12:30:00.001', \
		'2019-01-01 12:30:00.001', '2022-01-01 12:30:00.001', 0, \
		'2019-04-30 10:54:39', 1, 0), \
		(?, null, 'IT', 'JEFF', 0, 1, 100, 8, ?, ?, \
		4284322279, 4278190080, '2019-01-01 12:30:00.001', '2019-01-01 12:30:00.001', \
		'2022-01-01 12:30:00.001', 0, '2019-04-30 10:54:39', 0, 0)"

	params = [max_val, sub, body, max_val + 1, sub1, body1]
	cur.execute(query, params)


# insert dummy values for ALERTS
for i in range(50, 100, 2):

	max_val = 0

	query = "SELECT IFNULL(MAX(MessageQueueId), 0) + 1 FROM MessageQueue"
	result = cur.execute(query)
	if result != None:
		records = cur.fetchone()
		max_val = records[0]

	sub = 'TEST SUB - ' + str(i)
	body = 'TEST BODY - ' + str(i)
	sub1 = 'TEST SUB - ' + str(i + 1)
	body1 = 'TEST BODY - ' + str(i + 1)

	query = 'INSERT OR REPLACE INTO MessageQueue VALUES' + \
		"(?, null, 'House Keeping', 'NSIGHT', 1, 0, 100, 96, ?, \
		?, 4284322279, 4278190080, '2019-01-01 12:30:00.001', \
		'2019-01-01 12:30:00.001', '2022-01-01 12:30:00.001', 1, \
		'2019-04-30 10:54:39', 1, 0), \
		(?, null, 'IT', 'JEFF', 0, 1, 100, 8, ?, ?, \
		4284322279, 4278190080, '2019-01-01 12:30:00.001', '2019-01-01 12:30:00.001', \
		'2022-01-01 12:30:00.001', 1, '2019-04-30 10:54:39', 0, 0)"
	
	params = [max_val, sub, body, max_val + 1, sub1, body1]
	cur.execute(query, params)


# insert dummy service requests
for i in range(100, 250, 3):

	max_val = 0

	query = "SELECT IFNULL(MAX(MessageQueueId), 0) + 1 FROM MessageQueue"
	result = cur.execute(query)
	if result != None:
		records = cur.fetchone()
		max_val = records[0]

	sub = 'TEST SUB - ' + str(i)
	body = 'TEST BODY - ' + str(i)
	sub1 = 'TEST SUB - ' + str(i + 1)
	body1 = 'TEST BODY - ' + str(i + 1)
	sub2 = 'TEST SUB - ' + str(i + 2)
	body2 = 'TEST BODY - ' + str(i + 2)

	query = 'INSERT OR REPLACE INTO MessageQueue VALUES' + \
		"(?, null, 'LDWOW04', 'Battery', 1, 0, 96, 2, ?, \
		?, 4284322279, 4278190080, '2019-01-01 12:30:00.001', \
		'2019-01-01 12:30:00.001', '2022-01-01 12:30:00.001', 17, \
		'2019-04-30 10:54:39', 0, 0), \
		(?, null, 'Jeff', 'IT-Cart', 0, 1, 8, 96, ?, \
		?, 4284322279, 4278190080, '2019-01-01 12:30:00.001', \
		'2019-01-01 12:30:00.001', \
		'2022-01-01 12:30:00.001', 16, '2019-04-30 10:54:39', 0, 0), \
		(?, null, 'LDWOW04', 'Battery', 1, 0, 96, 0, ?, \
		?, 4284322279, 4278190080, '2019-01-01 12:30:00.001', \
		'2019-01-01 12:30:00.001', '2022-01-01 12:30:00.001', 17, \
		'2019-04-30 10:54:39', 0, 0)"
	
	params = [
		max_val, sub, body,
		max_val + 1, sub1, body1,
		max_val + 2, sub2, body2
	]
	cur.execute(query, params)


db.commit()
