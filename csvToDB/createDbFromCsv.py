from pysqlcipher3 import dbapi2 as sqlcipher
import time
import csv

from Crypto.Cipher import AES
import binascii
import math

db = sqlcipher.connect('capsaDB.db')
db.execute('pragma key="c@ps@"')

KEY = binascii.unhexlify('BE0135F28F01AE69C608A869D13812E1')
IV = binascii.unhexlify('BBD72E890181D83E0049BB32A95A2CB0')

line_count = 0

def determine_str(value):
	try:
		int(value)
	except ValueError:
		return True
	else:
		return False


# USERS TABLE
db.execute(
	'create table Users( \
		UserID int NOT NULL PRIMARY KEY, \
		PIN nvarchar(40) NOT NULL, \
		UserName nvarchar(50) NOT NULL, \
		ITServicesMenuEnable bit NOT NULL, \
		LockoutOverrideEnable bit NOT NULL, \
		DrawerAccessEnable bit NOT NULL, \
		Active bit NOT NULL, \
		ScreenBrightness tinyint NOT NULL, \
		VolumeLevel tinyint NOT NULL, \
		DrawerUnlockTimeSpan smallint NOT NULL, \
		LoginTimeout smallint NOT NULL, \
		LiftPresetStanding tinyint NOT NULL, \
		LiftPresetSitting tinyint NOT NULL, \
		UpdateTimestamp datetime NOT NULL, \
		DataChanged bit NOT NULL DEFAULT 0 \
	)'
)
with open('users.csv', 'a') as writeFile:
	writer = csv.writer(writeFile)
	for line in open("UsersData.csv"):
		line = line.strip()
		csv_row = line.split(';')
		csv_row.pop()
		query = 'insert into Users(UserID, PIN, UserName, ITServicesMenuEnable, LockoutOverrideEnable, DrawerAccessEnable, \
			Active, ScreenBrightness, VolumeLevel, DrawerUnlockTimeSpan, LoginTimeout, LiftPresetStanding, LiftPresetSitting, \
				UpdateTimestamp, DataChanged) values ('
		for col in range(len(csv_row)):
			element = csv_row[col]
			if determine_str(element):
				if element == 'True':
					element = '1'
				elif element == 'False':
					element = '0'
				else:
					if "'" in element:
						element = '"' + element + '"'
					else:
						element = "'" + element + "'"
			query += element
			if col < len(csv_row) - 1:
				query += ", "
			else:
				query += ")"

		element = str(csv_row[1])
		element = element.replace(" ", "")
		rijnd = AES.new(KEY, AES.MODE_CBC, IV)
		pt = rijnd.decrypt(binascii.unhexlify(element))
		pin = pt.decode('utf-8')
		print(pt)
		print("PT", pin)
		print(len(pin))
		writer.writerow([str(csv_row[1]), pin, str(csv_row[2])])

		db.execute(query)
		line_count += 1
	writeFile.close()
print(f'Processed {line_count} lines.')
line_count = 0
print("UPDATED USERS TABLE")
print("")


# WIRELESS NETWORKS TABLE
db.execute(
	'create table WirelessNetworks( \
		WirelessNetworkID int NOT NULL PRIMARY KEY, \
		SSID nvarchar(32) NOT NULL, \
		Encryption nvarchar(50) NOT NULL, \
		Authentication nvarchar(50) NOT NULL, \
		NetworkKey nchar(50), \
		KeyIndex nvarchar(50) NOT NULL, \
		EAPType nvarchar(50) NOT NULL, \
		WirelessNetworkName nvarchar(50), \
		UpdateTimestamp datetime NOT NULL, \
		EightZeroTwoAuthenticationEnable bit NOT NULL \
	)'
)
for line in open("WirelessNetworksData.csv"):
	line = line.strip()
	csv_row = line.split(';')
	csv_row.pop()
	query = 'insert into WirelessNetworks(WirelessNetworkID, SSID, Encryption, Authentication, NetworkKey, KeyIndex, EAPType, \
		WirelessNetworkName, UpdateTimestamp, EightZeroTwoAuthenticationEnable) values ('
	for col in range(len(csv_row)):
		element = csv_row[col]
		if determine_str(element):
			if element == 'True':
				element = '1'
			elif element == 'False':
				element = '0'
			else:
				if "'" in element:
					element = '"' + element + '"'
				else:
					element = "'" + element + "'"
		query += element
		if col < len(csv_row) - 1:
			query += ", "
		else:
			query += ")"
	db.execute(query)
	line_count += 1
print(f'Processed {line_count} lines.')
line_count = 0
print("UPDATED WIRELESS NETWORKS TABLE")		
print("")


# POWER TRENDS
db.execute(
	'create table PowerTrends( \
		UpdateTimestamp datetime NOT NULL, \
		AbsoluteCycles smallint NOT NULL, \
		AgedCapacity tinyint NOT NULL, \
		BatteryCurrent float NOT NULL, \
		BatteryLevel tinyint NOT NULL, \
		BatteryTemp float NOT NULL, \
		BatteryVoltage float NOT NULL, \
		BPMCurrent float NOT NULL, \
		BPMTemp float NOT NULL, \
		BPMVoltage float NOT NULL, \
		CummulativeCycles smallint NOT NULL, \
		CycleLifeDerating smallint NOT NULL, \
		DischargeDeratedCapacity tinyint NOT NULL, \
		DischargeDeratedPercent tinyint NOT NULL, \
		FanState bit NOT NULL, \
		RatedCycles smallint NOT NULL, \
		RoomTemp float NOT NULL, \
		StandardCapacity tinyint NOT NULL, \
		PluggedIn bit NOT NULL \
	)'
)
for line in open("PowerTrendsData.csv"):
	line = line.strip()
	csv_row = line.split(';')
	csv_row.pop()
	query = 'insert into PowerTrends(UpdateTimestamp, AbsoluteCycles, AgedCapacity, BatteryCurrent, BatteryLevel, BatteryTemp, \
		BatteryVoltage, BPMCurrent, BPMTemp, BPMVoltage, CummulativeCycles, CycleLifeDerating, DischargeDeratedCapacity, DischargeDeratedPercent, \
			FanState, RatedCycles, RoomTemp, StandardCapacity, PluggedIn) values ('
	for col in range(len(csv_row)):
		element = csv_row[col]
		if determine_str(element):
			if element == 'True':
				element = '1'
			elif element == 'False':
				element = '0'
			else:
				if "'" in element:
					element = '"' + element + '"'
				else:
					element = "'" + element + "'"
		query += element
		if col < len(csv_row) - 1:
			query += ", "
		else:
			query += ")"
	db.execute(query)
	line_count += 1
print(f'Processed {line_count} lines.')
line_count = 0
print("UPDATED POWER TRENDS TABLE")		
print("")		


# MESSAGE TYPES
db.execute(
	'create table MessageTypes( \
		MessageTypeID int NOT NULL PRIMARY KEY, \
		UpdateTimestamp datetime NOT NULL, \
		Description nvarchar(50) NOT NULL \
	)'
)
for line in open("MessageTypesData.csv"):
	line = line.strip()
	csv_row = line.split(';')
	csv_row.pop()
	query = 'insert into MessageTypes(MessageTypeID, UpdateTimestamp, Description) values ('
	for col in range(len(csv_row)):
		element = csv_row[col]
		if determine_str(element):
			if element == 'True':
				element = '1'
			elif element == 'False':
				element = '0'
			else:
				if "'" in element:
					element = '"' + element + '"'
				else:
					element = "'" + element + "'"
		query += element
		if col < len(csv_row) - 1:
			query += ", "
		else:
			query += ")"
	db.execute(query)
	line_count += 1
print(f'Processed {line_count} lines.')
line_count = 0
print("UPDATED MESSAGE TYPES TABLE")		
print("")


# MESSAGE QUEUE
db.execute(
	'create table MessageQueue( \
		MessageID int NOT NULL, \
		SenderID int NOT NULL, \
		SenderTypeID tinyint NOT NULL, \
		ReceiverID int NOT NULL, \
		ReceiverTypeID tinyint NOT NULL, \
		Subject nvarchar(50), \
		Body nvarchar(500), \
		BackgroundColor int NOT NULL, \
		ForegroundColor int NOT NULL, \
		SentTimestamp datetime, \
		ReceivedTimestamp datetime, \
		AcknowledgedTimestamp datetime, \
		ExpirationTimestamp datetime, \
		UpdateTimestamp datetime NOT NULL, \
		SeviceRequestSent bit NOT NULL DEFAULT 0, \
		SenderName nvarchar(100), \
		ReceiverName nvarchar(100), \
		MessageTypeID int NOT NULL, \
		AlertPopped bit NOT NULL \
	)'
)
for line in open("MessageQueueData.csv"):
	line = line.strip()
	csv_row = line.split(';')
	csv_row.pop()
	query = 'insert into MessageQueue(MessageID, SenderID, SenderTypeID, ReceiverID , ReceiverTypeID, Subject, Body, \
		BackgroundColor, ForegroundColor, SentTimestamp, ReceivedTimestamp, AcknowledgedTimestamp, ExpirationTimestamp, UpdateTimestamp, \
			SeviceRequestSent, SenderName, ReceiverName, MessageTypeID, AlertPopped) values ('
	for col in range(len(csv_row)):
		element = csv_row[col]
		if determine_str(element):
			if element == 'True':
				element = '1'
			elif element == 'False':
				element = '0'
			else:
				if "'" in element:
					element = '"' + element + '"'
				else:
					element = "'" + element + "'"
		query += element
		if col < len(csv_row) - 1:
			query += ", "
		else:
			query += ")"
	db.execute(query)
	line_count += 1
print(f'Processed {line_count} lines.')
line_count = 0
print("UPDATED MESSAGE QUEUE TABLE")		
print("")			


# PERSISTENT VARIABLES
db.execute(
	'create table PersistentVariables( \
		CartAccessSessionID datetime, \
		ID tinyint  NOT NULL PRIMARY KEY, \
		ActiveCartUserID int NOT NULL, \
		DrawersUnlocked bit NOT NULL, \
		UtcOffsetMinutes int NOT NULL \
	)'
)
for line in open("PersistentVariablesData.csv"):
	line = line.strip()
	csv_row = line.split(';')
	csv_row.pop()
	query = 'insert into PersistentVariables(CartAccessSessionID, ID, ActiveCartUserID, DrawersUnlocked, UtcOffsetMinutes) \
		values ('
	for col in range(len(csv_row)):
		element = csv_row[col]
		if determine_str(element):
			if element == 'True':
				element = '1'
			elif element == 'False':
				element = '0'
			else:
				if "'" in element:
					element = '"' + element + '"'
				else:
					element = "'" + element + "'"
		query += element
		if col < len(csv_row) - 1:
			query += ", "
		else:
			query += ")"

	db.execute(query)
	line_count += 1
print(f'Processed {line_count} lines.')
line_count = 0
print("UPDATED PERSISTENT VARIABLES TABLE")		
print("")


# FAULT EVENTS
db.execute(
	'create table FaultEvents( \
		FaultCodeID smallint NOT NULL, \
		UpdateTimestamp datetime NOT NULL \
	)'
)
for line in open("FaultEventsData.csv"):
	line = line.strip()
	csv_row = line.split(';')
	csv_row.pop()
	query = 'insert into FaultEvents(FaultCodeID, UpdateTimestamp) values ('
	for col in range(len(csv_row)):
		element = csv_row[col]
		if determine_str(element):
			if element == 'True':
				element = '1'
			elif element == 'False':
				element = '0'
			else:
				if "'" in element:
					element = '"' + element + '"'
				else:
					element = "'" + element + "'"
		query += element
		if col < len(csv_row) - 1:
			query += ", "
		else:
			query += ")"
	db.execute(query)
	line_count += 1
print(f'Processed {line_count} lines.')
line_count = 0
print("UPDATED FAULT EVENTS TABLE")		
print("")


# FACILITIES
db.execute(
	'create table Facilities( \
		FacilityID int NOT NULL PRIMARY KEY, \
		DisplayName nvarchar(50), \
		HelpScreenText nvarchar(100), \
		PinLockoutAttempts tinyint NOT NULL, \
		PinLockoutTimeSpan tinyint NOT NULL, \
		PinLength tinyint NOT NULL, \
		PartialPinTimeSpan tinyint NOT NULL, \
		PinMaskingTimeSpan tinyint NOT NULL, \
		DrawerUnlockMaxTimeSpan smallint NOT NULL, \
		LoginMaxTimeout smallint NOT NULL, \
		InactiveRedirectTimeSpan tinyint NOT NULL, \
		CalculatorRequiresLogin bit NOT NULL, \
		LightsRequireLogin bit NOT NULL, \
		LiftRequiresLogin bit NOT NULL, \
		NotificationRequiresLogin bit NOT NULL, \
		HelpRequiresLogin bit NOT NULL, \
		ServiceRequestsRequiresLogin bit NOT NULL, \
		ServiceRequestMessageTimeout tinyint NOT NULL, \
		UpdateTimestamp datetime NOT NULL, \
		SettingsRequiresLogin bit NOT NULL \
	)'
)
for line in open("FacilitiesData.csv"):
	line = line.strip()
	csv_row = line.split(';')
	csv_row.pop()
	query = 'insert into Facilities(FacilityID, DisplayName, HelpScreenText, PinLockoutAttempts, PinLockoutTimeSpan, PinLength, \
		PartialPinTimeSpan, PinMaskingTimeSpan, DrawerUnlockMaxTimeSpan, LoginMaxTimeout, InactiveRedirectTimeSpan, CalculatorRequiresLogin, \
			LightsRequireLogin, LiftRequiresLogin, NotificationRequiresLogin, HelpRequiresLogin, ServiceRequestsRequiresLogin, \
				ServiceRequestMessageTimeout, UpdateTimestamp, SettingsRequiresLogin) values ('
	for col in range(len(csv_row)):
		element = csv_row[col]
		if determine_str(element):
			if element == 'True':
				element = '1'
			elif element == 'False':
				element = '0'
			else:
				if "'" in element:
					element = '"' + element + '"'
				else:
					element = "'" + element + "'"
		query += element
		if col < len(csv_row) - 1:
			query += ", "
		else:
			query += ")"
	db.execute(query)
	line_count += 1
print(f'Processed {line_count} lines.')
line_count = 0
print("UPDATED FACILITIES TABLE")		
print("")


# DEPARTMENTS
db.execute(
	'create table Departments( \
		DepartmentID int NOT NULL PRIMARY KEY, \
		DepartmentName nvarchar(50) NOT NULL, \
		UpdateTimestamp datetime NOT NULL \
	)'
)
for line in open("DepartmentsData.csv"):
	line = line.strip()
	csv_row = line.split(';')
	csv_row.pop()
	query = 'insert into Departments(DepartmentID, DepartmentName, UpdateTimestamp) values ('
	for col in range(len(csv_row)):
		element = csv_row[col]
		if determine_str(element):
			if element == 'True':
				element = '1'
			elif element == 'False':
				element = '0'
			else:
				if "'" in element:
					element = '"' + element + '"'
				else:
					element = "'" + element + "'"
		query += element
		if col < len(csv_row) - 1:
			query += ", "
		else:
			query += ")"
	db.execute(query)
	line_count += 1
print(f'Processed {line_count} lines.')
line_count = 0
print("UPDATED DEPARTMENTS TABLE")		
print("")


# CART ACCESS EVENTS
db.execute(
	'create table CartAccessEvents( \
		UserID int NOT NULL, \
		CartAccessEventType tinyint NOT NULL, \
		UpdateTimestamp datetime NOT NULL, \
		SessionID datetime NOT NULL \
	)'
)
for line in open("CartAccessEventsData.csv"):
	line = line.strip()
	csv_row = line.split(';')
	csv_row.pop()
	query = 'insert into CartAccessEvents(UserID, CartAccessEventType, UpdateTimestamp, SessionID) values ('
	for col in range(len(csv_row)):
		element = csv_row[col]
		if determine_str(element):
			if element == 'True':
				element = '1'
			elif element == 'False':
				element = '0'
			else:
				if "'" in element:
					element = '"' + element + '"'
				else:
					element = "'" + element + "'"
		query += element
		if col < len(csv_row) - 1:
			query += ", "
		else:
			query += ")"
	db.execute(query)
	line_count += 1
print(f'Processed {line_count} lines.')
line_count = 0
print("UPDATED CART ACCESS EVENTS TABLE")		
print("")


# BPM EVENTS
db.execute(
	'create table BPMEvents( \
		BPMEventCode int NOT NULL, \
		UpdateTimestamp datetime NOT NULL \
	)'
)
for line in open("BPMEventsData.csv"):
	line = line.strip()
	csv_row = line.split(';')
	csv_row.pop()
	query = 'insert into BPMEvents(BPMEventCode, UpdateTimestamp) values ('
	for col in range(len(csv_row)):
		element = csv_row[col]
		if determine_str(element):
			if element == 'True':
				element = '1'
			elif element == 'False':
				element = '0'
			else:
				if "'" in element:
					element = '"' + element + '"'
				else:
					element = "'" + element + "'"
		query += element
		if col < len(csv_row) - 1:
			query += ", "
		else:
			query += ")"
	db.execute(query)
	line_count += 1
print(f'Processed {line_count} lines.')
line_count = 0
print("UPDATED BPM EVENTS TABLE")		
print("")


# BATTERY WARNINGS
db.execute(
	'create table BatteryWarnings( \
		BatteryWarningID int NOT NULL PRIMARY KEY, \
		WarningLevelType nvarchar(50) NOT NULL, \
		WarningLevel smallint NOT NULL, \
		WarningDescription nvarchar(140) NOT NULL, \
		WarningFlashingEnable bit NOT NULL, \
		UpdateTimestamp datetime NOT NULL \
	)'
)
for line in open("BatteryWarningsData.csv"):
	line = line.strip()
	csv_row = line.split(';')
	csv_row.pop()
	query = 'insert into BatteryWarnings(BatteryWarningID, WarningLevelType, WarningLevel, WarningDescription, \
		WarningFlashingEnable, UpdateTimestamp) values ('
	for col in range(len(csv_row)):
		element = csv_row[col]
		if determine_str(element):
			if element == 'True':
				element = '1'
			elif element == 'False':
				element = '0'
			else:
				if "'" in element:
					element = '"' + element + '"'
				else:
					element = "'" + element + "'"
		query += element
		if col < len(csv_row) - 1:
			query += ", "
		else:
			query += ")"
	db.execute(query)
	line_count += 1
print(f'Processed {line_count} lines.')
line_count = 0
print("UPDATED BATTERY WARNING TABLE")		
print("")


# CART PROFILES
db.execute(
	'create table CartProfiles( \
		CartProfileID int NOT NULL PRIMARY KEY, \
		CalculatorEnable bit NOT NULL, \
		LightsEnable bit NOT NULL, \
		NotificationAlertsEnable bit NOT NULL, \
		NotificationMessagesEnable bit NOT NULL, \
		PreferencesEnable bit NOT NULL, \
		ServiceRequestsEnable bit NOT NULL, \
		StatusBarDateTimeFormat nvarchar(50) NOT NULL, \
		StatusBarShowCartGroup bit NOT NULL, \
		StatusBarShowCartName bit NOT NULL, \
		StatusBarShowLogin bit NOT NULL, \
		StatusBarShowUserName bit NOT NULL, \
		LightsKeyboardBrightness tinyint NOT NULL, \
		LightsWorkSurfaceBrightness tinyint NOT NULL, \
		LightsGroundBrightness tinyint NOT NULL, \
		BatteryLevelRed tinyint NOT NULL, \
		BatteryLevelYellow tinyint NOT NULL, \
		UpdateTimestamp datetime NOT NULL, \
		FacilityID int NOT NULL \
	)'
)
for line in open("CartProfilesData.csv"):
	line = line.strip()
	csv_row = line.split(';')
	csv_row.pop()
	query = 'insert into CartProfiles(CartProfileID, CalculatorEnable, LightsEnable, NotificationAlertsEnable, NotificationMessagesEnable, \
		PreferencesEnable, ServiceRequestsEnable, StatusBarDateTimeFormat, StatusBarShowCartGroup, StatusBarShowCartName, StatusBarShowLogin, \
			StatusBarShowUserName, LightsKeyboardBrightness, LightsWorkSurfaceBrightness, LightsGroundBrightness, BatteryLevelRed, \
				BatteryLevelYellow, UpdateTimestamp, FacilityID) values ('
	for col in range(len(csv_row)):
		element = csv_row[col]
		if determine_str(element):
			if element == 'True':
				element = '1'
			elif element == 'False':
				element = '0'
			else:
				if "'" in element:
					element = '"' + element + '"'
				else:
					element = "'" + element + "'"
		query += element
		if col < len(csv_row) - 1:
			query += ", "
		else:
			query += ")"
	db.execute(query)
	line_count += 1
print(f'Processed {line_count} lines.')
line_count = 0
print("UPDATED CART PROFILES TABLE")		
print("")


# CART GROUPS
db.execute(
	'create table CartGroups( \
		CartGroupID int NOT NULL PRIMARY KEY, \
		CartGroupName nvarchar(50) NOT NULL, \
		UpdateTimestamp datetime NOT NULL \
	)'
)
for line in open("CartGroupsData.csv"):
	line = line.strip()
	csv_row = line.split(';')
	csv_row.pop()
	query = 'insert into CartGroups(CartGroupID, CartGroupName, UpdateTimestamp) values ('
	for col in range(len(csv_row)):
		element = csv_row[col]
		if determine_str(element):
			if element == 'True':
				element = '1'
			elif element == 'False':
				element = '0'
			else:
				if "'" in element:
					element = '"' + element + '"'
				else:
					element = "'" + element + "'"
		query += element
		if col < len(csv_row) - 1:
			query += ", "
		else:
			query += ")"
	db.execute(query)
	line_count += 1
print(f'Processed {line_count} lines.')
line_count = 0
print("UPDATED CART GROUPS TABLE")		
print("")


# CARTS
db.execute(
	'create table Carts( \
		CartID int NOT NULL PRIMARY KEY, \
		CartName nvarchar(50) NOT NULL, \
		UpdateTimestamp datetime NOT NULL, \
		CartSerialNumber nvarchar(13), \
		Active bit, \
		InstallationDate datetime, \
		BatteryFirmwareVersion nvarchar(20), \
		BatteryPartNumber nvarchar(8), \
		BatterySerialNumber nvarchar(40), \
		BPMFirmwareVersion nvarchar(20), \
		BPMPartNumber nvarchar(8), \
		BPMSerialNumber nvarchar(10), \
		BSPVersion nvarchar(20), \
		CartCoreVersion nvarchar(20), \
		CartGUIVersion nvarchar(20), \
		CartManufactureDate datetime, \
		CartPartNumber nvarchar(13), \
		CCSPartNumber nvarchar(8), \
		CCSSerialNumber nvarchar(10), \
		DBFirmwareVersion nvarchar(20), \
		DBPartNumber nvarchar(8), \
		DBSerialNumber nvarchar(10), \
		DiagnosticsVersion nvarchar(20), \
		DisplayPartNumber nvarchar(8), \
		DisplaySerialNumber nvarchar(10), \
		HALVersion nvarchar(20), \
		KernelVersion nvarchar(20), \
		LeftInverterPartNumber nvarchar(8), \
		LeftInverterSerialNumber nvarchar(10), \
		RightInverterPartNumber nvarchar(8), \
		RightInverterSerialNumber nvarchar(10), \
		PFFirmwareVersion nvarchar(20), \
		PFPartNumber nvarchar(8), \
		PFSerialNumber nvarchar(10), \
		ServicePackVersion nvarchar(20), \
		WifiFeatureEnable bit NOT NULL DEFAULT 0, \
		BatteryParamVCutoff smallint, \
		BatteryParamVRecovery smallint, \
		BatteryParamTRecovery smallint, \
		BatteryParamIBulk smallint, \
		BatteryParamVOvercharge smallint, \
		BatteryParamIOvercharge smallint, \
		BatteryParamVFloat1 smallint, \
		BatteryParamVFloat2 smallint, \
		BatteryParamTOvercharge smallint, \
		BatteryParamTFloat1 smallint, \
		BatteryParamRatedCycles smallint, \
		BatteryInstallationDate datetime, \
		BatteryWarrantyDate datetime, \
		WirelessMAC nvarchar(17), \
		WiredMAC nvarchar(17), \
		LastAccessPointMAC nvarchar(17), \
		DataChanged bit NOT NULL DEFAULT 0, \
		TotalRAMUsage float, \
		BatteryParamIRecovery smallint, \
		CartProfileID int NOT NULL, \
		UrlSync nvarchar(200), \
		UrlProvider nvarchar(200), \
		UrlReceiver nvarchar(200), \
		UrlServicePack nvarchar(200), \
		PowerModuleType tinyint DEFAULT 0, \
		LastAccessPointTimestamp datetime \
	)'
)
for line in open("Carts.csv"):
	line = line.strip()
	csv_row = line.split(';')
	csv_row.pop()
	query = 'insert into Carts(CartID, CartName, UpdateTimestamp, CartSerialNumber, Active, InstallationDate, BatteryFirmwareVersion, \
		BatteryPartNumber, BatterySerialNumber, BPMFirmwareVersion, BPMPartNumber, BPMSerialNumber, BSPVersion, CartCoreVersion, CartGUIVersion, \
			CartManufactureDate, CartPartNumber, CCSPartNumber, CCSSerialNumber, DBFirmwareVersion, DBPartNumber, DBSerialNumber, \
				DiagnosticsVersion, DisplayPartNumber, DisplaySerialNumber, HALVersion, KernelVersion, LeftInverterPartNumber, LeftInverterSerialNumber, \
					RightInverterPartNumber, RightInverterSerialNumber, PFFirmwareVersion, PFPartNumber, PFSerialNumber, ServicePackVersion, \
						WifiFeatureEnable, BatteryParamVCutoff, BatteryParamVRecovery, BatteryParamTRecovery, BatteryParamIBulk, BatteryParamVOvercharge, \
							BatteryParamIOvercharge, BatteryParamVFloat1, BatteryParamVFloat2, BatteryParamTOvercharge, BatteryParamTFloat1, BatteryParamRatedCycles, \
								BatteryInstallationDate, BatteryWarrantyDate, WirelessMAC, WiredMAC, LastAccessPointMAC, DataChanged, TotalRAMUsage, \
									BatteryParamIRecovery, CartProfileID, UrlSync, UrlProvider, UrlReceiver, UrlServicePack, PowerModuleType, \
										LastAccessPointTimestamp) values ('
	for col in range(len(csv_row)):
		element = csv_row[col]
		if determine_str(element):
			if element == 'True':
				element = '1'
			elif element == 'False':
				element = '0'
			else:
				if "'" in element:
					element = '"' + element + '"'
				else:
					element = "'" + element + "'"
		query += element
		if col < len(csv_row) - 1:
			query += ", "
		else:
			query += ")"
	db.execute(query)
	line_count += 1
print(f'Processed {line_count} lines.')
line_count = 0
print("UPDATED CARTS TABLE")		
print("")

db.commit()
