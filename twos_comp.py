# def twos_comp(val, bits):
#     """compute the 2's complement of int value val"""
#     if (val & (1 << (bits - 1))) != 0: # if sign bit is set e.g., 8bit: 128-255
#         val = val - (1 << bits)        # compute negative value
#     return val


def flip(c): 
    return '1' if (c == '0') else '0'


# Print 1's and 2's complement of  
# binary number represented by "bin"  
def printOneAndTwosComplement(binary): 

	n = len(binary)
	ones = ""
	twos = ""
	
	# for ones complement flip every bit
	for i in range(n):
		ones += flip(binary	[i])

	print(~int(ones,2))
		
	# # for two's complement go from right
	# # to left in ones complement and if
	# # we get 1 make, we make them 0 and
	# # keep going left when we get first
	# # 0, make that 1 and go out of loop
	# ones = list(ones.strip(""))
	# twos = list(ones)
	# for i in range(n - 1, -1, -1):
	# 	if (ones[i] == '1'):
	# 		twos[i] = '0'
	# 	else:
	# 		twos[i] = '1'
	# 		break
		
	# # If No break : all are 1 as in 111 or 11111
	# # in such case, add extra 1 at beginning
	# if (i == -1):
	# 	twos.insert(0, '1')
		
	# print("1's complement: ", *ones, sep = "")
	# print("2's complement: ", *twos, sep = "")


msb = 217
lsb = 0
current = (msb << 8) + lsb
binary = bin(current).replace("0b","")
printOneAndTwosComplement(binary)