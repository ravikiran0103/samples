class Test:

	def __init__(self):

		print("IN CONSTRUCTOR")

	def __del__(self):

		print("IN DESTRUCTOR")


if __name__ == "__main__":
	
	obj = Test()

	print("BEFORE NONE")
	obj = None
	print("AFTER NONE")