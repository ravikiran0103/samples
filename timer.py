import datetime
from threading import Timer

class test:
    def __init__(self):
        self.t = Timer(10, self.timeout)
        self.t.start()              # After 10 seconds, "Alarm!" will be printed

    def timeout(self):
        print("Alarm!")

if __name__ == "__main__":
    obj = test()
    x = datetime.datetime(2000, 1, 1)
    print(x)
    y = datetime.datetime.utcnow()
    print(y)