import QtQuick 2.0

Rectangle
{
    id: root
    color: "transparent"
    property alias text: text.text
    property alias fontColor: text.color
    property alias fontSize: text.font.pixelSize
    signal clicked
    width: 64
    height: 32
    radius: 5
    border { width: 2; color: "white" }
    Text
    {
        id: text
        anchors.centerIn: parent
        font.bold: true
    }
    MouseArea
    {
        anchors.fill: parent
        onClicked: {
            anim1.target = parent
            anim2.target = parent
            anim1.restart()
            anim2.restart()
            root.clicked()
        }
    }
    PropertyAnimation { id : anim1 ; property : "scale" ; from : 1 ; to : 0.9 ; duration : 250 }
    PropertyAnimation { id : anim2 ; property : "scale" ; from : 0.9 ; to : 1 ; duration : 250 }
}

