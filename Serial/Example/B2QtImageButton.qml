import QtQuick 2.0

Rectangle
{
    id: root
    property alias source: image.source
    signal clicked
    width: 64
    height: width
    color: "transparent"
    radius: width/3
    border { width: 2; color: "white" }
    Image
    {
        id: image
        anchors.fill: parent
        anchors.margins: root.width / 4
        fillMode: Image.PreserveAspectFit
    }
    MouseArea
    {
        id: mouseArea
        anchors.fill: parent
        onClicked: root.clicked()
    }
    states: State {
            name: "pressed"; when: mouseArea.pressed
            PropertyChanges { target: image; scale: 1.2 }
        }

        transitions: Transition {
            NumberAnimation { properties: "scale"; duration: 200; easing.type: Easing.InOutQuad }
        }
}

