import QtQuick 2.2

Rectangle
{
    property alias model: comboBox.items
    property alias textLabel: comboBox.label
    property alias selectedText: chosenItemText.text;
    property alias selectedIndex: listView.currentIndex
    signal selectionChanged

    id:comboBox
    property variant items
    property string textColor: "black"
    property string label
    width: 150;
    height: 30;
    z: 100;
    smooth: true;
    radius: 5
    color: "transparent"
    border { width: 2; color: "white" }
    Item
    {
        id: dropDownContainer
        anchors
        {
            right: parent.right
            top: parent.top
            bottom: parent.bottom
        }
        width: 48
        MouseArea
        {
            anchors.fill: parent;
            onClicked: comboBox.state = comboBox.state==="dropDown"?"":"dropDown"
        }
    }

    Image
    {
        id: downArrow
        anchors.fill: dropDownContainer
        anchors.margins: 10
        source: "qrc:/images/down.png"
        width: 32; height: 32
        fillMode: Image.PreserveAspectFit
        visible: true
    }
    Image
    {
        id: upArrow
        anchors.fill: dropDownContainer
        anchors.margins: 10
        source: "qrc:/images/up.png"
        width: 32; height: 32
        fillMode: Image.PreserveAspectFit
        visible: false
    }

    Rectangle
    {
        id:chosenItem
        anchors
        {
            left: comboBox.left
            top: comboBox.top
            bottom: comboBox.bottom
            right: downArrow.left
        }
        radius: comboBox.radius
        color: "transparent"
        smooth:true;
        Text
        {
            anchors.centerIn: parent
            id:chosenItemText
            text: comboBox.label
            color: "white"
            font.family: "Ubuntu"
            font.bold: true
            horizontalAlignment: Text.AlignRight
            font.pointSize: 12
            smooth:true
        }
    }

    Rectangle
    {
        id:dropDown
        width:comboBox.width;
        height:0;
        clip:true;
        radius: comboBox.radius
        anchors.top: chosenItem.bottom;
        anchors.margins: 2;
        border { width: 2; color: "white" }
        color: "white"
        z: 101

        ListView
        {
            id:listView
            currentIndex: -1
            anchors.fill: dropDown
            focus: true
            model: comboBox.items
            highlight: highlight
            delegate: Item {
                width:comboBox.width;
                height: comboBox.height;

                Text
                {
                    text: modelData
                    anchors.top: parent.top;
                    anchors.left: parent.left;
                    anchors.margins: 5;
                    color: comboBox.textColor
                    font.family: "Ubuntu"
                    elide: Text.ElideRight
                    horizontalAlignment: Text.AlignRight
                    font.pointSize: 12
                    smooth:true
                }

                MouseArea
                {
                    anchors.fill: parent;
                    onClicked:
                    {
                        var prevSelection = chosenItemText.text
                        chosenItemText.text = modelData
                        if(chosenItemText.text != prevSelection)
                        {
                            listView.currentIndex = index
                            comboBox.selectionChanged()
                        }
                        comboBox.state = ""
                    }
                }
            }
        }
    }

    Component
    {
        id: highlight
        Rectangle
        {
            width:comboBox.width - 10;
            height:comboBox.height;
            color: "lightgrey"
        }
    }

    states:
        [
        State
        {
            name: "dropDown";
            PropertyChanges { target: dropDown; height: comboBox.height * 3 }
            PropertyChanges { target: downArrow; visible: false }
            PropertyChanges { target: upArrow; visible: true }
        },
        State
        {
            name: "normal";
            PropertyChanges { target: dropDown; height: 0 }
            PropertyChanges { target: downArrow; visible: true }
            PropertyChanges { target: upArrow; visible: false }
        }
    ]

    transitions: Transition {
    NumberAnimation { target: dropDown; properties: "height"; easing.type: Easing.OutExpo; duration: 500 }
    }
}

