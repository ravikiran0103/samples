include(../defaults.pri)

QT       += core gui qml quick

TARGET = SerialExample

LIBS += -L../../lib/ -lB2QtSerial

TEMPLATE = app

SOURCES += main.cpp \

DISTFILES += \
    main.qml \
    B2QtComboBox.qml \
    B2QtTextButton.qml

RESOURCES += \
    main.qrc

unix {
    target.path = /usr/bin
    INSTALLS += target
}
