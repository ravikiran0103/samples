#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickView>
#include <QQmlContext>
#include <QtQml>

#include "seriallib.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    qmlRegisterType<SerialPort> ("SerialPort", 1, 0, "SerialPort");

    //QStringList ports = SerialPort::getAvailablePorts();
    QQuickView view;
    //view.rootContext()->setContextProperty("portsList", QVariant::fromValue(ports));
    view.setSource(QUrl("qrc:/main.qml"));
    view.show();

    return app.exec();
}
