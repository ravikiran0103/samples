import QtQuick 2.2
import QtQuick.Window 2.0

import SerialPort 1.0

Item
{
    id: rootItem
    property var portsList: []
    visible: true
    width: 1024//Screen.width
    height: 768//Screen.height
    SerialPort
    {
        id: sPort
        onError:
        {
            console.log("error", errorString);
        }
        onReadyToRead:
        {
            console.log(sPort.readData())
            terminal.text = terminal.text + sPort.readData()
        }
        onBytesWritten:
        {
            console.log("Data sent");
        }
        onBaudRateChanged: console.log("BaudRate: " + baudRate)
        onDataBitsChanged: console.log("DataBits: " + dataBits)
        onParityChanged: console.log("Parity: " + parity)
        onFlowControlChanged: console.log("FlowControl: " + flowControl)
        onStopBitsChanged: console.log("StopBits: " + stopBits)

        Component.onCompleted: portsList = sPort.getAvailablePorts();
    }
    Image
    {
        id: bg
        anchors.fill: parent
        source: "qrc:/images/bg.jpg"
        fillMode: Image.PreserveAspectCrop
    }
    Rectangle
    {
        id: terminalContainer
        anchors
        {
            top: rootItem.top
            topMargin: 10
            left: rootItem.left
            leftMargin: 10
        }
        width: rootItem.width * 0.75
        height: rootItem.height * 0.75
        radius: 10
        color: "#88FFFFFF"
        border
        {
            width: 1
            color: "white"
        }
        Flickable
        {
            id: flick

            anchors.fill: parent
            anchors.margins: 10
            contentWidth: terminal.paintedWidth
            contentHeight: terminal.paintedHeight
            clip: true
            flickableDirection: Flickable.VerticalFlick

            function ensureVisible(r)
            {
                if (contentX >= r.x)
                    contentX = r.x;
                else if (contentX+width <= r.x+r.width)
                    contentX = r.x+r.width-width;
                if (contentY >= r.y)
                    contentY = r.y;
                else if (contentY+height <= r.y+r.height)
                    contentY = r.y+r.height-height;
            }
            TextEdit
            {
                id: terminal
                width: flick.width
                height: flick.height
                wrapMode: TextEdit.Wrap
                textFormat: Text.PlainText
                selectByMouse: true
                selectionColor: "lightgrey"
                cursorPosition: terminal.text.length
                readOnly: true
                color: "black"
                textMargin: 5
                onCursorRectangleChanged: flick.ensureVisible(cursorRectangle)
                font
                {
                    family: "monospace"
                    pixelSize: 16
                }
            }
        }
    }

    Rectangle
    {
        id: inputContainer
        anchors
        {
            top: terminalContainer.bottom
            topMargin: 10
            left: rootItem.left
            leftMargin: 10
        }
        width: rootItem.width * 0.75
        height: rootItem.height * 0.2
        radius: 10
        color: "#88FFFFFF"
        border
        {
            width: 1
            color: "white"
        }
        Flickable
        {
            id: inputflick

            anchors.fill: parent
            contentWidth: input.paintedWidth
            contentHeight: input.paintedHeight
            clip: true
            flickableDirection: Flickable.VerticalFlick

            function ensureVisible(r)
            {
                if (contentX >= r.x)
                    contentX = r.x;
                else if (contentX+width <= r.x+r.width)
                    contentX = r.x+r.width-width;
                if (contentY >= r.y)
                    contentY = r.y;
                else if (contentY+height <= r.y+r.height)
                    contentY = r.y+r.height-height;
            }
            TextEdit
            {
                id: input
                width: inputflick.width
                height: inputflick.height
                focus: true
                wrapMode: TextEdit.Wrap
                textFormat: Text.PlainText
                selectByMouse: true
                textMargin: 10
                onCursorRectangleChanged: inputflick.ensureVisible(cursorRectangle)
                font
                {
                    family: "monospace"
                    pixelSize: 16
                }
            }
        }
    }

    B2QtComboBox
    {
        id: portnameCB
        width: rootItem.width * 0.15
        model: portsList
        anchors
        {
            left: terminalContainer.right
            topMargin: rootItem.height * 0.04
            top: terminalContainer.top
            leftMargin: rootItem.width * 0.04
        }
        z:10
        textLabel: "Port"
        onSelectionChanged:
        {
            console.log("Setting portname to " + selectedText);
            sPort.setPortName(selectedText);
        }
        enabled: openCloseContainer.state ===  "Disconnected"
    }

    B2QtComboBox
    {
        id: baudRateCB
        width: rootItem.width * 0.15
        model: ["1200", "2400", "4800", "9600",
            "19200", "38400", "57600", "115200"]
        anchors
        {
            left: terminalContainer.right
            top: portnameCB.bottom
            leftMargin: rootItem.width * 0.04
            topMargin: rootItem.height * 0.06
        }
        z: 9
        textLabel: "Baud Rate"
        onSelectionChanged:
        {
            console.log("Setting Baudrate to " + parseInt(selectedText));
            sPort.baudRate = parseInt(selectedText)
        }
    }

    B2QtComboBox
    {
        id: dataBitsCB
        width: rootItem.width * 0.15
        model: ["5", "6", "7", "8"]
        anchors
        {
            left: terminalContainer.right
            top: baudRateCB.bottom
            leftMargin: rootItem.width * 0.04
            topMargin: rootItem.height * 0.06
        }
        z: 8
        textLabel: "Data Bits"
        onSelectionChanged:
        {
            switch(selectedIndex)
            {
            case 0: sPort.dataBits = SerialPort.Data5; break;
            case 1: sPort.dataBits = SerialPort.Data6; break;
            case 2: sPort.dataBits = SerialPort.Data7; break;
            case 3: sPort.dataBits = SerialPort.Data8; break;
            default: sPort.dataBits = SerialPort.Data8;
            }
        }
    }

    B2QtComboBox
    {
        id: parityCB
        width: rootItem.width * 0.15
        model: ["No Parity", "Odd Parity", "Even Parity", "Space Parity", "Mark Parity"]
        anchors
        {
            left: terminalContainer.right
            top: dataBitsCB.bottom
            leftMargin: rootItem.width * 0.04
            topMargin: rootItem.height * 0.06
        }
        z: 7
        textLabel: "Parity"
        onSelectedIndexChanged:
        {
            switch(selectedIndex)
            {
            case 0: sPort.parity = SerialPort.NoParity; break;
            case 1: sPort.parity = SerialPort.OddParity; break;
            case 2: sPort.parity = SerialPort.EvenParity; break;
            case 3: sPort.parity = SerialPort.SpaceParity; break;
            case 4: sPort.parity = SerialPort.MarkParity; break;
            default: sPort.parity = SerialPort.NoParity;
            }
        }
    }

    B2QtComboBox
    {
        id: stopbitsCB
        width: rootItem.width * 0.15
        model: ["1", "2"]
        anchors
        {
            left: terminalContainer.right
            top: parityCB.bottom
            leftMargin: rootItem.width * 0.04
            topMargin: rootItem.height * 0.06
        }
        z: 6
        textLabel: "Stop Bits"
        onSelectionChanged:
        {
            switch(selectedIndex)
            {
            case 0: sPort.stopBits = SerialPort.OneStop; break;
            case 1: sPort.stopBits = SerialPort.TwoStop; break;
            default: sPort.stopBits = SerialPort.OneStop;
            }
        }
    }

    B2QtComboBox
    {
        id: flowControlCB
        width: rootItem.width * 0.15
        model: ["None", "Hardware", "Software"]
        anchors
        {
            left: terminalContainer.right
            top: stopbitsCB.bottom
            leftMargin: rootItem.width * 0.04
            topMargin: rootItem.height * 0.06
        }
        z: 5
        textLabel: "Flow Control"
        onSelectionChanged:
        {
            switch(selectedIndex)
            {
            case 0: sPort.flowControl = SerialPort.NoFlowControl; break;
            case 1: sPort.flowControl = SerialPort.HardwareControl; break;
            case 2: sPort.flowControl = SerialPort.SoftwareControl; break;
            default: sPort.stopBits = SerialPort.NoFlowControl;
            }
        }
    }

    Item
    {
        id: openCloseContainer
        width: 64
        height: 64
        anchors
        {
            left: terminalContainer.right
            top: flowControlCB.bottom
            topMargin: rootItem.height * 0.06
            leftMargin: rootItem.width * 0.04
        }
        state: "Disconnected"
        states: [
            State
            {
                name: "Connected"
                PropertyChanges
                {
                    target: closeBtn
                    visible: true
                }
                PropertyChanges
                {
                    target: openBtn
                    visible: false
                }
            },
            State
            {
                name: "Disconnected"
                PropertyChanges
                {
                    target: closeBtn
                    visible: false
                }
                PropertyChanges
                {
                    target: openBtn
                    visible: true
                }
            }
        ]
    }

    B2QtImageButton
    {
        id: openBtn
        anchors.fill: openCloseContainer
        source: "qrc:/images/connected.png"
        onClicked: {
                openCloseContainer.state = (openCloseContainer.state === "Disconnected")?"Connected":"Disconnected"
                console.log(sPort.openDevice(SerialPort.Read));
        }
    }

    B2QtImageButton
    {
        id: closeBtn
        anchors.fill: openCloseContainer
        source: "qrc:/images/disconnected.png"
        visible: false
        onClicked: {
                openCloseContainer.state = (openCloseContainer.state === "Disconnected")?"Connected":"Disconnected"
                sPort.closeDevice();
        }
    }

    B2QtImageButton
    {
        id: refreshBtn
        width: 64
        height: 64
        anchors
        {
            left: terminalContainer.right
            top: flowControlCB.bottom
            topMargin: rootItem.height * 0.06
            leftMargin: rootItem.width * 0.13
        }
        source: "qrc:/images/refresh.png"
        onClicked: portsList = sPort.getAvailablePorts()
    }

    B2QtImageButton
    {
        id: sendBtn
        width: 64
        height: 64
        anchors
        {
            left: terminalContainer.right
            top: inputContainer.top
            topMargin: rootItem.height * 0.06
            leftMargin: rootItem.width * 0.08
        }
        source: "qrc:/images/send.png"
        onClicked: {
            console.log(sPort.writeData(input.text))
            input.text = ""
        }
    }
}
//    TextButton
//    {
//        id: sendBtn
//        width: 64
//        height: 32
//        fontColor: "white"
//        text: "Send"
//        fontSize: 14
//        anchors
//        {
//            left: terminalContainer.right
//            top: inputContainer.top
//            topMargin: rootItem.height * 0.06
//            leftMargin: rootItem.width * 0.08
//        }
//        onClicked: {
//            console.log(input.text);
//            sPort.writeData(input.text); input.text = "";
//        }
//    }
//}

