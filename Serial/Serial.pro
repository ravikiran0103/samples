TEMPLATE = subdirs
CONFIG += ordered
SUBDIRS = \
    src \
    Example 

MOC_DIR += .moc
OBJECTS_DIR += .obj

tests.depends = src

OTHER_FILES += \
    defaults.pri

