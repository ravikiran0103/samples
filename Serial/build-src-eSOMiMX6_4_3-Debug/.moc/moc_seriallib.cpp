/****************************************************************************
** Meta object code from reading C++ file 'seriallib.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/seriallib.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'seriallib.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_SerialPort_t {
    QByteArrayData data[48];
    char stringdata0[489];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SerialPort_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SerialPort_t qt_meta_stringdata_SerialPort = {
    {
QT_MOC_LITERAL(0, 0, 10), // "SerialPort"
QT_MOC_LITERAL(1, 11, 11), // "readyToRead"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 12), // "bytesWritten"
QT_MOC_LITERAL(4, 37, 5), // "bytes"
QT_MOC_LITERAL(5, 43, 5), // "error"
QT_MOC_LITERAL(6, 49, 11), // "errorString"
QT_MOC_LITERAL(7, 61, 15), // "baudRateChanged"
QT_MOC_LITERAL(8, 77, 15), // "dataBitsChanged"
QT_MOC_LITERAL(9, 93, 13), // "parityChanged"
QT_MOC_LITERAL(10, 107, 18), // "flowControlChanged"
QT_MOC_LITERAL(11, 126, 15), // "stopBitsChanged"
QT_MOC_LITERAL(12, 142, 17), // "getAvailablePorts"
QT_MOC_LITERAL(13, 160, 11), // "setPortName"
QT_MOC_LITERAL(14, 172, 4), // "name"
QT_MOC_LITERAL(15, 177, 10), // "openDevice"
QT_MOC_LITERAL(16, 188, 8), // "OpenMode"
QT_MOC_LITERAL(17, 197, 4), // "mode"
QT_MOC_LITERAL(18, 202, 11), // "closeDevice"
QT_MOC_LITERAL(19, 214, 9), // "writeData"
QT_MOC_LITERAL(20, 224, 4), // "data"
QT_MOC_LITERAL(21, 229, 8), // "readData"
QT_MOC_LITERAL(22, 238, 8), // "baudRate"
QT_MOC_LITERAL(23, 247, 8), // "dataBits"
QT_MOC_LITERAL(24, 256, 8), // "DataBits"
QT_MOC_LITERAL(25, 265, 6), // "parity"
QT_MOC_LITERAL(26, 272, 6), // "Parity"
QT_MOC_LITERAL(27, 279, 8), // "stopBits"
QT_MOC_LITERAL(28, 288, 8), // "StopBits"
QT_MOC_LITERAL(29, 297, 11), // "flowControl"
QT_MOC_LITERAL(30, 309, 11), // "FlowControl"
QT_MOC_LITERAL(31, 321, 8), // "ReadOnly"
QT_MOC_LITERAL(32, 330, 9), // "WriteOnly"
QT_MOC_LITERAL(33, 340, 9), // "ReadWrite"
QT_MOC_LITERAL(34, 350, 5), // "Data5"
QT_MOC_LITERAL(35, 356, 5), // "Data6"
QT_MOC_LITERAL(36, 362, 5), // "Data7"
QT_MOC_LITERAL(37, 368, 5), // "Data8"
QT_MOC_LITERAL(38, 374, 8), // "NoParity"
QT_MOC_LITERAL(39, 383, 9), // "OddParity"
QT_MOC_LITERAL(40, 393, 10), // "EvenParity"
QT_MOC_LITERAL(41, 404, 11), // "SpaceParity"
QT_MOC_LITERAL(42, 416, 10), // "MarkParity"
QT_MOC_LITERAL(43, 427, 7), // "OneStop"
QT_MOC_LITERAL(44, 435, 7), // "TwoStop"
QT_MOC_LITERAL(45, 443, 13), // "NoFlowControl"
QT_MOC_LITERAL(46, 457, 15), // "HardwareControl"
QT_MOC_LITERAL(47, 473, 15) // "SoftwareControl"

    },
    "SerialPort\0readyToRead\0\0bytesWritten\0"
    "bytes\0error\0errorString\0baudRateChanged\0"
    "dataBitsChanged\0parityChanged\0"
    "flowControlChanged\0stopBitsChanged\0"
    "getAvailablePorts\0setPortName\0name\0"
    "openDevice\0OpenMode\0mode\0closeDevice\0"
    "writeData\0data\0readData\0baudRate\0"
    "dataBits\0DataBits\0parity\0Parity\0"
    "stopBits\0StopBits\0flowControl\0FlowControl\0"
    "ReadOnly\0WriteOnly\0ReadWrite\0Data5\0"
    "Data6\0Data7\0Data8\0NoParity\0OddParity\0"
    "EvenParity\0SpaceParity\0MarkParity\0"
    "OneStop\0TwoStop\0NoFlowControl\0"
    "HardwareControl\0SoftwareControl"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SerialPort[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       5,  108, // properties
       5,  128, // enums/sets
       0,    0, // constructors
       0,       // flags
       8,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   84,    2, 0x06 /* Public */,
       3,    1,   85,    2, 0x06 /* Public */,
       5,    1,   88,    2, 0x06 /* Public */,
       7,    0,   91,    2, 0x06 /* Public */,
       8,    0,   92,    2, 0x06 /* Public */,
       9,    0,   93,    2, 0x06 /* Public */,
      10,    0,   94,    2, 0x06 /* Public */,
      11,    0,   95,    2, 0x06 /* Public */,

 // methods: name, argc, parameters, tag, flags
      12,    0,   96,    2, 0x02 /* Public */,
      13,    1,   97,    2, 0x02 /* Public */,
      15,    1,  100,    2, 0x02 /* Public */,
      18,    0,  103,    2, 0x02 /* Public */,
      19,    1,  104,    2, 0x02 /* Public */,
      21,    0,  107,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::LongLong,    4,
    QMetaType::Void, QMetaType::QString,    6,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // methods: parameters
    QMetaType::QStringList,
    QMetaType::Bool, QMetaType::QString,   14,
    QMetaType::Bool, 0x80000000 | 16,   17,
    QMetaType::Void,
    QMetaType::LongLong, QMetaType::QByteArray,   20,
    QMetaType::QByteArray,

 // properties: name, type, flags
      22, QMetaType::Int, 0x00495103,
      23, 0x80000000 | 24, 0x0049510b,
      25, 0x80000000 | 26, 0x0049510b,
      27, 0x80000000 | 28, 0x0049510b,
      29, 0x80000000 | 30, 0x0049510b,

 // properties: notify_signal_id
       3,
       4,
       5,
       7,
       6,

 // enums: name, flags, count, data
      16, 0x0,    3,  148,
      24, 0x0,    4,  154,
      26, 0x0,    5,  162,
      28, 0x0,    2,  172,
      30, 0x0,    3,  176,

 // enum data: key, value
      31, uint(SerialPort::ReadOnly),
      32, uint(SerialPort::WriteOnly),
      33, uint(SerialPort::ReadWrite),
      34, uint(SerialPort::Data5),
      35, uint(SerialPort::Data6),
      36, uint(SerialPort::Data7),
      37, uint(SerialPort::Data8),
      38, uint(SerialPort::NoParity),
      39, uint(SerialPort::OddParity),
      40, uint(SerialPort::EvenParity),
      41, uint(SerialPort::SpaceParity),
      42, uint(SerialPort::MarkParity),
      43, uint(SerialPort::OneStop),
      44, uint(SerialPort::TwoStop),
      45, uint(SerialPort::NoFlowControl),
      46, uint(SerialPort::HardwareControl),
      47, uint(SerialPort::SoftwareControl),

       0        // eod
};

void SerialPort::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        SerialPort *_t = static_cast<SerialPort *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->readyToRead(); break;
        case 1: _t->bytesWritten((*reinterpret_cast< qint64(*)>(_a[1]))); break;
        case 2: _t->error((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->baudRateChanged(); break;
        case 4: _t->dataBitsChanged(); break;
        case 5: _t->parityChanged(); break;
        case 6: _t->flowControlChanged(); break;
        case 7: _t->stopBitsChanged(); break;
        case 8: { QStringList _r = _t->getAvailablePorts();
            if (_a[0]) *reinterpret_cast< QStringList*>(_a[0]) = _r; }  break;
        case 9: { bool _r = _t->setPortName((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 10: { bool _r = _t->openDevice((*reinterpret_cast< OpenMode(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 11: _t->closeDevice(); break;
        case 12: { qint64 _r = _t->writeData((*reinterpret_cast< QByteArray(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< qint64*>(_a[0]) = _r; }  break;
        case 13: { QByteArray _r = _t->readData();
            if (_a[0]) *reinterpret_cast< QByteArray*>(_a[0]) = _r; }  break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (SerialPort::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&SerialPort::readyToRead)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (SerialPort::*_t)(qint64 );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&SerialPort::bytesWritten)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (SerialPort::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&SerialPort::error)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (SerialPort::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&SerialPort::baudRateChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (SerialPort::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&SerialPort::dataBitsChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (SerialPort::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&SerialPort::parityChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (SerialPort::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&SerialPort::flowControlChanged)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (SerialPort::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&SerialPort::stopBitsChanged)) {
                *result = 7;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        SerialPort *_t = static_cast<SerialPort *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< qint32*>(_v) = _t->baudRate(); break;
        case 1: *reinterpret_cast< DataBits*>(_v) = _t->dataBits(); break;
        case 2: *reinterpret_cast< Parity*>(_v) = _t->parity(); break;
        case 3: *reinterpret_cast< StopBits*>(_v) = _t->stopBits(); break;
        case 4: *reinterpret_cast< FlowControl*>(_v) = _t->flowControl(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        SerialPort *_t = static_cast<SerialPort *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setBaudRate(*reinterpret_cast< qint32*>(_v)); break;
        case 1: _t->setDataBits(*reinterpret_cast< DataBits*>(_v)); break;
        case 2: _t->setParity(*reinterpret_cast< Parity*>(_v)); break;
        case 3: _t->setStopBits(*reinterpret_cast< StopBits*>(_v)); break;
        case 4: _t->setFlowControl(*reinterpret_cast< FlowControl*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject SerialPort::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_SerialPort.data,
      qt_meta_data_SerialPort,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *SerialPort::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SerialPort::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_SerialPort.stringdata0))
        return static_cast<void*>(const_cast< SerialPort*>(this));
    return QObject::qt_metacast(_clname);
}

int SerialPort::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 14)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 14;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 5;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void SerialPort::readyToRead()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void SerialPort::bytesWritten(qint64 _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void SerialPort::error(QString _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void SerialPort::baudRateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void SerialPort::dataBitsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}

// SIGNAL 5
void SerialPort::parityChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}

// SIGNAL 6
void SerialPort::flowControlChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, Q_NULLPTR);
}

// SIGNAL 7
void SerialPort::stopBitsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
