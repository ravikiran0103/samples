include(../defaults.pri)
TEMPLATE = lib
TARGET = ../../lib/B2QtSerial

CONFIG += c++11 RELEASE

QT +=  core serialport

MOC_DIR += .moc
OBJECTS_DIR += .obj

INCLUDEPATH += . ./include

SOURCES += seriallib.cpp \

HEADERS += seriallib.h \
    seriallibconfig.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
