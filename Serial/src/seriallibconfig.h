#ifndef SERIALPORTCONFIG
#define SERIALPORTCONFIG

#include <QtSerialPort/QSerialPort>
class SerialPortConfig : public QObject
{
    Q_OBJECT
public:
    //Variable declarations
    QSerialPort *m_sp;
    qint32 m_baudRate;
    bool m_isPortSet;

public slots:
    /*
     * <summary>
     *      Mapping error codes between Qt namespace and user input
     * </summary>
     */
    void mapErrorCode(QSerialPort::SerialPortError errCode)
    {
        switch(errCode)
        {
        case QSerialPort::NoError:                     emit error("NoError"); break;
        case QSerialPort::DeviceNotFoundError:         emit error("DeviceNotFoundError"); break;
        case QSerialPort::PermissionError:             emit error("PermissionError"); break;
        case QSerialPort::OpenError:                   emit error("OpenError"); break;
        case QSerialPort::ParityError:                 emit error("ParityError"); break;
        case QSerialPort::FramingError:                emit error("FramingError"); break;
        case QSerialPort::BreakConditionError:         emit error("BreakConditionError"); break;
        case QSerialPort::WriteError:                  emit error("WriteError"); break;
        case QSerialPort::ReadError:                   emit error("ReadError"); break;
        case QSerialPort::ResourceError:               emit error("ResourceError"); break;
        case QSerialPort::UnsupportedOperationError:   emit error("UnsupportedOperationError"); break;
        case QSerialPort::TimeoutError:                emit error("TimeoutError"); break;
        default:                                       emit error("UnknownError");
        }
    }

signals:
    void error(QString);
};


#endif // SERIALPORTCONFIG

