#ifndef SERIALPORT_H
#define SERIALPORT_H

#include <QStringList>
#include <QObject>
/*!
    \class SerialPort
    \brief Provides functions to access serial ports.

    \par
    You can get information about the available serial ports using the
    getAvailablePorts() method, which allows an enumeration of all the serial
    ports in the system. This is useful to obtain the correct name of the
    serial port you want to use. You can pass an the device's name as an argument
    to the setPortName() methods to access the desired serial device.

    \par
    After setting the port, you can open it in read-only (r/o), write-only
    (w/o), or read-write (r/w) mode using the openDevice() method.

    \note The serial port is always opened with exclusive access
    (No other process or thread can access an already opened serial port).

    \par
    Once you know that the ports are ready to read or write, you can
    use the readData() or writeData() methods. If not all the data is
    read at once, the remaining data will be available for later as new
    incoming data is appended to the SerialPort's internal read buffer.
    \par
    Use the closeDevice() method to close the port and cancel the I/O operations.
*/


class SerialPort : public QObject
{
    Q_OBJECT
    /*!
      \property SerialPort::baudRate
      \brief This property holds the data baud rate.
      \par
      To set the baud rate, use any positive qint32 value.

      \note The default value is 9600 bits per second.
      \sa baudRate(), setBaudRate(qint32), baudRateChanged()
     */
    Q_PROPERTY(qint32 baudRate READ baudRate WRITE setBaudRate NOTIFY baudRateChanged)
    /*!
      \property SerialPort::dataBits
      \brief This property holds the data bits in a frame.
      \par
      To set the number of data bits, use the DataBits enum.

      \note The default value is 8 data bits, i.e SerialPort::Data8.
      \sa dataBits(), setDataBits(DataBits dataBits), SerialPort::DataBits, dataBitsChanged()
     */
    Q_PROPERTY(DataBits dataBits READ dataBits WRITE setDataBits NOTIFY dataBitsChanged)
    /*!
      \property SerialPort::parity
      \brief This property holds the parity checking mode.
      \par
      To set the parity checking mode, use the Parity enum.

      \note The default value is no parity, i.e SerialPort::NoParity.
      \sa parity(), setParity(Parity parity), SerialPort::Parity, parityChanged()
     */
    Q_PROPERTY(Parity parity READ parity WRITE setParity NOTIFY parityChanged)
    /*!
      \property SerialPort::stopBits
      \brief This property holds the number of stop bits in a frame.
      \par
      To set the number of stop bits, use the StopBits enum.

      \note The default value is 1 stop bit, i.e SerialPort::OneStop.
      \sa stopBits(), setStopBits(StopBits stopBits), SerialPort::StopBits, stopBitsChanged()
     */
    Q_PROPERTY(StopBits stopBits READ stopBits WRITE setStopBits NOTIFY stopBitsChanged)
    /*!
      \property SerialPort::flowControl
      \brief This property holds the desired flow control mode.
      \par
      To set the flow control mode, use the FlowControl enum.

      \note The default value is no flow control, i.e SerialPort::NoFlowControl.
      \sa flowControl(), setFlowControl(FlowControl flowCtrl), SerialPort::FlowControl, flowControlChanged()
     */
    Q_PROPERTY(FlowControl flowControl READ flowControl WRITE setFlowControl NOTIFY flowControlChanged)


public:

    explicit SerialPort(QObject *parent = 0);
    ~SerialPort();

    /*!
     * \brief This enum specified the modes in which a serial port can be opened.
     * \enum SerialPort::OpenMode
     * \sa openDevice()
     */
    enum OpenMode
    {
        ReadOnly = 0,  /**< Open device for reading */
        WriteOnly = 1, /**< Open device for writing */
        ReadWrite = 2  /**< Open device for reading and writing */
    };
    Q_ENUMS(OpenMode)

    /*!
     * \enum SerialPort::DataBits
     * \brief This enum describes the number of data bits used.
     * \sa dataBits()
     */
    enum DataBits
    {
        Data5 = 5, /**< The number of data bits in each character is 5. It
                        is used for Baudot code. It generally only makes
                        sense with older equipment such as teleprinters. */
        Data6 = 6, /**< The number of data bits in each character is 6. It
                        is rarely used.  */
        Data7 = 7, /**< The number of data bits in each character is 7. It
                        is used for true ASCII. It generally only makes
                        sense with older equipment such as teleprinters. */
        Data8 = 8 /**< The number of data bits in each character is 8. It
                       is used for most kinds of data, as this size matches
                       the size of a byte. It is almost universally used in
                       newer applications. */
    };
    Q_ENUMS(DataBits)
    DataBits m_dataBits;
    /*!
     * \enum SerialPort::Parity
     * \brief This enum describes the parity scheme used.
     * \value NoParity
     * \value EvenParity
     * \value OddParity
     * \value SpaceParity
     * \value MarkParity
     * \sa parity()
     */
    enum Parity
    {
        NoParity = 0,           /**< No parity bit it sent. This is the most common
                                     parity setting. Error detection is handled by the
                                     communication protocol. */
        OddParity = 1,          /**< The number of 1 bits in each character, including
                                     the parity bit, is always even. */
        EvenParity = 2,         /**< The number of 1 bits in each character, including
                                     the parity bit, is always odd. It ensures that at
                                     least one state transition occurs in each character. */
        SpaceParity = 3,        /**< Space parity. The parity bit is sent in the space
                                     signal condition. It does not provide error
                                     detection information. */
        MarkParity = 4          /**< Mark parity. The parity bit is always set to the
                                     mark signal condition (logical 1). It does not
                                     provide error detection information. */
    };
    Q_ENUMS(Parity)
    Parity m_parity;
    /*!
     * \enum SerialPort::StopBits
     * \brief This enum describes the number of stop bits used.
     * \sa stopBits()
     */
    enum StopBits
    {
        OneStop = 1,  /**< 1 stop bit. */
        TwoStop = 2   /**< 2 stop bits. */
    };
    Q_ENUMS(StopBits)
    StopBits m_stopBits;
    /*!
     * \enum SerialPort::FlowControl
     * \brief This enum describes the flow control used.
     * \sa flowControl()
    */
    enum FlowControl
    {
        NoFlowControl = 0,      /**< No flow control. */
        HardwareControl = 1,    /**< Hardware flow control (RTS/CTS). */
        SoftwareControl = 2     /**< Software flow control (XON/XOFF). */
    };
    Q_ENUMS(FlowControl)
    FlowControl m_flowControl;


signals:
    /*!
     * \brief Signal emmitted when data is ready to be read from the serial device's buffer.
     * \sa readData()
     */
    void readyToRead();
    /*!
     * \brief This signal is emitted every time a payload of data has been written to the device.
     *  The bytes argument is set to the number of bytes that were written in this payload.
     * \sa writeData()
     */
    void bytesWritten(qint64 bytes);
    /*!
     * \brief This signal is emitted whenever an error occurs.
     * \param errorString Holds the latest error
     */
    void error(QString errorString);
    /*!
     * \brief This signal is emitted whenever the user sets the baud rate.
     */
    void baudRateChanged();
    /*!
     * \brief This signal is emitted whenever the user sets the number of data bits.
     */
    void dataBitsChanged();
    /*!
     * \brief This signal is emitted whenever the user sets the parity scheme.
     */
    void parityChanged();
    /*!
     * \brief This signal is emitted whenever the user sets the flow control.
     */
    void flowControlChanged();
    /*!
     * \brief This signal is emitted whenever the user sets the number of stop bits.
     */
    void stopBitsChanged();

public:
    /*!
     * \brief Get a list of serial ports available in the system.
     * \return List of all available serial ports in the system as QStringList.
     */
    Q_INVOKABLE QStringList getAvailablePorts();

    /*!
     * \brief Set the name of the serial port to \c name
     * \return Returns \c true if name is set, \c false otherwise.
     * \sa getAvailablePorts()
     */
    Q_INVOKABLE bool setPortName(QString name);

    qint32 baudRate() const;
    void setBaudRate(qint32 rate);
    DataBits dataBits() const;
    void setDataBits(DataBits dataBits);
    Parity parity() const;
    void setParity(Parity parity);
    StopBits stopBits() const;
    void setStopBits(StopBits stopBits);
    FlowControl flowControl() const;
    void setFlowControl(FlowControl flowCtrl);

    /*!
      \brief Opens the serial port using \c mode, and then returns true if successful;
      otherwise returns false and emits error() signal containing the error code.
      \param mode Mode for opening the serial port
      \sa SerialPort::OpenMode
     */
    Q_INVOKABLE bool openDevice(OpenMode mode);
    /*!
      \brief Close the serial port
      \note The serial port has to be open before trying to close it; otherwise
       this method emits \c error() signal.
     */
    Q_INVOKABLE void closeDevice();

    /*!
      \brief Writes the content of \c data to the device.
      \return Returns the number of bytes that were actually written, or -1 if an error occurred.
     */
    Q_INVOKABLE qint64 writeData(QByteArray data);
    /*!
      \brief Reads all available data from the device, and returns it as a byte array.
      \note This function has no way of reporting errors; returning an empty QByteArray
      can mean either that no data was currently available for reading, or that an error occurred.
     */
    Q_INVOKABLE QByteArray readData();

};

#endif // SERIALPORT_H
