#include "seriallib.h"
#include "seriallibconfig.h"
#include <QDebug>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QByteArray>
#include <QStringList>

SerialPortConfig *configObj;
/*
 * <summary>
 *      Create an object for QSerialPort class and use the object to set default properties
 *      for the port.
 */
SerialPort::SerialPort(QObject *parent) : QObject(parent)
{

    configObj = new SerialPortConfig;
    if(configObj)
    {

        configObj->m_sp = new QSerialPort();

        if(configObj->m_sp)
        {
            setBaudRate(9600);
            setDataBits(SerialPort::Data8);
            setFlowControl(SerialPort::NoFlowControl);
            setStopBits(SerialPort::OneStop);
            setParity(SerialPort::NoParity);
            connect(configObj->m_sp, SIGNAL(readyRead()), this, SIGNAL(readyToRead()));

            connect(configObj->m_sp, SIGNAL(bytesWritten(qint64)), this, SIGNAL(bytesWritten(qint64)));

            connect(configObj->m_sp, SIGNAL(error(QSerialPort::SerialPortError)),
                    configObj, SLOT(mapErrorCode(QSerialPort::SerialPortError)));

            connect(configObj, SIGNAL(error(QString)), this, SIGNAL(error(QString)));
        }
    }

}

/*
 * <summary>
 *      Delete instance of SerialPort object
 * </summary>
 */
SerialPort::~SerialPort()
{
    if(configObj && configObj->m_sp)
    {
        if(configObj->m_sp->isOpen())
            configObj->m_sp->close();
        delete configObj->m_sp;
        delete configObj;
    }
}

/*
 * <summary>
 *      Mapping no. of data bits between Qt namespace and user input
 * </summary>
 */
QSerialPort::DataBits mapDataBits(SerialPort::DataBits dataBits)
{
    switch(dataBits)
    {
    case SerialPort::Data5:     return QSerialPort::Data5;
    case SerialPort::Data6:     return QSerialPort::Data6;
    case SerialPort::Data7:     return QSerialPort::Data7;
    case SerialPort::Data8:     return QSerialPort::Data8;
    default:                    return QSerialPort::UnknownDataBits;
    }
}

/*
 * <summary>
 *      Mapping parity between Qt namespace and user input
 * </summary>
 */
QSerialPort::Parity mapParity(SerialPort::Parity parity)
{
    switch(parity)
    {
    case SerialPort::NoParity:      return QSerialPort::NoParity;
    case SerialPort::OddParity:     return QSerialPort::OddParity;
    case SerialPort::EvenParity:    return QSerialPort::EvenParity;
    case SerialPort::SpaceParity:   return QSerialPort::SpaceParity;
    case SerialPort::MarkParity:    return QSerialPort::MarkParity;
    default:                        return QSerialPort::UnknownParity;
    }
}

/*
 * <summary>
 *      Mapping no. of stop bits between Qt namespace and user input
 * </summary>
 */
QSerialPort::StopBits mapStopBits(SerialPort::StopBits stopBits)
{
    switch(stopBits)
    {
    case SerialPort::OneStop:       return QSerialPort::OneStop;
    case SerialPort::TwoStop:       return QSerialPort::TwoStop;
    default:                        return QSerialPort::UnknownStopBits;
    }
}

/*
 * <summary>
 *      Mapping flow control between Qt namespace and user input
 * </summary>
 */
QSerialPort::FlowControl mapFlowCtrl(SerialPort::FlowControl flowControl)
{
    switch(flowControl)
    {
    case SerialPort::NoFlowControl:         return QSerialPort::NoFlowControl;
    case SerialPort::HardwareControl:       return QSerialPort::HardwareControl;
    case SerialPort::SoftwareControl:       return QSerialPort::SoftwareControl;
    default:                                return QSerialPort::UnknownFlowControl;
    }
}

/*
 * <summary>
 *      Mapping port open mode between Qt namespace and user input
 *      Defaults to ReadWrite mode is an invalid mode is specified by user.
 * </summary>
 */
QIODevice::OpenMode mapOpenMode(SerialPort::OpenMode mode)
{
    switch(mode)
    {
    case SerialPort::ReadOnly:    return QIODevice::ReadOnly;
    case SerialPort::WriteOnly:   return QIODevice::WriteOnly;
    case SerialPort::ReadWrite:   return QIODevice::ReadWrite;
    default:                      return QIODevice::ReadWrite;
    }
}


/*
 * <summary>
 *      Use static method availablePorts() of QSerialPortInfo to get the info of all serial
 *      ports available in the device. Iterate through each port and retrive the names of the
 *      ports and append them to the list of ports. Return the list of ports.
 * </summary>
 */
QStringList SerialPort::getAvailablePorts()
{
    QStringList ports;
    foreach(const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
        ports << info.portName();
    return ports;
}

/*
 * <summary>
 *      Use the object of QSerialPort to set the port name to 'name'.
 *      Enable 'configObj->m_isPortSet' flag.
 * </summary>
 */
bool SerialPort::setPortName(QString name)
{
    if(configObj->m_sp)
    {
        if(getAvailablePorts().contains(name))
        {
            configObj->m_sp->setPortName(name);
            configObj->m_isPortSet = true;
            return true;
        }
        emit error("Invalid Portname");
        return false;
    }
    return false;
}

/*
 * <summary>
 *      Return value of baudRate property
 * </summary>
 */
qint32 SerialPort::baudRate() const
{
    return configObj->m_baudRate;
}

/*
 * <summary>
 *      Use the object of QSerialPort to set the baud rate to 'rate'
 * </summary>
 */
void SerialPort::setBaudRate(qint32 rate)
{
    configObj->m_baudRate = rate;
    if(configObj->m_sp)
    {
        configObj->m_sp->setBaudRate(rate);
        emit baudRateChanged();
    }
}
/*
 * <summary>
 *      Return value of dataBits property
 * </summary>
 */
SerialPort::DataBits SerialPort::dataBits() const
{
    return m_dataBits;
}

/*
 * <summary>
 *      Use the object of QSerialPort to set the no. of data bits to 'dataBits'
 * </summary>
 */
void SerialPort::setDataBits(DataBits dataBits)
{
    m_dataBits = dataBits;
    if(configObj->m_sp)
    {
        configObj->m_sp->setDataBits(mapDataBits(dataBits));
        emit dataBitsChanged();
    }
}

/*
 * <summary>
 *      Return value of parity property
 * </summary>
 */
SerialPort::Parity SerialPort::parity() const
{
    return m_parity;
}

/*
 * <summary>
 *      Use the object of QSerialPort to set the parity to 'parity'
 * </summary>
 */
void SerialPort::setParity(Parity parity)
{
    m_parity = parity;
    if(configObj->m_sp)
    {
        configObj->m_sp->setParity(mapParity(parity));
        emit parityChanged();
    }
}

/*
 * <summary>
 *      Return value of stopBits property
 * </summary>
 */
SerialPort::StopBits SerialPort::stopBits() const
{
    return m_stopBits;
}

/*
 * <summary>
 *      Use the object of QSerialPort to set the no. of stop bits to 'stopBits'
 * </summary>
 */
void SerialPort::setStopBits(StopBits stopBits)
{
    m_stopBits = stopBits;
    if(configObj->m_sp)
    {
        configObj->m_sp->setStopBits(mapStopBits(stopBits));\
        emit stopBitsChanged();
    }
}

/*
 * <summary>
 *      Return value of flowControl property
 * </summary>
 */
SerialPort::FlowControl SerialPort::flowControl() const
{
    return m_flowControl;
}

/*
 * <summary>
 *      Use the object of QSerialPort to set flow control to 'flowCtrl'
 * </summary>
 */
void SerialPort::setFlowControl(FlowControl flowCtrl)
{
    m_flowControl = flowCtrl;
    if(configObj->m_sp)
    {
        configObj->m_sp->setFlowControl(mapFlowCtrl(flowCtrl));
        emit flowControlChanged();
    }
}

/*
 * <summary>
 *      Use the object of QSerialPort to open serial port. Check if
 *      port name is set before opening.
 * </summary>
 */
bool SerialPort::openDevice(OpenMode mode)
{

    if(configObj->m_sp && !configObj->m_sp->isOpen() && configObj->m_isPortSet)
    {
        return configObj->m_sp->open(mapOpenMode(mode));
    }
    return false;
}

/*
 * <summary>
 *      If device is open, close device. Disable 'configObj->m_isPortSet' flag.
 * </summary>
 */
void SerialPort::closeDevice()
{
    if(configObj->m_sp && configObj->m_sp->isOpen() && configObj->m_isPortSet)
    {
        configObj->m_isPortSet = false;
        configObj->m_sp->close();
    }
}

/*
 * <summary>
 *      Write 'data' to serial port.
 * </summary>
 */
qint64 SerialPort::writeData(QByteArray data)
{
    qDebug() << data;
    if(configObj->m_sp && configObj->m_sp->isOpen() && !data.isEmpty())
        return configObj->m_sp->write(data);
    else
        return -1;
}

/*
 * <summary>
 *      Read all data in the serial port as a QByteArray and return to user.
 *      On error, return empty byte array.
 * </summary>
 */
QByteArray SerialPort::readData()
{
    if(configObj->m_sp && configObj->m_sp->isOpen())
        return (configObj->m_sp->readAll());
    else
        return QByteArray("");
}
