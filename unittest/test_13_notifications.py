from helper_file import Unit_test_object
carelink_unit_test = Unit_test_object()
from models.message_queue_model import MessageQueueModel
from models.user_model import UserModel
from datetime import datetime,timedelta
from utilities.common_enums import Setting_Enum
class TestNotifications():

        """TestNotifications  
        
        Description:This class contains the Unittest cases functions for Notifications module.      
        
        """

        # verify update_view module with alerts and notifications
        def test_13_01_update_view_with_notification_with_alert(self):
            result = carelink_unit_test.notifications_alerts.update_view()
            assert result == True

        # verify update_view module with no notifications and no alerts
        def test_13_02_update_view_no_notification_no_alert(self):
            carelink_unit_test.repository.message_queue_repo.clear()
            result = carelink_unit_test.notifications_alerts.update_view()
            assert result == True

        # verify update_view module with notifications and no alerts
        #sathis - Try to avoid hard code values
        def test_13_03_update_view_with_notification_no_alert(self):
            msg_models = []
            cart_default_settings = carelink_unit_test.repository.setting_repo.get_cart_default_setting(
                            [Setting_Enum.CART_ID.value]
                        )
    
            if carelink_unit_test.repository.setting_repo.validate_setting(
                        Setting_Enum.CART_ID.value,
                        cart_default_settings,
                        "Cart ID"
                    ):
                
                self.cart_id = int(cart_default_settings[Setting_Enum.CART_ID.value])
            for i in range(1, 10):
                msg_model = MessageQueueModel(i,i,"nsight",self.cart_id,1,0,100,
                                96,"this is a testing notification"+str(i),"testing",4284322279,4278190080,
                                datetime.utcnow(),datetime.utcnow(),
	                            datetime.utcnow()+ timedelta(days=2),0,datetime.utcnow(),
                                0,0)
                msg_models.append(msg_model)
            carelink_unit_test.repository.message_queue_repo.update_or_add(msg_models)
            result = carelink_unit_test.notifications_alerts.update_view()
            assert result == True

        # verify update_view module with alerts and no notifications
        def test_13_04_update_view_no_notification_with_alert(self):
            carelink_unit_test.repository.message_queue_repo.clear()
            msg_models = []
            cart_default_settings = carelink_unit_test.repository.setting_repo.get_cart_default_setting(
                            [Setting_Enum.CART_ID.value]
                        )
    
            if carelink_unit_test.repository.setting_repo.validate_setting(
                        Setting_Enum.CART_ID.value,
                        cart_default_settings,
                        "Cart ID"
                    ):
                
                self.cart_id = int(cart_default_settings[Setting_Enum.CART_ID.value])
            for i in range(1, 10):
                msg_model = MessageQueueModel(i,i,"nsight",self.cart_id,1,0,100,
                                96,"this is a testing notification"+str(i),"testing",4284322279,4278190080,
                                datetime.utcnow(),datetime.utcnow(),
	                            datetime.utcnow()+ timedelta(days=2),1,datetime.utcnow(),
                                0,0)
                msg_models.append(msg_model)
            carelink_unit_test.repository.message_queue_repo.update_or_add(msg_models)
            result = carelink_unit_test.notifications_alerts.update_view()
            assert result == True

        # verify update_view module with 50 alerts and 50 notifications
        def test_13_05_update_view_more_notification_alert(self):
            carelink_unit_test.repository.message_queue_repo.clear()
            #sathis - introduce variable near usage
            msg_models = []
            cart_default_settings = carelink_unit_test.repository.setting_repo.get_cart_default_setting(
                            [Setting_Enum.CART_ID.value]
                        )
    
            if carelink_unit_test.repository.setting_repo.validate_setting(
                        Setting_Enum.CART_ID.value,
                        cart_default_settings,
                        "Cart ID"
                    ):
                
                self.cart_id = int(cart_default_settings[Setting_Enum.CART_ID.value])
            for i in range(1, 100,2):
                msg_model = MessageQueueModel(i,i,"nsight",self.cart_id,1,0,100,
                                96,"this is a testing notification"+str(i),"testing",4284322279,4278190080,
                                datetime.utcnow(),datetime.utcnow(),
	                            datetime.utcnow()+ timedelta(days=2),0,datetime.utcnow(),
                                0,0)
                msg_models.append(msg_model)
            for i in range(2, 100,2):
                msg_model = MessageQueueModel(i,i,"nsight",self.cart_id,1,0,100,
                                96,"this is a testing notification"+str(i),"testing",4284322279,4278190080,
                                datetime.utcnow(),datetime.utcnow(),
	                            datetime.utcnow()+ timedelta(days=2),1,datetime.utcnow(),
                                0,0)
                msg_models.append(msg_model)
            carelink_unit_test.repository.message_queue_repo.update_or_add(msg_models)
            result = carelink_unit_test.notifications_alerts.update_view()
            assert result == True
        
        # verify update_view module with alerts and notifications after login
        def test_13_06_update_view_notification_alert_login(self):
            carelink_unit_test.repository.message_queue_repo.clear()
            #sathis - check if we need to check return value
            self.cart_user = self.get_single_user()
            carelink_unit_test.notifications_alerts._security.active_user = self.cart_user
            msg_models = []
            for i in range(1, 100,2):
                msg_model = MessageQueueModel(i,i,"nsight","steve",1,0,100,
                                self.cart_user.user_id,"this is a testing notification"+str(i),"testing",4284322279,4278190080,
                                datetime.utcnow(),datetime.utcnow(),
	                            datetime.utcnow()+ timedelta(days=2),0,datetime.utcnow(),
                                0,0)
                msg_models.append(msg_model)
            for i in range(2, 100,2):
                msg_model = MessageQueueModel(i,i,"nsight","steve",1,0,100,
                                self.cart_user.user_id,"this is a testing notification"+str(i),"testing",4284322279,4278190080,
                                datetime.utcnow(),datetime.utcnow(),
	                            datetime.utcnow()+ timedelta(days=2),1,datetime.utcnow(),
                                0,0)
                msg_models.append(msg_model)
            carelink_unit_test.repository.message_queue_repo.update_or_add(msg_models)
            result = carelink_unit_test.notifications_alerts.update_view()
            assert result == True


        # get single user from usermodel
        def get_single_user(self):
            try: 
                db = carelink_unit_test.db_connection.cursor()
                query = "SELECT * FROM User limit 1"
                records = db.execute(query).fetchone()
                if(records != None):
                    return UserModel(
                            records[0], records[1], records[2],
                            records[3], records[4], records[5], records[6]
                    )

                return None

            except Exception as e:
                    print("UserRepository - getSingle failed ", e)
                    return False
        
