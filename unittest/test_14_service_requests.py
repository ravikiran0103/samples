from helper_file import Unit_test_object
carelink_unit_test = Unit_test_object()
from models.message_queue_model import MessageQueueModel
from models.user_model import UserModel
from datetime import datetime,timedelta
from utilities.common_enums import Setting_Enum

class TestServiceRequests():
        """TestServiceRequests  
        
        Description:This class contains the Unittest cases functions for ServiceRequests module.      
        
        """

        # verify get_sender_id_name after login
        def test_14_01_get_sender_id_name_with_login(self):
            self.cart_user = self.get_single_user()
            carelink_unit_test.security.active_user = self.cart_user
            sender_id,sender_name = carelink_unit_test.service_request.get_sender_id_name()
            assert sender_id == self.cart_user.user_id
            assert sender_name == self.cart_user.user_display_name
            
        # verify get_sender_id_name before login
        def test_14_02_get_sender_id_name_without_login(self):
            carelink_unit_test.security.active_user = None
            cart_default_settings =	\
						carelink_unit_test.repository.setting_repo.get_cart_default_setting(
							[
								Setting_Enum.CART_NAME.value,
								Setting_Enum.CART_ID.value
							]
						)
            sender_id,sender_name = carelink_unit_test.service_request.get_sender_id_name()
            assert sender_id == int(
						cart_default_settings[Setting_Enum.CART_ID.value]
					)
            assert sender_name == cart_default_settings[
						Setting_Enum.CART_NAME.value
					]

        # verify display_service_request_types with request types in message type table
        def test_14_03_display_service_request_types_with_data(self):
          result = carelink_unit_test.service_request.display_service_request_types()
          assert result == True

        # verify display_service_request_types without request types in message type table
        def test_14_04_display_service_request_types_without_data(self):
          carelink_unit_test.repository.message_type_repo.clear()
          result = carelink_unit_test.service_request.display_service_request_types()
          assert result == False

        # verify display_service_request with service requests sent by cart(without login)
        def test_14_05_display_service_requests_without_data(self):
          carelink_unit_test.repository.message_queue_repo.clear()
          result = carelink_unit_test.service_request.display_service_request_types()
          assert result == False

        # verify display_service_request without service requests sent (without login)
        def test_14_06_display_service_requests_with_data(self):
          msg_models = []
          cart_default_settings = carelink_unit_test.repository.setting_repo.get_cart_default_setting(
                            [Setting_Enum.CART_ID.value]
                        )
    
          if carelink_unit_test.repository.setting_repo.validate_setting(
                      Setting_Enum.CART_ID.value,
                      cart_default_settings,
                      "Cart ID"
                  ):
              
              self.cart_id = int(cart_default_settings[Setting_Enum.CART_ID.value])
          for i in range(1, 10):
              msg_model = MessageQueueModel(i,i + 1,"ldwow04","nsight",
              1,0,96,2,"This is testing request"+str(i),"testing",4284322279,
              4278190080,datetime.utcnow(),datetime.utcnow(),
              datetime.utcnow() + timedelta(days=2),17,datetime.utcnow(),0,0)
              msg_models.append(msg_model)
          carelink_unit_test.repository.message_queue_repo.update_or_add(msg_models)
          result = carelink_unit_test.service_request.display_service_requests()
          assert result == True

        # verify display_service_request with service requests sent by user(with login)
        def test_14_07_display_service_requests_without_data_login(self):
          self.cart_user = self.get_single_user()
          carelink_unit_test.service_request._security.active_user = self.cart_user
          carelink_unit_test.repository.message_queue_repo.clear()
          result = carelink_unit_test.service_request.display_service_request_types()
          assert result == False

        # verify display_service_request without service requests sent (with login)
        def test_14_08_display_service_requests_with_data_login(self):
          self.cart_user = self.get_single_user()
          carelink_unit_test.service_request._security.active_user = self.cart_user
          msg_models = []
          
          for i in range(1, 10):
              msg_model = MessageQueueModel(i,i + 1,"ldwow04","nsight",
              1,0,self.cart_user.user_id,2,"This is testing request"+str(i),"testing",4284322279,
              4278190080,datetime.utcnow(),datetime.utcnow(),
              datetime.utcnow() + timedelta(days=2),17,datetime.utcnow(),0,0)
              msg_models.append(msg_model)
          carelink_unit_test.repository.message_queue_repo.update_or_add(msg_models)
          result = carelink_unit_test.service_request.display_service_requests()
          assert result == True

        

        # get single user from usermodel
        def get_single_user(self):
            try: 
                db = carelink_unit_test.db_connection.cursor()
                query = "SELECT * FROM User limit 1"
                records = db.execute(query).fetchone()
                if(records != None):
                    return UserModel(
                            records[0], records[1], records[2],
                            records[3], records[4], records[5], records[6]
                    )

                return None

            except Exception as e:
                    print("UserRepository - getSingle failed ", e)
                    return False