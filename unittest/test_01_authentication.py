from helper_file import Unit_test_object
import pytest
from Crypto.Cipher import AES
import binascii
import time
import math
from PyQt5.QtCore import QByteArray
from pysqlcipher3 import dbapi2 as sqlcipher
import time

KEY = binascii.unhexlify('BE0135F28F01AE69C608A869D13812E1')
IV = binascii.unhexlify('BBD72E890181D83E0049BB32A95A2CB0')

from sqlite.repositories.user_repository import UserRepository
from models.user_model import UserModel
from utilities.common_enums import Setting_Enum,ResponseType


carelink_unit_test = Unit_test_object()

# decrypt the pin
def decrypted_pin(element):
        rijnd = AES.new(KEY, AES.MODE_CBC, IV)
        pt = rijnd.decrypt(binascii.unhexlify(element))
        pin = pt.decode('utf-8').rstrip() 
        return pin  

# get single user from usermodel
def get_single_user():
        try: 
            db = carelink_unit_test.db_connection.cursor()
            query = "SELECT * FROM User limit 1"
            records = db.execute(query).fetchone()
            if(records != None):
                return UserModel(
                        records[0], records[1], records[2],
                        records[3], records[4], records[5], records[6]
                )

            return None

        except sqlcipher.Error as e:
                print("UserRepository - getSingle failed ", e)
                return False

# Get Cart default settings list value
def get_cart_settings():
        cart_default_setting = carelink_unit_test.repository.setting_repo.get_cart_default_setting(
				[Setting_Enum.ACTIVE_CART_USER_ID.value,Setting_Enum.PIN_LOCKOUT_ATTEMPTS.value,Setting_Enum.ACTIVE.value])
        return cart_default_setting

# assign the variable to call the get_single_user
user_pin = get_single_user()

# encrypt the pin
def encrypted_pin():
        get_encrypt_pin = user_pin.pin.rstrip()
        pin_decrypt = decrypted_pin(get_encrypt_pin) 
        return pin_decrypt

data_bytes = QByteArray()
class TestAuthentication():
        """TestAuthentication  
        
        DESCRIPTION:This class contains the Unittest cases functions for authentication.      
        
        """
        # check the pin with none
        def test_1_01_pin_none(self):
                pin_none = "none"
                assert carelink_unit_test.security.validation_worker(pin_none) ==  False

        #check the pin with empty
        def test_1_02_pin_empty(self):
                pin_empty = ""
                assert carelink_unit_test.security.validation_worker(pin_empty)== False

        #  Validate rijndael  algorithm
        def test_1_03_get_pin(self): 
                get_encrypt_pin = user_pin.pin.rstrip()
                pin_decrypt = encrypted_pin()
                pin_encrypt =carelink_unit_test.security.encrypt(pin_decrypt)
                assert get_encrypt_pin ==  pin_encrypt

        # validate authenticate status         
        def test_1_04_authenticate_status(self):
                validate_pin = carelink_unit_test.security.validation_worker(encrypted_pin())
                assert validate_pin == True
                cart_default_setting = get_cart_settings()
                active_cart_user_id = int(cart_default_setting[Setting_Enum.ACTIVE_CART_USER_ID.value])
                assert active_cart_user_id == 1

        # validate failed attempt count
        def test_1_05_failed_attempt_count(self):
                carelink_unit_test.security.validation_worker("1234")
                assert carelink_unit_test.security._failed_login_attempt_count == 1
                carelink_unit_test.security.validation_worker("1234")
                assert carelink_unit_test.security._failed_login_attempt_count == 2
                carelink_unit_test.security.validation_worker("1234")
                assert carelink_unit_test.security._failed_login_attempt_count == 3
                carelink_unit_test.security.validation_worker("1234")
                assert carelink_unit_test.security._failed_login_attempt_count == 4
                carelink_unit_test.security.validation_worker("1234")
                assert carelink_unit_test.security._failed_login_attempt_count == 5

        # Validate login failed status        
        def test_1_06_max_login_attempt(self):
                cart_default_setting = get_cart_settings()
                pin_lockout_attempts = int(cart_default_setting[Setting_Enum.PIN_LOCKOUT_ATTEMPTS.value])
                cart_active = bool(int(cart_default_setting[Setting_Enum.ACTIVE.value]))
                carelink_unit_test.security.failed_validation(not cart_active)
                for _ in range(0,pin_lockout_attempts+1):
                        carelink_unit_test.security.validation_worker("123")
                assert carelink_unit_test.user_changed_event_args.cart_locked_out == True

        # Validate logout status
        def test_1_07_cart_user_logout(self):
                carelink_unit_test.security.logout()
                cart_default_setting = get_cart_settings()
                active_cart_user_id = int(cart_default_setting[Setting_Enum.ACTIVE_CART_USER_ID.value])
                assert active_cart_user_id == 0

        # Serial portlogin and logout
        # Write serial data when serial port is not opened        
        def test_1_08_Write_data_serial_when_its_not_opened(self):
                carelink_unit_test.home_presenter.close_serial_port()
                result = carelink_unit_test.home_presenter.send_response_back_to_host(ResponseType.NAK)
                assert result == False
        
        # Open serial port  
        def test_1_09_open_serial_port(self):
                result = carelink_unit_test.home_presenter.open_serial_port()
                assert result == True
        
        # Write login command to serial port               
        def test_1_10_Write_login_command_to_serial_port(self):
                data_bytes.clear()
                data_bytes.append("\x02p123456780010101\x0393")
                result = carelink_unit_test.home_presenter.serial_data_entered(data_bytes)
                assert result == True
        
        # Write junk data to serial port        
        def test_1_11_Write_junk_data_to_serial_port(self):
                data_bytes.clear()
                data_bytes.append("23563563/23988923")
                result = carelink_unit_test.home_presenter.serial_data_entered(data_bytes)
                assert result == False
        
        # Write logout data to serial port
        def test_1_12_Write_log_out_to_serial_port(self):
                data_bytes.clear()
                data_bytes.append("\x02L\x03AE")
                result = carelink_unit_test.home_presenter.serial_data_entered(data_bytes)
                assert result == True
                
        # Write invalid data to logout        
        def test_1_13_Write_invalid_data_to_log_out(self):
                data_bytes.clear()
                data_bytes.append("\x02L\x03ACDGAE")
                result = carelink_unit_test.home_presenter.serial_data_entered(data_bytes)
                assert result == False
                         
        # Close serial port
        def test_1_14_close_serial_port(self):
                result = carelink_unit_test.home_presenter.close_serial_port()
                assert result == True

        # Write the data when serial port after closed
        def test_1_15_Write_data_to_serial_port_after_closed(self):
                result = carelink_unit_test.home_presenter.send_response_back_to_host(ResponseType.NAK)
                assert result == False
                   
        # Open serial port after close and write the login data
        def test_1_16_open_serial_port_after_closed_write_login_data(self):
                carelink_unit_test.home_presenter.open_serial_port()
                data_bytes.clear()
                data_bytes.append("\x02p123456780010101\x0393")
                result = carelink_unit_test.home_presenter.serial_data_entered(data_bytes)
                assert result == True
                time.sleep(0.5)

        ## validate serial port failed attempt count
        def test_1_17_serial_failed_validation(self):
                data_bytes.clear()
                data_bytes.append("\x02p000233420010101\x03A9")
                carelink_unit_test.home_presenter.serial_data_entered(data_bytes)
                time.sleep(0.5)
                assert carelink_unit_test.security._failed_login_attempt_count == 1
                data_bytes.clear()
                data_bytes.append("\x02p000233420010101\x03A9")
                carelink_unit_test.home_presenter.serial_data_entered(data_bytes)
                time.sleep(0.5)
                assert carelink_unit_test.security._failed_login_attempt_count == 2
                data_bytes.clear()
                data_bytes.append("\x02p000233420010101\x03A9")
                carelink_unit_test.home_presenter.serial_data_entered(data_bytes)
                time.sleep(0.5)
                assert carelink_unit_test.security._failed_login_attempt_count == 3
                data_bytes.clear()
                data_bytes.append("\x02p000233420010101\x03A9")
                carelink_unit_test.home_presenter.serial_data_entered(data_bytes)
                time.sleep(0.5)
                assert carelink_unit_test.security._failed_login_attempt_count == 4
                data_bytes.clear()
                data_bytes.append("\x02p000233420010101\x03A9")
                carelink_unit_test.home_presenter.serial_data_entered(data_bytes)
                time.sleep(0.5)
                assert carelink_unit_test.security._failed_login_attempt_count == 5
        
        # Validate serial port max_login_attempt        
        def test_1_18_serial_port_max_login_attempt(self):
                cart_default_setting = get_cart_settings()
                pin_lockout_attempts = int(cart_default_setting[Setting_Enum.PIN_LOCKOUT_ATTEMPTS.value])
                cart_active = bool(int(cart_default_setting[Setting_Enum.ACTIVE.value]))
                carelink_unit_test.security.failed_validation(not cart_active)
                for _ in range(0,pin_lockout_attempts+1):
                        data_bytes.clear()
                        data_bytes.append("\x02p000233420010101\x03A9")
                        carelink_unit_test.home_presenter.serial_data_entered(data_bytes)
                        time.sleep(0.5)
                assert carelink_unit_test.user_changed_event_args.cart_locked_out == True
        
        # Validate on_security_current_user_changed with None input
        def test_1_19_on_security_current_user_changed(self):
                result = carelink_unit_test.home_presenter.onSecurityCurrentUserChanged(None)
                assert result == False

        # Validate on_security_current_user_changed with cart_locked_out is true
        def test_1_20_on_security_current_user_changed_with_cart_locked_out_true(self):
                carelink_unit_test.user_changed_event_args.current_user_changed(0, False, 
					True, False, False)
                result = carelink_unit_test.home_presenter.onSecurityCurrentUserChanged(
                        carelink_unit_test.user_changed_event_args)
                assert result == True
        
        # Validate on_security_current_user_changed with cart_inactive is true
        def test_1_21_on_security_current_user_changed_with_cart_inactive_true(self):
                carelink_unit_test.user_changed_event_args.current_user_changed(0, False, 
					False, False, True)
                result = carelink_unit_test.home_presenter.onSecurityCurrentUserChanged(
                        carelink_unit_test.user_changed_event_args)
                assert result == True

        # Validate on_security_current_user_changed with failed login attempt is true
        def test_1_22_on_security_current_user_changed_with_failed_login_attempt_true(self):
                carelink_unit_test.user_changed_event_args.current_user_changed(0, True, 
					False, False, False)
                result = carelink_unit_test.home_presenter.onSecurityCurrentUserChanged(
                        carelink_unit_test.user_changed_event_args)
                assert result == True
        # Dhurka - Please change the function name with meaningful.all false means what are the inputs
        # Validate on_security_current_user_changed with all False
        def test_1_23_on_security_current_user_changed_with_all_false(self):
                carelink_unit_test.user_changed_event_args.current_user_changed(0, False, 
					False, False, False)
                result = carelink_unit_test.home_presenter.onSecurityCurrentUserChanged(
                        carelink_unit_test.user_changed_event_args)
                assert result == True
        
        # Validate user logout state when activity monitor timer exceeds
        def test_1_24_start_logout_timer(self):
                carelink_unit_test.security.start_logout_timer(1)
                time.sleep(1.5)
                cart_default_setting = get_cart_settings()
                active_cart_user_id = int(cart_default_setting[Setting_Enum.ACTIVE_CART_USER_ID.value])
                assert active_cart_user_id == 0
                