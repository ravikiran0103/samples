from helper_file import Unit_test_object
carelink_unit_test = Unit_test_object()

class TestToolsFactoryReset():
        """TestToolsFactoryReset  
        
        Description:This class contains the Unittest cases functions for Factory Reset module.      
        
        """

        # verify set_factory_reset_value_for_settings
        def test_16_01_set_factory_reset_value_for_settings(self):
            result = carelink_unit_test.security.set_factory_reset_value_for_settings()
            assert result == True

        # Verify add_default_user
        def test_16_02_add_default_user(self):
            result = carelink_unit_test.security.add_default_user()
            assert result == True

        # verify set_factory_reset_value_for_user_settings
        def test_16_03_set_factory_reset_value_for_user_settings(self):
            result = carelink_unit_test.security.set_factory_reset_value_for_user_settings()
            assert result == True

        # verify set_factory_reset
        def test_16_04_set_factory_reset(self):
            result = carelink_unit_test.security.set_factory_reset()
            assert result == True
