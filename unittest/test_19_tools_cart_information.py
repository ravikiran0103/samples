from helper_file import Unit_test_object
carelink_unit_test = Unit_test_object()

class TestToolsCartInformation():
        """TestToolsCartInformation  
        
        Description:This class contains the Unittest cases functions for cart information module.      
        
        """
        
        # Verify update_static_values_to_view
        def test_18_01_update_static_values_to_view(self):
            result = carelink_unit_test.tool_cart_info.update_static_values_to_view()
            assert result == True

        # verify get_cart_default_values
        def test_18_02_get_cart_default_values(self):
            result = carelink_unit_test.tool_cart_info.get_cart_default_values()
            assert result == True

        # verify get_cart_power_data
        def test_18_03_get_cart_power_data(self):
            result = carelink_unit_test.tool_cart_info.get_cart_power_data()
            assert result == True

        # verify on_temperature_changed
        def test_18_04_on_temperature_changed(self):
            result = carelink_unit_test.tool_cart_info.onTemperatureChanged()
            assert result == True

     