import xml.dom.minidom
import os
import glob
from openpyxl import load_workbook
from openpyxl.styles import Color, Font
from openpyxl.styles.colors import RED
import configparser
    
def main():

    #Get the app version
    app_version = '1.0.9'

    #Get the Excel file name from the unit test dir
    path = '.'
    extension = 'xlsx'
    os.chdir(path)
    result = glob.glob('*.{}'.format(extension))

    if len(result) > 0 :
        filepath = result[0]
    else :
        filepath = "Carelink2.0_UnitTestCase.xlsx"

    print(filepath)

    #Rename the file name with currect application version number
    if app_version in filepath:
        print("Same Version")
    else:
        file_name = filepath.split("_")
        second_file_name = file_name[1].split(".")
        if len(file_name) > 0 :
            new_file_name = file_name[0] + "_" + second_file_name[0] + "_" + app_version + ".xlsx"
            print("File Path not equal. So rename it",filepath,new_file_name)
            os.rename(filepath,new_file_name)
            filepath = new_file_name

    doc = xml.dom.minidom.parse("result.xml")

    wb = load_workbook(filepath)
    ws = wb.active
    # Add application version number in the excel sheet
    ws['F2'] = app_version

    # print out the document node and the name of the first child tag
    print (doc.nodeName)
    print (doc.firstChild.tagName)

    # get a list of XML tags from the document and print each one
    testcases = doc.getElementsByTagName("testcase")
    print ("%d testcases:" % testcases.length)
    cell_row = 0

    for testsuite in testcases:
        c = testsuite.getAttribute("name")
        result = c.split("_")
        test_id = result[1] + "." + result[2]
        test_result = False
        test_fail = testsuite.getElementsByTagName("failure")
        if (test_fail.length > 0):
            test_result = "Fail"
        else:
            test_result = "Pass"
        for row in ws.iter_rows(min_row=13,min_col=4,max_col=4):
            for cell in row:
                if cell.value == float(test_id):
                    cell_row = cell.row
                    cellnumber = "I" + str(cell_row)
                    cell_text =  ws[cellnumber]
                    if test_result == "Pass":
                        cell_text.font=Font(color="FF008000")
                    else:
                        cell_text.font=Font(color=RED)

                    ws[cellnumber] = test_result
                    break            
    wb.save(filepath)
    print("Save the result successfully")

if __name__ == "__main__":
    main()
