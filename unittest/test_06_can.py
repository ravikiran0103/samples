import ctypes
from helper_file import Unit_test_object
carelink_unit_test = Unit_test_object()
from utilities.common_enums import CanResponseType

class TestCanBus():
    
    #write the data when can device not opened
    def test_6_01_write_data_to_can_when_can_device_not_opened(self):
        carelink_unit_test.can_bus.dispose()
        data_length = ctypes.c_uint32 (10)
        response_data = (ctypes.c_ubyte * 10)(100,0,0,0,0,0,0,0,0,0)
        result = carelink_unit_test.can_bus.process_CAN_response(
            CanResponseType.LIFT_MOVE_ABSOLUTE_RESPONSE, data_length,response_data
            )
        assert result == True
    
    # open the can device    
    def test_6_02_open_can_device(self):
        result = carelink_unit_test.can_bus.open_can_device()    
        assert result == 0

    # write data into the can device
    def test_6_03_write_data_to_can_device(self):
        data_length = ctypes.c_uint32 (5)
        response_data = (ctypes.c_ubyte * 10)(251,0,0,0,0,0,0,0,0,0)
        result = carelink_unit_test.can_bus.process_CAN_response(
            CanResponseType.LIFT_MOVE_ABSOLUTE_RESPONSE, data_length,response_data
            ) 
        assert result == True  
    
    # close the can device    
    def test_6_04_close_the_can_device(self):
        result = carelink_unit_test.can_bus.dispose()
        assert result == False
    
    # write data into the can device after closed  
    def test_6_05_write_data_can_device_after_closed(self):
        data_length = ctypes.c_uint32 (10)
        response_data = (ctypes.c_ubyte * 10)(100,0,0,0,0,0,0,0,0,0)
        result = carelink_unit_test.can_bus.process_CAN_response(
            CanResponseType.NSTRIDE_LOCK_RESPONSE, data_length,response_data
            ) 
        assert result == True        
    
    # open the can device after closed and write the data into can device
    def test_6_06_open_can_device_after_close_and_write_data_to_can_device(self):
        carelink_unit_test.can_bus.open_can_device()
        data_length = ctypes.c_uint32 (5)
        response_data = (ctypes.c_ubyte * 10)(100,0,0,0,0,0,0,0,0,0)
        result = carelink_unit_test.can_bus.process_CAN_response(
            CanResponseType.LIFT_MOVE_ABSOLUTE_RESPONSE, data_length,response_data
            ) 
        assert result == True   