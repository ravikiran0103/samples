from helper_file import Unit_test_object
carelink_unit_test = Unit_test_object()

class TestToolsConnectivity():
        """TestToolsConnectivity  
        
        Description:This class contains the Unittest cases functions for connectivity module.      
        
        """

        # verify update_values
        def test_18_01_update_values(self):
            result = carelink_unit_test.tool_connectivity.update_values()
            assert result == True

        # verify get_cart_url_and_interval_time
        def test_18_02_get_cart_url_and_interval_time(self):
            result = carelink_unit_test.tool_connectivity.get_cart_url_and_interval_time()
            assert result == True

        