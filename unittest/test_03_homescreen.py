from datetime import datetime
from datetime import timedelta
from helper_file import Unit_test_object
from utilities.common_enums import Setting_Enum
from utilities import helper
carelink_unit_test = Unit_test_object()

# get Utc_date_time_format
def get_date_time(utc_minutes,date_time_format):
    date_time_obj = (datetime.utcnow() + timedelta(minutes=utc_minutes))          
    return date_time_obj.strftime(helper.to_python_date_time_format(date_time_format))

# get cart_default_setting_datetime_format_offset_minutes
def get_cart_default_setting():
    cart_default_settings = \
                carelink_unit_test.repository.setting_repo.get_cart_default_setting(
                    [
                        Setting_Enum.DATE_TIME_FORMAT.value,
                        Setting_Enum.UTC_OFFSET_MINUTES.value
                    ]
                )
    return cart_default_settings            
                
class TestHomeScreen():
    """
    TestHomeScreen
    
    DESCRIPTION:This class contains the unittest case functions for the Home screen design.
    """
    # verify date time display in the status view with valid date
    def test_3_01_verify_datetime_display_with_valid_date(self):
        cart_default_settings = get_cart_default_setting()
        date_format = cart_default_settings[Setting_Enum.DATE_TIME_FORMAT.value]
        offset_minutes = int(cart_default_settings[Setting_Enum.UTC_OFFSET_MINUTES.value])
        date_time = carelink_unit_test.workspace_presenter.get_date_time(date_format,offset_minutes)
        show_date_time = get_date_time(offset_minutes,date_format)
        assert date_time == show_date_time
         
    # verify date time display in the status view with date format none and offset minutes 0    
    def test_3_02_datetime_display_dateformat_none_offset_zero(self):   
        date_format = None
        offset_minutes = 0
        date_time = carelink_unit_test.workspace_presenter.get_date_time(date_format,offset_minutes) 
        assert date_time == None
    
    # verify date time display in the status view with negative offset value     
    def test_3_03_datetime_display_with_negative_offset(self): 
        cart_default_settings = get_cart_default_setting() 
        date_format = cart_default_settings[Setting_Enum.DATE_TIME_FORMAT.value]
        offset_minutes = -60
        show_date_time = get_date_time(-60 ,date_format)
        date_time = carelink_unit_test.workspace_presenter.get_date_time(date_format,offset_minutes)
        assert date_time == show_date_time
    
    # verify date time display in the status view with invalid date    
    def test_3_04_datetime_display_with_invalid_date(self):
        cart_default_settings = get_cart_default_setting()    
        date_format = "M, dd MMM yyyy H:mm"
        offset_minutes = int(cart_default_settings[Setting_Enum.UTC_OFFSET_MINUTES.value])   
        show_date_time = get_date_time(offset_minutes,date_format)
        date_time = carelink_unit_test.workspace_presenter.get_date_time(date_format,offset_minutes)
        assert date_time == show_date_time
    
    # verify user & cart name in status view
    
    #verify user & cart name in status view with show cart name true
    def test_3_05_user_cart_name_statusview_cartname(self):
        login_name = True
        show_cart_name = True
        show_user_name = False
        user_name = "scot"
        cart_name = "stiles"
        user_cart_name_status = carelink_unit_test.workspace_presenter.get_name(login_name, user_name, cart_name, show_user_name ,show_cart_name)
        assert user_cart_name_status == cart_name
    
    # verify user & cart name in status view with login user true
    def test_3_06_user_cart_name_statusview_none(self):
        login_name = True
        show_user_name = False
        show_cart_name = False
        user_name = "scot"
        cart_name = "stiles"
        user_cart_name_status = carelink_unit_test.workspace_presenter.get_name(login_name, user_name, cart_name, show_user_name ,show_cart_name)
        assert user_cart_name_status == None
    
    # verify user & cart name in status view with show cart name true and login name true
    def test_3_07_user_cart_name_statusview_username(self):
        login_name = True
        show_user_name = True
        show_cart_name = False
        user_name = "scot"
        cart_name = "stiles"
        user_cart_name_status = carelink_unit_test.workspace_presenter.get_name(login_name, user_name, cart_name, show_user_name ,show_cart_name)
        assert user_cart_name_status == user_name
   
    # verify user & cart name in status view with show cart name true with log false
    def test_3_08_user_cart_name_statusview_cartname(self):
        login_name = False
        show_user_name = False
        show_cart_name = True
        user_name = "scot"
        cart_name = "stiles"
        user_cart_name_status = carelink_unit_test.workspace_presenter.get_name(login_name, user_name, cart_name, show_user_name ,show_cart_name)
        assert user_cart_name_status == cart_name

    # verify user & cart name in status view with show cart name will login,cart,user name is false
    def test_3_09_user_cart_name_statusview_none(self):
        login_name = False
        show_user_name = False
        show_cart_name = False
        user_name = "scot"
        cart_name = "stiles"
        user_cart_name_status = carelink_unit_test.workspace_presenter.get_name(login_name, user_name, cart_name, show_user_name ,show_cart_name)
        assert user_cart_name_status == None
    
    # verify user & cart name in status view with show cart name will login,cart,user name is none
    def test_3_10_user_cart_name_statusview_none(self):
        login_name = None
        show_user_name = None
        show_cart_name = None
        user_name = ""
        cart_name = ""
        user_cart_name_status = carelink_unit_test.workspace_presenter.get_name(login_name, user_name, cart_name, show_user_name ,show_cart_name)
        assert user_cart_name_status == None