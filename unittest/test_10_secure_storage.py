import ctypes
from helper_file import Unit_test_object
carelink_unit_test = Unit_test_object()
from utilities.common_enums import CanResponseType

class TestSecureStorage():
    
    # verify open secure storage on request with None input
    def test_10_01_secure_storage_open_close_request_none(self):
        result = carelink_unit_test.secure_storage.secure_storage_lock_request(None)
        assert result == False
    
    # verify open secure storage on request with False input
    def test_10_02_secure_storage_open_close_request_false(self):
        result = carelink_unit_test.secure_storage.secure_storage_lock_request(True)
        assert result == True
        
    # Verify open secure storage on request with True input
    def test_10_03_secure_storage_open_close_request_true(self):
        result = carelink_unit_test.secure_storage.secure_storage_lock_request(False)
        assert result == True

    # Verify the process response message with drawers locked response input    
    def test_10_04_processing_response_message_with_drawers_locked_response(self):
        result = carelink_unit_test.secure_storage.processing_response_message(
            CanResponseType.DRAWERS_LOCKED_RESPONSE.value,
            5,[0,0,0,0,0]
            )
        assert result == True

    # Verify the process response message with drawers unsolicited response input    
    def test_10_05_processing_response_message_with_drawers_unsolicited_response(self):
        result = carelink_unit_test.secure_storage.processing_response_message(
            CanResponseType.DRAWERS_UNSOLICITED_RESPONSE.value, 
            5,[0,0,0,0,0]
            )
        assert result == True

    # Verify the process response message with drawers error response input    
    def test_10_06_processing_response_message_with_drawers_error_response(self):
        result = carelink_unit_test.secure_storage.processing_response_message(
            CanResponseType.DRAWERS_ERROR_RESPONSE.value,
            5,[0,0,0,0,0]
            )
        assert result == True

    # Verify securestorage available local variable when secure_storage_presence with true input    
    def test_10_07_secure_storage_presence_with_status_true(self):
        carelink_unit_test.secure_storage.secure_storage_presence(True)
        assert carelink_unit_test.secure_storage.secure_storage_available == True 

    # Verify securestorage available local variable when secure_storage_presence with false input    
    def test_10_08_secure_storage_presence_with_status_false(self):
        carelink_unit_test.secure_storage.secure_storage_presence(False)
        assert carelink_unit_test.secure_storage.secure_storage_available == False

    # Verify securestorage locked local variable when secure_storage_locked_response with success message input    
    def test_10_09_secure_storage_locked_with_state_locked(self):
        carelink_unit_test.secure_storage.secure_storage_locked_response([0,1,1])
        assert carelink_unit_test.secure_storage.secure_storage_locked == True

    # Verify securestorage locked local variable when secure_storage_locked_response with other than success message input    
    def test_10_10_secure_storage_locked_with_state_unlocked(self):
        carelink_unit_test.secure_storage.secure_storage_locked_response([1,1,1])
        assert carelink_unit_test.secure_storage.secure_storage_locked == True