import sys 
import os
from pysqlcipher3 import dbapi2 as sqlcipher
sys.path.append('..')
sys.path.append('.')
from security.security import Security
from security.security import UserChangedEventArgs 
from sqlite.data_context import DataContext
from sqlite.repository import Repository
from security.security import Security
from diagnostics.logging.logger import Log
from platforms.premium_features_board import lift_device
from platforms.premium_features_board.nstride import NStride
from platforms.can.can_manager import CanManager
from gui.presenters.calculator_presenter import CalculatorPresenter
from gui.presenters.workspace_presenter import WorkspacePresenter
from gui.presenters.home_presenter import HomePresenter
from platforms.lock_board import secure_storage
from platforms.task_light_board.lights import Lights
from platforms.task_light_board.keyboard_light import KeyboardLight
from platforms.networking.networking import Networking
from gui.presenters.networking_presenter import NetworkingPresenter
from gui.presenters.notifications_alerts_presenter import NotificationsAlertsPresenter
from gui.presenters.service_request_presenter import ServiceRequestPresenter
from gui.presenters.tools_settings_presenter import SettingsPresenter
from platforms.system.display_brightness import DisplayBrightness
from platforms.system.volume import VolumeControl
from gui.presenters.tools_lift_calibration_presenter import ToolsLiftCalibrationPresenter
from gui.presenters.tools_connectivity_presenter import ToolConnectivityPresenter
from gui.presenters.tools_cart_information_presenter import ToolCartInformationPresenter
from platforms.system.resources import Resources

class Unit_test_object():
    def __init__(self):
        self.log = Log()
        self.can_bus = CanManager(self.log)
        self.secure_storage = secure_storage.SecureStorage(self.log, 
                                self.can_bus)
        self.nstride = NStride(self.log,self.can_bus)
        self.lift = lift_device.LiftDevice(self.log,self.can_bus)
        self.data_context = DataContext(self.log,"capsaDB.db")
        self.repository = Repository(self.data_context,self.log)
        self.security = Security(self.log,self.repository,None, 
                        self.secure_storage)
        self.workspace_presenter = WorkspacePresenter(self.repository, 
                                    self.secure_storage, self.nstride, 
                                    None, self.security,self.log)
        self.db_connection = self.data_context.data_base_connection
        self.user_changed_event_args = self.security._user_changed_event_args
        self.calculator = CalculatorPresenter(self.log)
        self.lights = Lights(self.log,self.can_bus)
        self.keyboard_light = KeyboardLight(self.log)
        self.home_presenter = HomePresenter(self.lift, self.repository, 
                            self.security, self.log, self.lights, 
                            self.secure_storage,self.keyboard_light)
        self.network = Networking(self.log)
        self.networking_presenter = NetworkingPresenter(self.log,self.network)
        self.notifications_alerts = NotificationsAlertsPresenter(
            self.log, self.repository, None, self.security)
        self.service_request = ServiceRequestPresenter(
            self.log, self.repository, None, self.security
        )
        self.volume = VolumeControl(self.log)
        self.display_brightness = DisplayBrightness(self.log)
        self.settings_presenter = SettingsPresenter(self.repository, 
                                                            self.security,
                                                            self.log,
                                                            self.display_brightness,
                                                            self.volume
                                                        )
        self.tools_lift_calibration = ToolsLiftCalibrationPresenter(
            self.log, self.repository, self.lift
        )
        self.tool_connectivity = ToolConnectivityPresenter(
            self.log, self.repository, None
        )
        self.resource = Resources(self.log)
        self.tool_cart_info = ToolCartInformationPresenter(
            self.log, self.repository, None, None, self.resource, self.network)
