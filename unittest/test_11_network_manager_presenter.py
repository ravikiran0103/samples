import ctypes
import subprocess

from helper_file import Unit_test_object
from utilities.common_enums import CanResponseType, NetworkAdapters
carelink_unit_test = Unit_test_object()

class TestNetworkManagerPresentor:

    # Verify validate_ip_address module with none IP address
    def test_11_01_validate_ip_address_with_none_input(self):
        result = carelink_unit_test.networking_presenter.validate_ip(None)
        assert result == False
    
    # Verify validate_ip_address module with IP address as empty string
    def test_11_02_validate_ip_address_with_empty_string(self):
        result = carelink_unit_test.networking_presenter.validate_ip("")
        assert result == False
    
    # Verify validate_ip_address module with invalid ip
    def test_11_03_validate_ip_address_with_invalid_ip_address(self):
        result = carelink_unit_test.networking_presenter.validate_ip("192")
        assert result == False

    # Verify validate_ip_address module with invalid ip
    def test_11_04_validate_ip_address_with_invalid_ip_address(self):
        result = carelink_unit_test.networking_presenter.validate_ip("192.")
        assert result == False

    # Verify validate_ip_address module with invalid ip
    def test_11_05_validate_ip_address_with_invalid_ip_address(self):
        result = carelink_unit_test.networking_presenter.validate_ip("192.8221")
        assert result == False

    # Verify validate_ip_address module with invalid ip
    def test_11_06_validate_ip_address_with_invalid_ip_address(self):
        result = carelink_unit_test.networking_presenter.validate_ip("192.8.")
        assert result == False

    # Verify validate_ip_address module with invalid ip
    def test_11_07_validate_ip_address_with_invalid_ip_address(self):
        result = carelink_unit_test.networking_presenter.validate_ip("192.82.132")
        assert result == False

    # Verify validate_ip_address module with invalid ip
    def test_11_08_validate_ip_address_with_invalid_ip_address(self):
        result = carelink_unit_test.networking_presenter.validate_ip("192.8.221.")
        assert result == False

    # Verify validate_ip_address module with invalid ip
    def test_11_09_validate_ip_address_with_invalid_ip_address(self):
        result = carelink_unit_test.networking_presenter.validate_ip("192.8.11.1111")
        assert result == False

    # Verify validate_ip_address module with invalid ip
    def test_11_10_validate_ip_address_with_invalid_ip_address(self):
        result = carelink_unit_test.networking_presenter.validate_ip("192.8.11.111.")
        assert result == False

    # Verify validate_ip_address module with invalid ip
    def test_11_11_validate_ip_address_with_invalid_ip_address(self):
        result = carelink_unit_test.networking_presenter.validate_ip("192.8.11.256")
        assert result == False

    # Verify validate_ip_address module with invalid ip
    def test_11_12_validate_ip_address_with_invalid_ip_address(self):
        result = carelink_unit_test.networking_presenter.validate_ip("192.8.11.25O")
        assert result == False

    # Verify validate_ip_address module with invalid ip
    def test_11_13_validate_ip_address_with_invalid_ip_address(self):
        result = carelink_unit_test.networking_presenter.validate_ip("192.8.aa.25O")
        assert result == False
    
    # Verify validate_ip_address module with valid ip address 
    def test_11_14_validate_ip_address_with_valid_ip_address(self):
        result = carelink_unit_test.networking_presenter.validate_ip("0.0.0.0")
        assert result == True

    #Reviewer - why we need the following 4 cases.Seems all are same
    #Dev - test cases 17 and 18 were used to validate boundry values such as 0 and 255
    #Dev - test cases 15 and 16 were used to validate the seems like same data. So I removed
    # Verify validate_ip_address module with valid ip address
    def test_11_15_validate_ip_address_with_valid_ip_address(self):
        result = carelink_unit_test.networking_presenter.validate_ip("192.168.7.114")
        assert result == True

    # Verify validate_ip_address module with valid ip address
    #def test_11_16_validate_ip_address_with_valid_ip_address(self):
     #   result = carelink_unit_test.networking_presenter.validate_ip("192.168.6.125")
      #  assert result == True

    # Verify validate_ip_address module with valid ip address
    def test_11_16_validate_ip_address_with_valid_ip_address(self):
        result = carelink_unit_test.networking_presenter.validate_ip("192.168.255.1")
        assert result == True

    # Verify validate_ip_address module with valid ip address
    def test_11_17_validate_ip_address_with_valid_ip_address(self):
        result = carelink_unit_test.networking_presenter.validate_ip("192.168.0.125")
        assert result == True

# -------------------------------validate_and_set_ip_address-----------------------------------------------

    # Verify validate_and_set_ip_address module with adapter type None
    def test_11_18_validate_and_set_ip_address_with_adapter_type_none(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_ip_address(None, "192.168.7.114")
        assert result == False

    # Verify validate_and_set_ip_address module with invalid adapter type
    def test_11_19_validate_and_set_ip_address_with_adapter_type_invalid(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_ip_address(4, "192.168.7.114")
        assert result == False

    #Reviewer for adapter cases we can pass some string also.pl add the case
    #Dev : done
    # Verify validate_and_set_ip_address module with adapter type NetworkAdapters.WIRED
    def test_11_20_validate_and_set_ip_address_with_adapter_type_network_adapters_wired_ip_address_none(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_ip_address(NetworkAdapters.WIRED,None)
        assert result == False

    # Verify validate_and_set_ip_address module with adapter type NetworkAdapters.WIRED and empty IP address 
    def test_11_21_validate_and_set_ip_address_with_adapter_type_network_adapters_wired_ip_address_empty(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_ip_address(NetworkAdapters.WIRED,"")
        assert result == False

    # Verify validate_and_set_ip_address module with adapter type NetworkAdapters.WIRED and invalid IP address 
    def test_11_22_validate_and_set_ip_address_with_adapter_type_network_adapters_wired_invalid_ip_address(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_ip_address(NetworkAdapters.WIRED,"192.168.6.342")
        assert result == False

    # Verify validate_and_set_ip_address module with adapter type NetworkAdapters.WIRED and valid IP address
    def test_11_23_validate_and_set_ip_address_with_adapter_type_network_adapters_wired_valid_ip_address(self):
        subprocess.run(
            ['ifconfig eth0 down'],
            shell=True
        )
        result = carelink_unit_test.networking_presenter.validate_and_set_ip_address(NetworkAdapters.WIRED,"192.168.7.114")
        assert result == True

    # Verify validate_and_set_ip_address module with adapter type NetworkAdapters.WIRELESS and IP address None
    def test_11_24_validate_and_set_ip_address_with_adapter_type_network_adapters_wireless_ip_address_none(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_ip_address(NetworkAdapters.WIRELESS,None)
        assert result == False

    # Verify validate_and_set_ip_address module with adapter type NetworkAdapters.WIRELESS and empty IP address
    def test_11_25_validate_and_set_ip_address_with_adapter_type_network_adapters_wireless_ip_address_empty(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_ip_address(NetworkAdapters.WIRELESS,"")
        assert result == False

    # Verify validate_and_set_ip_address module with adapter type NetworkAdapters.WIRELESS and invalid IP address
    def test_11_26_validate_and_set_ip_address_with_adapter_type_network_adapters_wireless_ip_address_invalid(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_ip_address(NetworkAdapters.WIRELESS,"192.168.6.256")
        assert result == False

    # Verify validate_and_set_ip_address module with adapter type NetworkAdapters.WIRELESS and valid IP address
    def test_11_27_validate_and_set_ip_address_with_adapter_type_network_adapters_wireless_ip_address_valid(self):
        subprocess.run(
            ['ifconfig wlan0 down'],
            shell=True
        )
        result = carelink_unit_test.networking_presenter.validate_and_set_ip_address(NetworkAdapters.WIRELESS,"192.168.7.114")
        assert result == True

    # -------------------------------validate_and_set_sub_netmask-----------------------------------------------

    # Verify validate_and_set_sub_netmask module with adapter type None
    def test_11_28_validate_and_set_sub_netmask_with_adapter_type_none(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_sub_netmask(None,"255.255.255.0")
        assert result == False
##
    # Verify validate_and_set_sub_netmask module with invalid adapter type
    def test_11_29_validate_and_set_sub_netmask_with_adapter_type_invalid(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_sub_netmask(4,"255.255.255.0")
        assert result == False

    # Verify validate_and_set_sub_netmask module with adapter type NetworkAdapters.WIRED
    def test_11_30_validate_and_set_sub_netmask_with_adapter_type_network_adapters_wired_ip_address_none(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_sub_netmask(NetworkAdapters.WIRED,None)
        assert result == False

    # Verify validate_and_set_sub_netmask module with adapter type NetworkAdapters.WIRED and empty IP address 
    def test_11_31_validate_and_set_sub_netmask_with_adapter_type_network_adapters_wired_ip_address_empty(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_sub_netmask(NetworkAdapters.WIRED,"")
        assert result == False

    # Verify validate_and_set_sub_netmask module with adapter type NetworkAdapters.WIRED and invalid IP address 
    def test_11_32_validate_and_set_sub_netmask_with_adapter_type_network_adapters_wired_invalid_ip_address(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_sub_netmask(NetworkAdapters.WIRED,"192.168.6.342")
        assert result == False

    # Verify validate_and_set_sub_netmask module with adapter type NetworkAdapters.WIRED and valid IP address
    def test_11_33_validate_and_set_sub_netmask_with_adapter_type_network_adapters_wired_valid_ip_address(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_sub_netmask(NetworkAdapters.WIRED,"255.255.255.0")
        assert result == True

    # Verify validate_and_set_sub_netmask module with adapter type NetworkAdapters.WIRELESS and IP address None
    def test_11_34_validate_and_set_sub_netmask_with_adapter_type_network_adapters_wireless_ip_address_none(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_sub_netmask(NetworkAdapters.WIRELESS,None)
        assert result == False

    # Verify validate_and_set_sub_netmask module with adapter type NetworkAdapters.WIRELESS and empty IP address
    def test_11_35_validate_and_set_sub_netmask_with_adapter_type_network_adapters_wireless_ip_address_empty(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_sub_netmask(NetworkAdapters.WIRELESS,"")
        assert result == False

    # Verify validate_and_set_sub_netmask module with adapter type NetworkAdapters.WIRELESS and invalid IP address
    def test_11_36_validate_and_set_sub_netmask_with_adapter_type_network_adapters_wireless_ip_address_invalid(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_sub_netmask(NetworkAdapters.WIRELESS,"192.168.6.256")
        assert result == False

    # Verify validate_and_set_sub_netmask module with adapter type NetworkAdapters.WIRELESS and valid IP address
    def test_11_37_validate_and_set_sub_netmask_with_adapter_type_network_adapters_wireless_ip_address_valid(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_sub_netmask(NetworkAdapters.WIRELESS,"255.255.255.0")
        assert result == True

    # -------------------------------validate_and_set_gateway-----------------------------------------------

    # Verify validate_and_set_gateway module with adapter type None
    def test_11_38_validate_and_set_gateway_with_adapter_type_none(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_gateway(None,"192.168.6.1")
        assert result == False
##    
    # Verify validate_and_set_gateway module with invalid adapter type
    def test_11_39_validate_and_set_gateway_with_adapter_type_invalid(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_gateway(7,"192.168.6.1")
        assert result == False

    # Verify validate_and_set_gateway module with adapter type NetworkAdapters.WIRED
    def test_11_40_validate_and_set_gateway_with_adapter_type_network_adapters_wired_ip_address_none(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_gateway(NetworkAdapters.WIRED, None)
        assert result == False

    # Verify validate_and_set_gateway module with adapter type NetworkAdapters.WIRED and empty IP address 
    def test_11_41_validate_and_set_gateway_with_adapter_type_network_adapters_wired_ip_address_empty(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_gateway(NetworkAdapters.WIRED,"")
        assert result == False

    # Verify validate_and_set_gateway module with adapter type NetworkAdapters.WIRED and invalid IP address 
    def test_11_42_validate_and_set_gateway_with_adapter_type_network_adapters_wired_ip_address_invalid(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_gateway(NetworkAdapters.WIRED,"192.168.6.342")
        assert result == False

    # Verify validate_and_set_gateway module with adapter type NetworkAdapters.WIRED and valid IP address
    def test_11_43_validate_and_set_gateway_with_adapter_type_network_adapters_wired_valid_ip_address_valid(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_gateway(NetworkAdapters.WIRED,"192.168.7.1")
        assert result == True

    # Verify validate_and_set_gateway module with adapter type NetworkAdapters.WIRELESS and IP address None
    def test_11_44_validate_and_set_gateway_with_adapter_type_network_adapters_wireless_ip_address_none(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_gateway(NetworkAdapters.WIRELESS,None)
        assert result == False

    # Verify validate_and_set_gateway module with adapter type NetworkAdapters.WIRELESS and empty IP address
    def test_11_45_validate_and_set_gateway_with_adapter_type_network_adapters_wireless_ip_address_empty(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_gateway(NetworkAdapters.WIRELESS,"")
        assert result == False

    # Verify validate_and_set_gateway module with adapter type NetworkAdapters.WIRELESS and invalid IP address
    def test_11_46_validate_and_set_gateway_with_adapter_type_network_adapters_wireless_ip_address_invalid(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_gateway(NetworkAdapters.WIRELESS,"192.168.6.256")
        assert result == False

    # Verify validate_and_set_gateway module with adapter type NetworkAdapters.WIRELESS and valid IP address
    def test_11_47_validate_and_set_gateway_with_adapter_type_network_adapters_wireless_ip_address_valid(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_gateway(NetworkAdapters.WIRELESS,"192.168.7.1")
        assert result == True

    # -------------------------------validate_and_set_primary_dns-----------------------------------------------

    # Verify validate_and_set_pri_dns module with adapter type None
    def test_11_48_validate_and_set_pri_dns_with_adapter_type_none(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_primary_dns(None,"192.168.6.1")
        assert result == False
##
    # Verify validate_and_set_pri_dns module with invalid adapter type
    def test_11_49_validate_and_set_pri_dns_with_adapter_type_invalid(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_primary_dns(7,"192.168.6.1")
        assert result == False

    # Verify validate_and_set_pri_dns module with adapter type NetworkAdapters.WIRED and DNS as None
    def test_11_50_validate_and_set_pri_dns_with_adapter_type_network_adapters_wired_dns_none(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_primary_dns(NetworkAdapters.WIRED,None)
        assert result == False

    # Verify validate_and_set_pri_dns module with adapter type NetworkAdapters.WIRED and empty DNS 
    def test_11_51_validate_and_set_pri_dns_with_adapter_type_network_adapters_wired_dns_empty(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_primary_dns(NetworkAdapters.WIRED,"")
        assert result == False

    # Verify validate_and_set_pri_dns module with adapter type NetworkAdapters.WIRED and invalid DNS
    def test_11_52_validate_and_set_pri_dns_with_adapter_type_network_adapters_wired_dns_invalid(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_primary_dns(NetworkAdapters.WIRED,"192.168.6.342")
        assert result == False

    # Verify validate_and_set_pri_dns module with adapter type NetworkAdapters.WIRED and valid DNS
    def test_11_53_validate_and_set_pri_dns_with_adapter_type_network_adapters_wired_dns_valid(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_primary_dns(NetworkAdapters.WIRED,"192.168.6.1")
        assert result == True

    # Verify validate_and_set_pri_dns module with adapter type NetworkAdapters.WIRELESS and DNS as None
    def test_11_54_validate_and_set_pri_dns_with_adapter_type_network_adapters_wireless_dns_none(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_primary_dns(NetworkAdapters.WIRELESS,None)
        assert result == False

    # Verify validate_and_set_pri_dns module with adapter type NetworkAdapters.WIRELESS and empty DNS
    def test_11_55_validate_and_set_pri_dns_with_adapter_type_network_adapters_wireless_dns_empty(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_primary_dns(NetworkAdapters.WIRELESS,"")
        assert result == False

    # Verify validate_and_set_pri_dns module with adapter type NetworkAdapters.WIRELESS and invalid DNS 
    def test_11_56_validate_and_set_pri_dns_with_adapter_type_network_adapters_wireless_dns_invalid(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_primary_dns(NetworkAdapters.WIRELESS,"192.168.6.256")
        assert result == False

    # Verify validate_and_set_pri_dns module with adapter type NetworkAdapters.WIRELESS and valid IP address
    def test_11_57_validate_and_set_pri_dns_with_adapter_type_network_adapters_wireless_dns_valid(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_primary_dns(NetworkAdapters.WIRELESS,"192.168.6.1")
        assert result == True

    # -------------------------------validate_and_set_secondary_dns-----------------------------------------------

    # Verify validate_and_set_secondary_dns module with adapter type None
    def test_11_58_validate_and_set_secondary_dns_with_adapter_type_none(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_secondary_dns(None,"192.168.6.1")
        assert result == False
##
    # Verify validate_and_set_secondary_dns module with invalid adapter type
    def test_11_59_validate_and_set_secondary_dns_with_adapter_type_invalid(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_secondary_dns(6,"192.168.6.1")
        assert result == False

    # Verify validate_and_set_secondary_dns module with adapter type NetworkAdapters.WIRED
    def test_11_60_validate_and_set_secondary_dns_with_adapter_type_network_adapters_wired_dns_none(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_secondary_dns(NetworkAdapters.WIRED,None)
        assert result == False

    # Verify validate_and_set_secondary_dns module with adapter type NetworkAdapters.WIRED and empty IP address 
    def test_11_61_validate_and_set_secondary_dns_with_adapter_type_network_adapters_wired_dns_empty(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_secondary_dns(NetworkAdapters.WIRED,"")
        assert result == False

    # Verify validate_and_set_secondary_dns module with adapter type NetworkAdapters.WIRED and invalid IP address 
    def test_11_62_validate_and_set_secondary_dns_with_adapter_type_network_adapters_wired_dns_invalid(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_secondary_dns(NetworkAdapters.WIRED,"192.168.6.342")
        assert result == False

    # Verify validate_and_set_secondary_dns module with adapter type NetworkAdapters.WIRED and valid IP address
    def test_11_63_validate_and_set_secondary_dns_with_adapter_type_network_adapters_wired_dns_valid(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_secondary_dns(NetworkAdapters.WIRED,"192.168.6.1")
        assert result == True

    # Verify validate_and_set_secondary_dns module with adapter type NetworkAdapters.WIRELESS and IP address None
    def test_11_64_validate_and_set_secondary_dns_with_adapter_type_network_adapters_wireless_dns_none(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_secondary_dns(NetworkAdapters.WIRELESS,None)
        assert result == False

    # Verify validate_and_set_secondary_dns module with adapter type NetworkAdapters.WIRELESS and empty IP address
    def test_11_65_validate_and_set_secondary_dns_with_adapter_type_network_adapters_wireless_dns_empty(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_secondary_dns(NetworkAdapters.WIRELESS,"")
        assert result == False

    # Verify validate_and_set_secondary_dns module with adapter type NetworkAdapters.WIRELESS and invalid IP address
    def test_11_66_validate_and_set_secondary_dns_with_adapter_type_network_adapters_wireless_dns_invalid(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_secondary_dns(NetworkAdapters.WIRELESS,"192.168.6.256")
        assert result == False

    # Verify validate_and_set_secondary_dns module with adapter type NetworkAdapters.WIRELESS and valid IP address
    def test_11_67_validate_and_set_secondary_dns_with_adapter_type_network_adapters_wireless_dns_valid(self):
        result = carelink_unit_test.networking_presenter.validate_and_set_secondary_dns(NetworkAdapters.WIRELESS,"192.168.6.1")
        assert result == True