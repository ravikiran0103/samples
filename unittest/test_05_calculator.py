from decimal import Decimal
from helper_file import Unit_test_object
from utilities.strings import Strings
carelink_unit_test = Unit_test_object()

class TestCalculator():
    
    #Addition operation
    # Add two positive numbers
    def test_5_01_add_two_positive_numbers(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("10","")
        carelink_unit_test.calculator.execute_update("20","+")
        assert carelink_unit_test.calculator.running_total == 30
        
    # Add one positive & one negative number    
    def test_5_02_add_one_postive_one_negative_number(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("20","")
        carelink_unit_test.calculator.execute_update("-5","+")
        assert carelink_unit_test.calculator.running_total == 15
    
    # Add one negative & one positive number    
    def test_5_03_add_one_negative_one_postive_number(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("-25","")
        carelink_unit_test.calculator.execute_update("10","+")
        assert carelink_unit_test.calculator.running_total == -15    
    
    # Add two negative numbers     
    def test_5_04_add_two_negative_numbers(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("-36","")
        carelink_unit_test.calculator.execute_update("-10","+")
        assert carelink_unit_test.calculator.running_total == -46
    
    # Add one positive number & zero   
    def test_5_05_add_one_positive_number_and_zero(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("25","")
        carelink_unit_test.calculator.execute_update("0","+")
        assert carelink_unit_test.calculator.running_total == 25  
    
    # Add one negative& one positive decimal number  
    def test_5_06_add_one_negative_one_positive_decimal_number(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("-12.5","")
        carelink_unit_test.calculator.execute_update("1.566","+")
        assert carelink_unit_test.calculator.running_total == Decimal('-10.934')
            
    # Add two decimal numbers
    def test_5_07_add_two_decimal_numbers(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("10.5","")
        carelink_unit_test.calculator.execute_update("0.5","+")
        assert carelink_unit_test.calculator.running_total == 11.0  
    
    # Add two zeros     
    def test_5_08_add_two_zeros(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("0","")
        carelink_unit_test.calculator.execute_update("0","+")
        assert carelink_unit_test.calculator.running_total == 0
    
    #Subtract Operation
    # Subtract two positive numbers    
    def test_5_09_subtract_two_positive_numbers(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("25","")
        carelink_unit_test.calculator.execute_update("5","-")
        assert carelink_unit_test.calculator.running_total == 20    
    
    # Subtract one positive & one negative number     
    def test_5_10_subtract_one_postive_one_negative_number(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("50","")
        carelink_unit_test.calculator.execute_update("-20","-")
        assert carelink_unit_test.calculator.running_total == 70
    
    # Subtract one negative & one positive number     
    def test_5_11_subtract_one_negative_one_postive_number(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("-51","")
        carelink_unit_test.calculator.execute_update("25","-")
        assert carelink_unit_test.calculator.running_total == -76  
    
    # Subtract two negative numbers     
    def test_5_12_subtract_two_negative_numbers(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("-10","")
        carelink_unit_test.calculator.execute_update("-5","-")
        assert carelink_unit_test.calculator.running_total == -5
    
    # Subtract one positive number & zero      
    def test_5_13_subtract_one_positive_number_and_zero(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("20","")
        carelink_unit_test.calculator.execute_update("0","-")
        assert carelink_unit_test.calculator.running_total == 20    
    
    # Subtract one negative& one positive decimal number  
    def test_5_14_subtract_one_negative_one_positive_decimal_number(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("-12.5","")
        carelink_unit_test.calculator.execute_update("1.566","-")
        assert carelink_unit_test.calculator.running_total == Decimal('-14.066')
            
    # subtract two decimal numbers
    def test_5_15_subtract_two_decimal_numbers(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("10.5","")
        carelink_unit_test.calculator.execute_update("0.5","-")
        assert carelink_unit_test.calculator.running_total == 10.0  
        
    # Subtract two zeros    
    def test_5_16_subtract_two_zeros(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("0","")
        carelink_unit_test.calculator.execute_update("0","-")
        assert carelink_unit_test.calculator.running_total == 0  
    
    #Multiplication operation
    # Multiply two positive numbers    
    def test_5_17_multiply_two_positive_numbers(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("10","")
        carelink_unit_test.calculator.execute_update("5","*")
        assert carelink_unit_test.calculator.running_total == 50    
     
    # Multiply one positive & one negative number 
    def test_5_18_multiply_one_postive_one_negative_number(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("25","")
        carelink_unit_test.calculator.execute_update("-4","*")
        assert carelink_unit_test.calculator.running_total == -100
    
    # multiply one negative & one positive number      
    def test_5_19_multiply_one_negative_one_postive_number(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("-10","")
        carelink_unit_test.calculator.execute_update("5","*")
        assert carelink_unit_test.calculator.running_total == -50    
    
    # Multiply two negative numbers   
    def test_5_20_multiply_two_negative_numbers(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("-8","")
        carelink_unit_test.calculator.execute_update("-6","*")
        assert carelink_unit_test.calculator.running_total == 48
    
    # Multiply one positive number & zero      
    def test_5_21_multiply_one_positive_number_and_zero(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("15","")
        carelink_unit_test.calculator.execute_update("0","*")
        assert carelink_unit_test.calculator.running_total == 0    
    
    # Multiply one negative& one positive decimal number  
    def test_5_22_multiply_one_negative_one_positive_decimal_number(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("-12.5","")
        carelink_unit_test.calculator.execute_update("1.566","*")
        assert carelink_unit_test.calculator.running_total == Decimal('-19.575')
            
    # multiply two decimal numbers
    def test_5_23_multiply_two_decimal_numbers(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("10.5","")
        carelink_unit_test.calculator.execute_update("0.5","*")
        assert carelink_unit_test.calculator.running_total == 5.25 
        
    # Multiply two zeros    
    def test_5_24_multiply_two_zeros(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("0","")
        carelink_unit_test.calculator.execute_update("0","*")
        assert carelink_unit_test.calculator.running_total == 0     
    
    #Division operation
    # Divide two positive numbers
    def test_5_25_divide_two_positive_numbers(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("15","")
        carelink_unit_test.calculator.execute_update("2","/")
        assert carelink_unit_test.calculator.running_total == 7.5    
    
    # Divide one positive & one negative number     
    def test_5_26_divide_one_postive_one_negative_number(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("50","")
        carelink_unit_test.calculator.execute_update("-3","/")
        assert carelink_unit_test.calculator.running_total == Decimal('-16.66666667')
    
    # Divide one negative & one positive number      
    def test_5_27_divide_one_negative_one_postive_number(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("-14","")
        carelink_unit_test.calculator.execute_update("3","/")
        assert carelink_unit_test.calculator.running_total == Decimal('-4.666666667')
    
    # Divide two negative numbers     
    def test_5_28_divide_two_negative_numbers(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("-7","")
        carelink_unit_test.calculator.execute_update("-9","/")
        assert carelink_unit_test.calculator.running_total == Decimal('0.7777777778')
    
    # Divide one positive number & zero      
    def test_5_29_divide_one_positive_number_and_zero(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("25","")
        carelink_unit_test.calculator.execute_update("0","/")
        assert carelink_unit_test.calculator.running_total == Strings.INFINITY
    
    # Divide zero & one positive number     
    def test_5_30_divide_zero_one_positive_number(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("0","")
        carelink_unit_test.calculator.execute_update("52","/")
        assert carelink_unit_test.calculator.running_total == 0
    
    # Divide one negative& one positive decimal number  
    def test_5_31_divide_one_negative_one_positive_decimal_number(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("-12.5","")
        carelink_unit_test.calculator.execute_update("1.566","/")
        assert carelink_unit_test.calculator.running_total == Decimal('-7.982120051')
            
    # Divide two decimal numbers
    def test_5_32_divide_two_decimal_numbers(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("10.5","")
        carelink_unit_test.calculator.execute_update("0.5","/")
        assert carelink_unit_test.calculator.running_total == 21
        
    # Divide two zeros    
    def test_5_33_divide_two_zeros(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("0","")
        carelink_unit_test.calculator.execute_update("0","/")
        assert carelink_unit_test.calculator.running_total == Strings.INFINITY
    
    # Memory Operation
    # Memory add of two numbers
    def test_5_34_memory_add_of_two_numbers(self):
        carelink_unit_test.calculator.memory_number = "10"
        carelink_unit_test.calculator.memory_sign = ""
        carelink_unit_test.calculator.onMemoryAdd()
        carelink_unit_test.calculator.memory_number = "5"
        carelink_unit_test.calculator.onMemoryAdd()
        assert carelink_unit_test.calculator.memory_value == 15
        
    # memory add of two decimal numbers
    def test_5_35_memory_add_of_two_decimal_numbers(self):
        carelink_unit_test.calculator.memory_value = 0
        carelink_unit_test.calculator.memory_number = "1.5"
        carelink_unit_test.calculator.memory_sign = ""
        carelink_unit_test.calculator.onMemoryAdd()
        carelink_unit_test.calculator.memory_number = "0.5"
        carelink_unit_test.calculator.onMemoryAdd()
        assert carelink_unit_test.calculator.memory_value == 2.0
           
    # Memory add one negative number & one positive number
    def test_5_36_memory_add_one_negative_one_positive_number(self):
        carelink_unit_test.calculator.memory_value = 0
        carelink_unit_test.calculator.memory_number = "-5"
        carelink_unit_test.calculator.memory_sign = ""
        carelink_unit_test.calculator.onMemoryAdd()
        carelink_unit_test.calculator.memory_number = "-15"
        carelink_unit_test.calculator.onMemoryAdd()
        assert carelink_unit_test.calculator.memory_value == -20
    
    # Memory subtract of two numbers     
    def test_5_37_memory_subtract_of_two_number(self):
        carelink_unit_test.calculator.memory_value = 0
        carelink_unit_test.calculator.memory_number = "8"
        carelink_unit_test.calculator.memory_sign = ""
        carelink_unit_test.calculator.onMemorySubtract()
        carelink_unit_test.calculator.memory_number = "4"
        carelink_unit_test.calculator.onMemorySubtract()
        assert carelink_unit_test.calculator.memory_value == -12 
        
    # Memory subtract of two decimal numbers
    def test_5_38_memory_subtract_of_two_decimal_numbers(self):
        carelink_unit_test.calculator.memory_value = 0
        carelink_unit_test.calculator.memory_number = "1.5"
        carelink_unit_test.calculator.memory_sign = ""
        carelink_unit_test.calculator.onMemorySubtract()
        carelink_unit_test.calculator.memory_number = "0.5"
        carelink_unit_test.calculator.onMemorySubtract()
        assert carelink_unit_test.calculator.memory_value == -2.0
        
    # Memory subtract one negative & one positive number    
    def test_5_39_memory_subtract_one_negative_one_positive_number(self):
        carelink_unit_test.calculator.memory_value = 0
        carelink_unit_test.calculator.memory_number = "-6"
        carelink_unit_test.calculator.memory_sign = ""
        carelink_unit_test.calculator.onMemorySubtract()
        carelink_unit_test.calculator.memory_number = "5"
        carelink_unit_test.calculator.onMemorySubtract()
        assert carelink_unit_test.calculator.memory_value == 1 
    
    # Memory add of two numbers & clear memory function    
    def test_5_40_memory_add_of_two_number_clear_memory_function(self):
        carelink_unit_test.calculator.onMemoryClear()
        assert carelink_unit_test.calculator.memory_value == 0    
    
    # Combination of arithmetic operation
    # Add two numbers & subtract one number    
    def test_5_41_add_two_numbers_and_subtract_one_number(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("25","")
        carelink_unit_test.calculator.execute_update("5","+")
        carelink_unit_test.calculator.execute_update("10","-")
        assert carelink_unit_test.calculator.running_total == 20
    
    # Add two numbers & multiply one number    
    def test_5_42_add_two_numbers_and_multiply_one_number(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("6","")
        carelink_unit_test.calculator.execute_update("5","+")
        carelink_unit_test.calculator.execute_update("2","*")
        assert carelink_unit_test.calculator.running_total == 22
    
    # Add two numbers & divide one number    
    def test_5_43_add_two_numbers_and_divide_one_number(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("8","")
        carelink_unit_test.calculator.execute_update("5","+")
        carelink_unit_test.calculator.execute_update("3","/")
        assert carelink_unit_test.calculator.running_total == Decimal('4.333333333')
    
    # Subtract two numbers & add one number     
    def test_5_44_subtract_two_numbers_and_add_one_number(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("25","")
        carelink_unit_test.calculator.execute_update("5","-")
        carelink_unit_test.calculator.execute_update("10","+")
        assert carelink_unit_test.calculator.running_total == 30    
    
    # Subtract two numbers & multiply one number  
    def test_5_45_subtract_two_numbers_and_multiply_one_number(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("85","")
        carelink_unit_test.calculator.execute_update("55","-")
        carelink_unit_test.calculator.execute_update("5","*")
        assert carelink_unit_test.calculator.running_total == 150 
    
    # Subtract two numbers & divide one number  
    def test_5_46_subtract_two_numbers_and_divide_one_number(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("12","")
        carelink_unit_test.calculator.execute_update("3","-")
        carelink_unit_test.calculator.execute_update("12","/")
        assert carelink_unit_test.calculator.running_total == 0.75   
    
    #  Multiply two numbers and add one number
    def test_5_47_multiply_two_numbers_and_add_one_number(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("8","")
        carelink_unit_test.calculator.execute_update("5","*")
        carelink_unit_test.calculator.execute_update("16","+")
        assert carelink_unit_test.calculator.running_total == 56     
    
    #  Multiply two numbers and subtract one number 
    def test_5_48_multiply_two_numbers_and_subtract_one_number(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("25","")
        carelink_unit_test.calculator.execute_update("4","*")
        carelink_unit_test.calculator.execute_update("17","-")
        assert carelink_unit_test.calculator.running_total == 83
    
    #  Multiply two numbers and divide one number      
    def test_5_49_multiply_two_numbers_and_divide_one_number(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("100","")
        carelink_unit_test.calculator.execute_update("5","*")
        carelink_unit_test.calculator.execute_update("18","/")
        assert carelink_unit_test.calculator.running_total == Decimal('27.77777778')
    
    # Divide two numbers and add one number   
    def test_5_50_divide_two_numbers_and_add_one_number(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("8","")
        carelink_unit_test.calculator.execute_update("5","/")
        carelink_unit_test.calculator.execute_update("19","+")
        assert carelink_unit_test.calculator.running_total == Decimal('20.6')
    
    # Divide two numbers and subtract one number   
    def test_5_51_divide_two_numbers_and_subtract_one_number(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("15","")
        carelink_unit_test.calculator.execute_update("3","/")
        carelink_unit_test.calculator.execute_update("20","-")
        assert carelink_unit_test.calculator.running_total == -15
    
    # Divide two numbers and multiply one number    
    def test_5_52_divide_two_numbers_and_multiply_one_number(self):
        carelink_unit_test.calculator.running_total = 0
        carelink_unit_test.calculator.execute_update("20","")
        carelink_unit_test.calculator.execute_update("4","/")
        carelink_unit_test.calculator.execute_update("21","*")
        assert carelink_unit_test.calculator.running_total == 105               