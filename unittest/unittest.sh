#!/bin/bash
export DISPLAY=:0

cp ../capsaDB.db .
echo "Database copied"

cp -r ../libs .
echo "Libraries copied"

pytest -v --junit-xml=result.xml
python3.5 report.py