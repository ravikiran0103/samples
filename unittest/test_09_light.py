import ctypes
from helper_file import Unit_test_object
carelink_unit_test = Unit_test_object()
from utilities.common_enums import Setting_Enum
from utilities.common_enums import CanResponseType
import time

class TestLights():
    
    # Verify groundlight on off request with None intensity input
    def test_9_01_ground_light_on_off_request_with_none_intensity(self):
        result = carelink_unit_test.lights.ground_light_on_off_request(None)
        assert result == False
    
    # Verify groundlight on off request with low intensity input
    def test_9_02_ground_light_on_off_request_with_low_intensity_zero(self):
        result = carelink_unit_test.lights.ground_light_on_off_request(0)
        assert result == True
    
    # Verify groundlight on off request with high intensity input
    def test_9_03_ground_light_on_off_request_with_high_intensity_255(self):
        result = carelink_unit_test.lights.ground_light_on_off_request(255)
        assert result == True 
    
    # Verify groundlight on off request with  intensity input between 0 to 255
    def test_9_04_ground_light_on_off_request_with_intermediate_intensity(self):
        result = carelink_unit_test.lights.ground_light_on_off_request(128)
        assert result == True 

    # Verify groundlight on off request with out of boundry negative intensity input
    def test_9_05_ground_light_on_off_request_with_negative_intensity(self):
        result = carelink_unit_test.lights.ground_light_on_off_request(-33)
        assert result == False 

    # Verify groundlight on off request with out of boundry positive intensity input
    def test_9_06_ground_light_on_off_request_with_intensity_above_255(self):
        result = carelink_unit_test.lights.ground_light_on_off_request(338)
        assert result == False 
    
    # Verify the process response message with ground light response as ground light intensity response
    def test_9_07_light_processing_response_with_ground_light_intensity_response(self):
        carelink_unit_test.lights.processing_response_message(CanResponseType.GROUND_LIGHT_INTENSITY_RESPONSE,11,[0,1,0,0,0,0,0,0,0,0])
        assert carelink_unit_test.lights.ground_light_available == True
        
    # Verify the process response message with ground light error response input    
    def test_9_08_light_processing_response_with_ground_light_error_response(self):
        carelink_unit_test.lights.processing_response_message(CanResponseType.GROUND_LIGHT_ERROR_RESPONSE,11,[0,1,0,0,0,0,0,0,0,0])
        assert carelink_unit_test.lights.ground_light_available == True

    # verify groundlight available local variable when ground light presence with true input     
    def test_9_09_ground_light_presence_with_availability_true(self):
        carelink_unit_test.lights.ground_light_presence(True)
        assert carelink_unit_test.lights.ground_light_available == True

    # verify groundlight available local variable when ground light presence with false input         
    def test_9_10_ground_light_presence_with_availability_false(self):
        carelink_unit_test.lights.ground_light_presence(False)
        assert carelink_unit_test.lights.ground_light_available == False
    
     # Verify toggle_ground_light module in home_screen_presentor when ground lights on
    def test_9_11_home_screen_presentor_toggle_ground_light_with_light_on(self):
        result = carelink_unit_test.home_presenter.toggle_ground_light(True)
        assert result == True
        
    # Verify toggle_ground_light module in home_screen_presentor when ground lights off
    def test_9_12_home_screen_presentor_toggle_ground_light_with_light_off(self):
        result = carelink_unit_test.home_presenter.toggle_ground_light(False)
        assert result == True
    
    # verify process ground light onoff response and send it status to presenter    
    def test_9_13_process_ground_light_onoff_response(self):
        result = carelink_unit_test.lights.process_ground_light_on_off_response([0,1,0,0,0,0,0,0,0,0])
        assert result == True

    # verify process ground light onoff response with no input data
    def test_9_14_process_ground_light_onoff_response_with_no_input(self):
        result = carelink_unit_test.lights.process_ground_light_on_off_response(None)
        assert result == False
    
    #------------------------------- surface light----------------------------------

    # verify surface light on_off request with None input
    def test_9_15_surface_light_on_off_request_with_none_intensity(self):
        result = carelink_unit_test.lights.surface_light_on_off_request(None)
        assert result == False     
    
    # verify surface light on_off request with Low intensity input
    def test_9_16_surface_light_on_off_request_with_low_intensity_zero(self):
        result = carelink_unit_test.lights.surface_light_on_off_request(0)
        assert result == True
    
    # verify surfacelight on off request with input to the intensity    
    def test_9_17_surface_light_on_off_request_with_high_intensity_255(self):
        result = carelink_unit_test.lights.surface_light_on_off_request(255)
        assert result == True 

    # verify surfacelight on off request with the intensity between 0 to 255
    def test_9_18_surface_light_on_off_request_with_intermediate_intensity(self):
        result = carelink_unit_test.lights.surface_light_on_off_request(115)
        assert result == True     
    
    # verify surfacelight on off request with the negative intensity
    def test_9_19_surface_light_on_off_request_with_negative_intensity(self):
        result = carelink_unit_test.lights.surface_light_on_off_request(-11)
        assert result == False

    # verify surfacelight on off request with the out of boundry positive intensity
    def test_9_20_surface_light_on_off_request_intensity_above_255(self):
        result = carelink_unit_test.lights.surface_light_on_off_request(321)
        assert result == False

    # Verify the process response message with response as light intensity response input     
    def test_9_21_light_processing_response_with_surface_light_intensity_response(self):
        carelink_unit_test.lights.processing_response_message(CanResponseType.SURFACE_LIGHT_INTENSITY_RESPONSE,11,[0,1,0,0,0,0,0,0,0,0])
        assert carelink_unit_test.lights.surface_light_available == True
    
    # Verify the process response message with error response         
    def test_9_22_light_processing_response_with_surface_light_error_response(self):
        carelink_unit_test.lights.processing_response_message(CanResponseType.SURFACE_LIGHT_ERROR_RESPONSE,11,[0,1,0,0,0,0,0,0,0,0])
        assert carelink_unit_test.lights.surface_light_available == True

    # Verify surfacelight available local variable when surface light presence with true input    
    def test_9_23_surface_light_presence_with_availability_true(self):
        carelink_unit_test.lights.surface_light_presence(True)
        assert carelink_unit_test.lights.surface_light_available == True
    
    # Verify surfacelight available local variable when surface light presence with false input             
    def test_9_24_surface_light_presence_availability_false(self):
        carelink_unit_test.lights.surface_light_presence(False)
        assert carelink_unit_test.lights.surface_light_available == False
    
    # Verify process surfacelight onoff response and send it status to presenter                   
    def test_9_25_process_surface_light_onoff_response(self):
        result = carelink_unit_test.lights.process_surface_light_on_off_response(
            [0,1,0,0,0,0,0,0,0,0])
        assert result == True    

    # Verify process surfacelight onoff response with no input data                 
    def test_9_26_process_surface_light_onoff_response_with_no_input(self):
        result = carelink_unit_test.lights.process_surface_light_on_off_response(
            None)
        assert result ==  False
        
    # Verify toggle_surface_light module in home_screen_presentor when surface lights on
    def test_9_27_home_screen_presentor_toggle_surface_light_with_light_on(self):
        result = carelink_unit_test.home_presenter.toggle_surface_light(True)
        assert result == True
        
    # Verify toggle_surface_light module in home_screen_presentor when surface lights off
    def test_9_28_home_screen_presentor_toggle_surface_light_with_light_off(self):
        result = carelink_unit_test.home_presenter.toggle_surface_light(False)
        assert result == True

    #------------------------------KEYBOARD LIGHT-------------------------------------
    # Verify calculate_light_code with lowest boundry value it accepts
    def test_9_29_calculate_light_code_with_low_boundry_value_zero(self):
        carelink_unit_test.keyboard_light.keyboard_light_intensity = 0
        result = carelink_unit_test.keyboard_light.calculate_light_code()
        assert result == "000000"

    # Verify calculate_light_code with highest boundry value it accepts
    def test_9_30_calculate_light_code_with_high_boundry_value_255(self):
        carelink_unit_test.keyboard_light.keyboard_light_intensity = 255
        result = carelink_unit_test.keyboard_light.calculate_light_code()
        assert result == "FFFFFF"

    # Verify calculate_light_code with the value in between 0 and 255
    def test_9_31_calculate_light_code_with_intermediate_value(self):
        carelink_unit_test.keyboard_light.keyboard_light_intensity = 101
        result = carelink_unit_test.keyboard_light.calculate_light_code()
        assert result == "656565"
    
    # Verify calculate_light_code with the negative value 
    def test_9_32_calculate_light_code_with_negative_value(self):
        old_light_code = carelink_unit_test.keyboard_light.keyboard_light_color
        carelink_unit_test.keyboard_light.keyboard_light_intensity = -10
        result = carelink_unit_test.keyboard_light.calculate_light_code()
        assert result == old_light_code

    # Verify calculate_light_code with the out of boundry positive value
    def test_9_33_calculate_light_code_with_intensity_above_255(self):
        old_light_code = carelink_unit_test.keyboard_light.keyboard_light_color
        carelink_unit_test.keyboard_light.keyboard_light_intensity = 586
        result = carelink_unit_test.keyboard_light.calculate_light_code()
        assert result == old_light_code

    # Verify calculate_light_code with None input
    def test_9_34_calculate_light_code_with_none_value(self):
        old_light_code = carelink_unit_test.keyboard_light.keyboard_light_color
        carelink_unit_test.keyboard_light.keyboard_light_intensity = None
        result = carelink_unit_test.keyboard_light.calculate_light_code()
        assert result == old_light_code

    # Verify toggle_keyboard_light when keyboard lights on 
    def test_9_35_toggle_keyboard_light_with_light_on(self):        
        result = carelink_unit_test.keyboard_light.toggle_keyboard_light(True)
        assert result == True

    # Verify toggle_keyboard_light when keyboard lights off
    def test_9_36_toggle_keyboard_light_with_light_off(self):
        result = carelink_unit_test.keyboard_light.toggle_keyboard_light(False)
        assert result == True

    # Verify toggle_keyboard_light module in home_screen_presentor when keyboard lights on
    def test_9_37_home_screen_presentor_toggle_keyboard_light_with_light_on(self):
        result = carelink_unit_test.home_presenter.toggle_keyboard_light(True)
        assert result == True
        
    # Verify toggle_keyboard_light module in home_screen_presentor when keyboard lights off
    def test_9_38_home_screen_presentor_toggle_keyboard_light_with_light_off(self):
        result = carelink_unit_test.home_presenter.toggle_keyboard_light(False)
        assert result == True
    
    # Verify toggle_keyboard_light module in home_screen_presentor when with None input
    def test_9_39_home_screen_presentor_toggle_keyboard_light_with_light_off(self):
        result = carelink_unit_test.home_presenter.toggle_keyboard_light(None)
        assert result == False