import glob
import linecache


lookup = 'def test_'

def filebrowser(word=""):
	
	file = []
	for f in glob.glob("*"):

		if word in f:
			file.append(f)

	file.sort()
	return file

def get_case_id_procedure(file):
	with open(file) as myFile:

		for num, line in enumerate(myFile, 1):

			if lookup in line:

				prefix_index = line.index("test") + len("test_")
				sufix_index = line.index("(")

				test_case = line[prefix_index:sufix_index]
				second_occur_index = test_case.find(
					"_",
					test_case.find("_") + 1
				)
				test_case_id = test_case[0:second_occur_index ]
				test_case_id = test_case_id.replace("_", ".")

				procedure = linecache.getline(file, (num - 1))

				if "#" in procedure:
					procedure = procedure[
						(procedure.index("#") + len("#")) : len(procedure)
					]

				print(test_case_id, procedure)


unit_test_files = filebrowser("test_")
for file in unit_test_files:
	get_case_id_procedure(file)