from helper_file import Unit_test_object
carelink_unit_test = Unit_test_object()
from utilities.common_enums import CanResponseType
from utilities.common_enums import Setting_Enum
from models.user_model import UserModel
import time

class TestLift():
    
    # Verify lift target position to move the lift device
    def test_7_01_lift_target_position(self):
        result = carelink_unit_test.lift.lift_move_target_position_request(160)
        assert result == True
     
    # verify current postion local variable when response get from the can command
    def test_7_02_lift_response(self):
        carelink_unit_test.lift.process_lift_current_position_response([0, 84, 0, 0, 0, 0, 0, 0, 0, 0])
        assert carelink_unit_test.lift.lift_current_position == 84  
     
    # verify lift stop request function to stop the lift immediately    
    def test_7_03_lift_stop_request(self):
        result = carelink_unit_test.lift.lift_stop_request()     
        assert result == True
    
    # verify lift moveup request function to start the lift moveup
    def test_7_04_lift_move_up_request(self):
        result = carelink_unit_test.lift.lift_move_up_request()
        assert result == True
      
    # verify lift movedown request function to start the lift movedown
    def test_7_05_lift_move_down_request(self):
        result = carelink_unit_test.lift.lift_move_down_request()  
        assert result == True
    
    # verify lift current position request for getting lift current position   
    def test_7_06_lift_current_postion_request(self):
        result = carelink_unit_test.lift.lift_current_position_request()  
        assert result == True  
     
    # lift processing response with lift position response input   
    def test_7_07_lift_processing_response_with_lift_postion_response(self):
        carelink_unit_test.lift.processing_response_message(CanResponseType.LIFT_POSITION_RESPONSE,10,[0, 190, 0, 0, 0, 0, 0, 0, 0, 0])
        assert carelink_unit_test.lift.lift_current_position == 190 
      
    #lift processing response with lift unsolicated position response input    
    def test_7_08_lift_processing_response_with_lift_unsolicited_postion_response(self):
        carelink_unit_test.lift.processing_response_message(CanResponseType.LIFT_UNSOLICITED_POSITION_RESPONSE,10,[100, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        assert carelink_unit_test.lift.lift_current_position == 100

    # Verify onLiftPresetSelected module in home_screen_presentor on stand position
    def test_7_09_on_lift_preset_selector_with_state_lift_stand(self):
        result = carelink_unit_test.home_presenter.onLiftPresetSelected(True)
        assert result == True
    
    # Verify onLiftPresetSelected module in home_screen_presentor on sit position
    def test_7_10_on_lift_preset_selector_with_state_lift_sit(self):
        result = carelink_unit_test.home_presenter.onLiftPresetSelected(False)
        assert result == True

    # Verify onLiftPresetRequestSave module in home_screen_presentor on stand position
    def test_7_11_on_lift_preset_selector_request_save_with_state_lift_stand(self):
        self.cart_user = self.get_single_user()
        carelink_unit_test.home_presenter._security.active_user = self.cart_user
        carelink_unit_test.home_presenter.onLiftPresetRequestSave(True)
        time.sleep(1)
        value = carelink_unit_test.repository.user_setting_repo.get_user_settings(
                    carelink_unit_test.home_presenter._security.active_user.user_id,
                    [
                        Setting_Enum.LIFT_PRESET_STANDING.value
                    ])
        assert int(value[Setting_Enum.LIFT_PRESET_STANDING.value]) == carelink_unit_test.lift.lift_current_position
        
    # Verify onLiftPresetRequestSave module in home_screen_presentor on sit position
    def test_7_12_on_lift_preset_selector_request_save_with_state_lift_sit(self):
        self.cart_user = self.get_single_user()
        carelink_unit_test.home_presenter._security.active_user = self.cart_user
        carelink_unit_test.home_presenter.onLiftPresetRequestSave(False)
        time.sleep(1)
        value = carelink_unit_test.repository.user_setting_repo.get_user_settings(
                    carelink_unit_test.home_presenter._security.active_user.user_id,
                    [
                        Setting_Enum.LIFT_PRESET_SITTING.value
                    ])
        assert int(value[Setting_Enum.LIFT_PRESET_SITTING.value]) == carelink_unit_test.lift.lift_current_position
        
    # Verify process_unwanted_lift_position_response with CAN response data
    def test_7_13_process_unwanted_lift_position_response(self):
        result = carelink_unit_test.lift.process_unwanted_lift_position_response([120,0,0,0,0,0,0,0,0,0])
        assert result == True

    # Verify process_unwanted_lift_position_response with no data
    def test_7_14_process_unwanted_lift_position_response_with_no_input(self):
        result = carelink_unit_test.lift.process_unwanted_lift_position_response(None)
        assert result == False

    # Verify process_lift_current_position_response with CAN response data
    def test_7_15_process_lift_current_position_response(self):
        result = carelink_unit_test.lift.process_lift_current_position_response([0,120,0,0,0,0,0,0,0,0])
        assert result == True

    # Verify process_lift_current_position_response with no data
    def test_7_16_process_lift_current_position_response_with_no_input(self):
        result = carelink_unit_test.lift.process_lift_current_position_response(None)
        assert result == False

    # get single user from usermodel
    def get_single_user(self):
        try: 
            db = carelink_unit_test.db_connection.cursor()
            query = "SELECT * FROM User limit 1"
            records = db.execute(query).fetchone()
            if(records != None):
                return UserModel(
                        records[0], records[1], records[2],
                        records[3], records[4], records[5], records[6]
                )

            return None

        except Exception as e:
                print("UserRepository - getSingle failed ", e)
                return False