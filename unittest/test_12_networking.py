import ctypes
from helper_file import Unit_test_object
from utilities.common_enums import CanResponseType, NetworkAdapters
from utilities.common_configs import NETWORK_LIBRARY_FILE,	\
	KeyManagementFromString, EAPTypeFromString,	\
	PskFromKeyManagement, KeyManagementToString,	\
	network_config_file_path, access_point_file_path
from platforms.networking.networking import WifiConfig
from utilities.strings import Strings
import configparser

carelink_unit_test = Unit_test_object()
import time

#Reviewer - Move the all const value to common files.
#Dev - These values are used in this file only
IP_ADDRESS = "192.168.7.114"
GATEWAY = "192.168.7.1"
PRIMARY_DNS = "192.168.6.4"
SECONDARY_DNS = "192.168.6.1"
SUBNET_MASK = "255.255.255.0"

#Reviewer - Check all test cases with wrong ip,DNS,Subnetmask,gateway ,mismatch format cases
#Dev - All cases were already checked in test_11_network_manager_presenter.py 
class TestNetworkManager:

    # Verify set_ip_address module for adapter type NetworkAdapters.WIRED on DHCP disable state
    #Reviewer - what will happen if we give any string to adapter type.Same for all other cases
    #Dev - These conditions were checked in test_11_network_manager_presenter.py file
    def test_12_01_set_ip_address_with_wired_adapter_valid_ip_address_on_dhcp_disabled(self):
        carelink_unit_test.network.set_ip_address(NetworkAdapters.WIRED,IP_ADDRESS)
        assert carelink_unit_test.network.get_ip_address(NetworkAdapters.WIRED) == IP_ADDRESS
    
    # Verify set_ip_address module for adapter type NetworkAdapters.WIRELESS on DHCP disable state
    def test_12_02_set_ip_address_with_wireless_adapter_valid_ip_address_on_dhcp_disabled(self):
        carelink_unit_test.network.set_ip_address(NetworkAdapters.WIRELESS,IP_ADDRESS)
        assert carelink_unit_test.network.get_ip_address(NetworkAdapters.WIRELESS) == IP_ADDRESS

    # Verify set_subnet_mask module for adapter type NetworkAdapters.WIRED on DHCP disable state
    def test_12_03_set_subnet_mask_with_wired_adapter_valid_subnet_mask_on_dhcp_disabled(self):
        carelink_unit_test.network.set_subnet_mask(NetworkAdapters.WIRED,SUBNET_MASK)
        assert carelink_unit_test.network.get_subnet_mask(NetworkAdapters.WIRED) == SUBNET_MASK
    
    # Verify set_subnet_mask module for adapter type NetworkAdapters.WIRELESS on DHCP disable state
    def test_12_04_set_subnet_mask_with_wireless_adapter_valid_subnet_mask_on_dhcp_disabled(self):
        carelink_unit_test.network.set_subnet_mask(NetworkAdapters.WIRELESS,SUBNET_MASK)
        assert carelink_unit_test.network.get_subnet_mask(NetworkAdapters.WIRELESS) == SUBNET_MASK

    # Verify set_gateway_address module for adapter type NetworkAdapters.WIRED On DHCP disabled state 
    def test_12_05_set_gateway_address_with_wired_adapter_valid_gateway_address_on_dhcp_disabled(self):
        carelink_unit_test.network.set_gateway_address(NetworkAdapters.WIRED,GATEWAY)
        assert carelink_unit_test.network.get_gateway_address(NetworkAdapters.WIRED) == GATEWAY
    
    # Verify set_gateway_address module for adapter type NetworkAdapters.WIRELESS  On DHCP disabled state 
    def test_12_06_set_gateway_address_with_wireless_adapter_valid_gateway_address_on_dhcp_disabled(self):
        carelink_unit_test.network.set_gateway_address(NetworkAdapters.WIRELESS,GATEWAY)
        assert carelink_unit_test.network.get_gateway_address(NetworkAdapters.WIRELESS) == GATEWAY

    #Verify set_dhcp_enabled module with adapter type NetworkAdapters.WIRED  and DHCP enabled state 
    def test_12_07_set_dhcp_enabled_with_wired_adapter_dhcp_enalbed(self):
        carelink_unit_test.network.set_dhcp_enabled(NetworkAdapters.WIRED,True)
        assert carelink_unit_test.network.get_dhcp_enabled(NetworkAdapters.WIRED) == True

    #Verify set_dhcp_enabled module with adapter type NetworkAdapters.WIRED  and DHCP disabled state 
    def test_12_08_set_dhcp_enabled_with_wired_adapter_dhcp_disabled(self):
        carelink_unit_test.network.set_dhcp_enabled(NetworkAdapters.WIRED,False)
        assert carelink_unit_test.network.get_dhcp_enabled(NetworkAdapters.WIRED) == False
    
    #Verify set_dhcp_enabled module with adapter type NetworkAdapters.WIRELESS  and DHCP enabled state 
    def test_12_09_set_dhcp_enabled_with_wireless_adapter_dhcp_enalbed(self):
        carelink_unit_test.network.set_dhcp_enabled(NetworkAdapters.WIRELESS,True)
        assert carelink_unit_test.network.get_dhcp_enabled(NetworkAdapters.WIRELESS) == True
    
    #Verify set_dhcp_enabled module with adapter type NetworkAdapters.WIRELESS  and DHCP disabled state 
    def test_12_10_set_dhcp_enabled_with_wireless_adapter_dhcp_disalbed(self):
        carelink_unit_test.network.set_dhcp_enabled(NetworkAdapters.WIRELESS,False)
        assert carelink_unit_test.network.get_dhcp_enabled(NetworkAdapters.WIRELESS) == False
    
    # Verify set_primary_dns modules for adapter type NetworkAdapter.WIRED
    def test_12_11_set_primary_dns_with_wired_adapter(self):
        result = carelink_unit_test.network.set_primary_dns(NetworkAdapters.WIRED,PRIMARY_DNS)
        assert result == True

    # Verify set_secondary_dns modules for adapter type NetworkAdapter.WIRED
    def test_12_12_set_secondary_dns_with_wired_adapter(self):
        result = carelink_unit_test.network.set_secondary_dns(NetworkAdapters.WIRED,SECONDARY_DNS)
        assert result == True
    
    # Verify get_dns modules for adapter type NetworkAdapter.WIRED
    def test_12_13_get_dns_with_wired_adapter(self):
        result = carelink_unit_test.network.get_dns(NetworkAdapters.WIRED)
        assert result == [PRIMARY_DNS,SECONDARY_DNS]

    # Verify set_primary_dns modules for adapter type NetworkAdapter.WIRELESS
    def test_12_14_set_primary_dns_with_wireless_adapter(self):
        result = carelink_unit_test.network.set_primary_dns(NetworkAdapters.WIRELESS,PRIMARY_DNS)
        assert result == True

    # Verify set_secondary_dns modules for adapter type NetworkAdapter.WIRELESS
    def test_12_15_set_secondary_dns_with_wireless_adapter(self):
        result = carelink_unit_test.network.set_secondary_dns(NetworkAdapters.WIRELESS,SECONDARY_DNS)
        assert result == True
    
    # Verify get_dns modules for adapter type NetworkAdapter.WIRELESS
    def test_12_16_get_dns_with_wireless_adapter(self):
        result = carelink_unit_test.network.get_dns(NetworkAdapters.WIRELESS)
        assert result == [PRIMARY_DNS,SECONDARY_DNS]

    # Verify set_adapter module for adapter type NetworkAdapter.WIRED
    def test_12_17_set_adapter_with_wireless_adapter(self):
        result = carelink_unit_test.network.set_adapter(NetworkAdapters.WIRED)
        assert result == True
    #Reviewer - Did you tested this case with/without adapter
    #yes
    # Verify set_adapter module for adapter type NetworkAdapter.WIRELESS
    def test_12_18_set_adapter_with_wireless_adapter(self):
        result = carelink_unit_test.network.set_adapter(NetworkAdapters.WIRELESS)
        assert result == True

    # Verify get_adapter_status module for adapter type NetworkAdapter.WIRED
    def test_12_19_get_adapter_status_with_wired_adapter(self):
        result = carelink_unit_test.network.get_adapter_status(NetworkAdapters.WIRED)
        assert result != None

    # Verify get_adapter_status module for adapter type NetworkAdapter.WIRELESS
    def test_12_20_get_adapter_status_with_wireless_adapter(self):
        result = carelink_unit_test.network.get_adapter_status(NetworkAdapters.WIRELESS)
        assert result != None

#Reviewer - Did you check this cases ? They are directly checking len(ssid) instead of None value.It will be crash 
#Dev - yeah. Check test case 12_22
#Check all cases
	# Verify add_wireless_network module with ssid as none
    def test_12_21_add_wireless_network_with_ssid_none(self):
        wifi_config = WifiConfig()
        wifi_config.ssid = None
        wifi_config.key_mgmt = "WPA_PSK"
        wifi_config.psk = "12345abcde"
        wifi_config.encrypt_mode = "CCMP TKIP"
        wifi_config.eap_type = None
        wifi_config.user = None
        wifi_config.pwd = None
        result = carelink_unit_test.network.add_wireless_network(
            [wifi_config]
        )
        assert result == False

	# Verify add_wireless_network module with ssid as empty string
    def test_12_22_add_wireless_network_with_ssid_empty(self):
        wifi_config = WifiConfig()
        wifi_config.ssid = ""
        wifi_config.key_mgmt = "WPA_PSK"
        wifi_config.psk = "12345abcde"
        wifi_config.encrypt_mode = "CCMP TKIP"
        wifi_config.eap_type = None
        wifi_config.user = None
        wifi_config.pwd = None
        result = carelink_unit_test.network.add_wireless_network(
            [wifi_config]
        )
        assert result == False

	# Verify add_wireless_network module with key_management as none
    #Reviewer - In new code they updated key_management as enum.Update the case
    #Dev - I used strings instead of enum.because in application, we get the string from config file.
    def test_12_23_add_wireless_network_with_key_mgmt_none(self):
        wifi_config = WifiConfig()
        wifi_config.ssid = "test123"
        wifi_config.key_mgmt = None
        wifi_config.psk = "12345abcde"
        wifi_config.encrypt_mode = "CCMP TKIP"
        wifi_config.eap_type = None
        wifi_config.user = None
        wifi_config.pwd = None
        result = carelink_unit_test.network.add_wireless_network(
            [wifi_config]
        )
        assert result == False

	# Verify add_wireless_network module with key_management as empty
    def test_12_24_add_wireless_network_with_key_mgmt_empty(self):
        wifi_config = WifiConfig()
        wifi_config.ssid = "test123"
        wifi_config.key_mgmt = ""
        wifi_config.psk = "12345abcde"
        wifi_config.encrypt_mode = "CCMP TKIP"
        wifi_config.eap_type = None
        wifi_config.user = None
        wifi_config.pwd = None
        result = carelink_unit_test.network.add_wireless_network(
            [wifi_config]
        )
        assert result == False

	# Verify add_wireless_network module with invalid key_management 
    def test_12_25_add_wireless_network_with_key_mgmt_invalid(self):
        wifi_config = WifiConfig()
        wifi_config.ssid = "test123"
        wifi_config.key_mgmt = "test_11"
        wifi_config.psk = "12345abcde"
        wifi_config.encrypt_mode = "CCMP TKIP"
        wifi_config.eap_type = None
        wifi_config.user = None
        wifi_config.pwd = None
        result = carelink_unit_test.network.add_wireless_network(
            [wifi_config]
        )
        assert result == False

	# Verify add_wireless_network module with psk as None
    def test_12_26_add_wireless_network_with_psk_none(self):
        wifi_config = WifiConfig()
        wifi_config.ssid = "test123"
        wifi_config.key_mgmt = "WPA_PSK"
        wifi_config.psk = None
        wifi_config.encrypt_mode = "CCMP TKIP"
        wifi_config.eap_type = None
        wifi_config.user = None
        wifi_config.pwd = None
        result = carelink_unit_test.network.add_wireless_network(
            [wifi_config]
        )
        assert result == False

	# Verify add_wireless_network module with empty psk
    def test_12_27_add_wireless_network_with_empty_psk(self):
        wifi_config = WifiConfig()
        wifi_config.ssid = "test123"
        wifi_config.key_mgmt = "WPA_PSK"
        wifi_config.psk = ""
        wifi_config.encrypt_mode = "CCMP TKIP"
        wifi_config.eap_type = None
        wifi_config.user = None
        wifi_config.pwd = None
        result = carelink_unit_test.network.add_wireless_network(
            [wifi_config]
        )
        assert result == False

	# Verify add_wireless_network module with valid data
    def test_12_28_add_wireless_network_with_valid_data(self):
        wifi_config = WifiConfig()
        wifi_config.ssid = "test123"
        wifi_config.key_mgmt = "WPA_PSK"
        wifi_config.psk = "12345abcde"
        wifi_config.encrypt_mode = "CCMP TKIP"
        wifi_config.eap_type = None
        wifi_config.user = None
        wifi_config.pwd = None
        result = carelink_unit_test.network.add_wireless_network(
            [wifi_config]
        )
        assert result == True

    # verify dhcp_thread module with invalid network adapter
    def test_12_29_dhcp_thread_wireless_with_adapter_type_invalid(self):
        result = carelink_unit_test.network.dhcp_thread(5)
        assert result == False
        pass

    # verify dhcp_thread module with wireless network adapter
    def test_12_30_dhcp_thread_with_wireless_adapter(self):
        result = carelink_unit_test.network.dhcp_thread(NetworkAdapters.WIRELESS)
        assert result == True
        pass
    # verify dhcp_thread module with wired network adapter
    def test_12_31_dhcp_thread_with_wired_adapter(self):
        result = carelink_unit_test.network.dhcp_thread(NetworkAdapters.WIRED)
        assert result == True
        pass

    # Verify set_ip_address module for adapter type NetworkAdapters.WIRED on DHCP enable state
    def test_12_32_set_ip_address_with_wired_adapter_valid_ip_address_on_dhcp_enabled(self):
        carelink_unit_test.network.set_dhcp_enabled(NetworkAdapters.WIRED, True)
        result = carelink_unit_test.network.set_ip_address(NetworkAdapters.WIRED, IP_ADDRESS)
        assert result == False
    
    # Verify set_ip_address module for adapter type NetworkAdapters.WIRELESS on DHCP enable state
    def test_12_33_set_ip_address_with_wireless_adapter_valid_ip_address_on_dhcp_enabled(self):
        carelink_unit_test.network.set_dhcp_enabled(NetworkAdapters.WIRELESS, True)
        result = carelink_unit_test.network.set_ip_address(NetworkAdapters.WIRELESS,IP_ADDRESS)
        assert result == False

    # Verify get_ip_address module for adapter type NetworkAdapters.WIRED on DHCP enable state
    def test_12_34_get_ip_address_with_wired_adapter_on_dhcp_enabled(self):
        result = carelink_unit_test.network.get_ip_address(NetworkAdapters.WIRED)
        assert result != "0.0.0.0" 
    
    # Verify get_ip_address module for adapter type NetworkAdapters.WIRELESS on DHCP enable state
    def test_12_35_get_ip_address_with_wireless_adapter_on_dhcp_enabled(self):
        result = carelink_unit_test.network.get_ip_address(NetworkAdapters.WIRELESS)
        assert result != "0.0.0.0" 

    # Verify set_subnet_mask module for adapter type NetworkAdapters.WIRED on DHCP enable state
    def test_12_36_set_subnet_mask_with_wired_adapter_valid_subnet_mask_on_dhcp_enabled(self):
        result = carelink_unit_test.network.set_subnet_mask(NetworkAdapters.WIRED,SUBNET_MASK)
        assert result == False

    # Verify set_subnet_mask module for adapter type NetworkAdapters.WIRELESS on DHCP enable state
    def test_12_37_set_subnet_mask_with_wireless_adapter_valid_subnet_mask_on_dhcp_enabled(self):
        result = carelink_unit_test.network.set_subnet_mask(NetworkAdapters.WIRELESS,SUBNET_MASK)
        assert result == False

    # Verify get_subnet_mask module for adapter type NetworkAdapters.WIRED on DHCP enable state
    def test_12_38_get_subnet_mask_with_wired_adapter_on_dhcp_enabled(self):
        result = carelink_unit_test.network.get_subnet_mask(NetworkAdapters.WIRED)
        assert result != "0.0.0.0" 

    # Verify get_subnet_mask module for adapter type NetworkAdapters.WIRELESS on DHCP enable state
    def test_12_39_get_subnet_mask_with_wireless_adapter_on_dhcp_enabled(self):
        result = carelink_unit_test.network.get_subnet_mask(NetworkAdapters.WIRELESS)
        assert result != "0.0.0.0" 

    # Verify set_gateway_address module for adapter type NetworkAdapters.WIRED On DHCP enabled state 
    def test_12_40_set_gateway_address_with_wired_adapter_valid_gateway_address_on_dhcp_enabled(self):
        result = carelink_unit_test.network.set_gateway_address(NetworkAdapters.WIRED,GATEWAY)
        assert result == False
    
    # Verify set_gateway_address module for adapter type NetworkAdapters.WIRELESS  On DHCP enabled state 
    def test_12_41_set_gateway_address_with_wireless_adapter_valid_gateway_address_on_dhcp_enabled(self):
        result = carelink_unit_test.network.set_gateway_address(NetworkAdapters.WIRELESS,GATEWAY)
        assert result == False

    # Verify get_gateway_address module for adapter type NetworkAdapters.WIRED on DHCP enable state
    def test_12_42_get_gateway_address_with_wired_adapter_on_dhcp_enabled(self):
        result = carelink_unit_test.network.get_gateway_address(NetworkAdapters.WIRED)
        assert result != "0.0.0.0" 

    # Verify get_gateway_address module for adapter type NetworkAdapters.WIRELESS on DHCP enable state
    def test_12_43_get_gateway_address_with_wireless_adapter_on_dhcp_enabled(self):
        result = carelink_unit_test.network.get_gateway_address(NetworkAdapters.WIRELESS)
        assert result != "0.0.0.0" 

    def test_12_44_import_network_configs_with_config_none(self):
        result = carelink_unit_test.network.import_network_configs(None)
        assert result == Strings.NET_SET_IMPORT_FAILED
    
    def test_12_45_import_network_configs_with_empty_config(self):
        result = carelink_unit_test.network.import_network_configs("")
        assert result == Strings.NET_SET_IMPORT_FAILED
    
    def test_12_46_import_network_configs_with_invalid_config(self):
        config = configparser.ConfigParser()
        config['test'] = {}
        result = carelink_unit_test.network.import_network_configs(config)
        assert result == Strings.NET_SET_IMPORT_FAILED

    def test_12_47_import_network_configs_with_wiredconfig_no_dhcp_field(self):
        config = configparser.ConfigParser()
        config['WiredConfig'] = {
            'IP':'192.168.7.158',
            'SubNetmask':'255.255.255.0',
            'Gateway':'192.168.7.1',
            'PrimaryDNS':'192.168.6.1',
            'SecondaryDNS':'192.168.6.4'
        }
        result = carelink_unit_test.network.import_network_configs(config)
        assert result == Strings.NET_SET_EMPTY
    
    def test_12_48_import_network_configs_with_wiredconfig_invalid_field(self):
        config = configparser.ConfigParser()
        config['WiredConfig'] = {
            'DHCPEnable':'0',
            'IPfhf':'192.168.7.158',
            'SubNetmask':'255.255.255.0',
            'Gateway':'192.168.7.1',
            'PrimaryDNS':'192.168.6.1',
            'SecondaryDNS':'192.168.6.4'
        }
        result = carelink_unit_test.network.import_network_configs(config)
        assert result == Strings.NET_SET_EMPTY
    
    def test_12_49_import_network_configs_with_wiredconfig_no_ip_field(self):
        config = configparser.ConfigParser()
        config['WiredConfig'] = {
            'DHCPEnable':'0',
            'SubNetmask':'255.255.255.0',
            'Gateway':'192.168.7.1',
            'PrimaryDNS':'192.168.6.1',
            'SecondaryDNS':'192.168.6.4'
        }
        result = carelink_unit_test.network.import_network_configs(config)
        assert result == Strings.NET_SET_EMPTY
    
    def test_12_50_import_network_configs_with_wiredconfig_no_netmask_field(self):
        config = configparser.ConfigParser()
        config['WiredConfig'] = {
            'DHCPEnable':'0',
            'IP':'192.168.7.158',
            'Gateway':'192.168.7.1',
            'PrimaryDNS':'192.168.6.1',
            'SecondaryDNS':'192.168.6.4'
        }
        result = carelink_unit_test.network.import_network_configs(config)
        assert result == Strings.NET_SET_EMPTY

    def test_12_51_import_network_configs_with_wiredconfig_no_gateway_filed(self):
        config = configparser.ConfigParser()
        config['WiredConfig'] = {
            'DHCPEnable':'0',
            'IP':'192.168.7.158',
            'SubNetmask':'255.255.255.0',
            'PrimaryDNS':'192.168.6.1',
            'SecondaryDNS':'192.168.6.4'
        }
        result = carelink_unit_test.network.import_network_configs(config)
        assert result == Strings.NET_SET_EMPTY

    def test_12_52_import_network_configs_with_wiredconfig_no_pri_dns_field(self):
        config = configparser.ConfigParser()
        config['WiredConfig'] = {
            'DHCPEnable':'0',
            'IP':'192.168.7.158',
            'SubNetmask':'255.255.255.0',
            'Gateway':'192.168.7.1',
            'SecondaryDNS':'192.168.6.4'
        }
        result = carelink_unit_test.network.import_network_configs(config)
        assert result == Strings.NET_SET_EMPTY
    
    def test_12_53_import_network_configs_with_wiredconfig_no_sec_dns_field(self):
        config = configparser.ConfigParser()
        config['WiredConfig'] = {
            'DHCPEnable':'0',
            'IP':'192.168.7.158',
            'SubNetmask':'255.255.255.0',
            'Gateway':'192.168.7.1',
            'PrimaryDNS':'192.168.6.1',
        }
        result = carelink_unit_test.network.import_network_configs(config)
        assert result == Strings.NET_SET_EMPTY
    
    def test_12_54_import_network_configs_with_wiredconfig_all_fields(self):
        config = configparser.ConfigParser()
        config['WiredConfig'] = {
            'DHCPEnable':'0',
            'IP':'192.168.7.158',
            'SubNetmask':'255.255.255.0',
            'Gateway':'192.168.7.1',
            'PrimaryDNS':'192.168.6.1',
            'SecondaryDNS':'192.168.6.4'
        }
        result = carelink_unit_test.network.import_network_configs(config)
        assert result == Strings.NET_SET_IMPORTED
    
    def test_12_55_import_network_configs_with_wirelessconfig_invalid_field(self):
        config = configparser.ConfigParser()
        config['WirelessConfig'] = {
            'DHCPEnable':'0',
            'IP212':'192.168.7.158',
            'SubNetmask':'255.255.255.0',
            'Gateway':'192.168.7.1',
            'PrimaryDNS':'192.168.6.1',
            'SecondaryDNS':'192.168.6.4'
        }
        result = carelink_unit_test.network.import_network_configs(config)
        assert result == Strings.NET_SET_EMPTY
    
    def test_12_56_import_network_configs_with_wirelessconfig_no_dhcp_field(self):
        config = configparser.ConfigParser()
        config['WirelessConfig'] = {
            'IP':'192.168.7.158',
            'SubNetmask':'255.255.255.0',
            'Gateway':'192.168.7.1',
            'PrimaryDNS':'192.168.6.1',
            'SecondaryDNS':'192.168.6.4'
        }
        result = carelink_unit_test.network.import_network_configs(config)
        assert result == Strings.NET_SET_EMPTY

    def test_12_57_import_network_configs_with_wirelessconfig_no_ip_field(self):
        config = configparser.ConfigParser()
        config['WirelessConfig'] = {
            'DHCPEnable':'0',
            'SubNetmask':'255.255.255.0',
            'Gateway':'192.168.7.1',
            'PrimaryDNS':'192.168.6.1',
            'SecondaryDNS':'192.168.6.4'
        }
        result = carelink_unit_test.network.import_network_configs(config)
        assert result == Strings.NET_SET_EMPTY
    
    def test_12_58_import_network_configs_with_wirelessconfig_no_netmask_field(self):
        config = configparser.ConfigParser()
        config['WirelessConfig'] = {
            'DHCPEnable':'0',
            'IP':'192.168.7.158',
            'Gateway':'192.168.7.1',
            'PrimaryDNS':'192.168.6.1',
            'SecondaryDNS':'192.168.6.4'
        }
        result = carelink_unit_test.network.import_network_configs(config)
        assert result == Strings.NET_SET_EMPTY

    def test_12_59_import_network_configs_with_wirelessconfig_no_gateway_filed(self):
        config = configparser.ConfigParser()
        config['WirelessConfig'] = {
            'DHCPEnable':'0',
            'IP':'192.168.7.158',
            'SubNetmask':'255.255.255.0',
            'PrimaryDNS':'192.168.6.1',
            'SecondaryDNS':'192.168.6.4'
        }
        result = carelink_unit_test.network.import_network_configs(config)
        assert result == Strings.NET_SET_EMPTY

    def test_12_60_import_network_configs_with_wirelessconfig_no__pri_dns_field(self):
        config = configparser.ConfigParser()
        config['WirelessConfig'] = {
            'DHCPEnable':'0',
            'IP':'192.168.7.158',
            'SubNetmask':'255.255.255.0',
            'Gateway':'192.168.7.1',
            'SecondaryDNS':'192.168.6.4'
        }
        result = carelink_unit_test.network.import_network_configs(config)
        assert result == Strings.NET_SET_EMPTY
    
    def test_12_61_import_network_configs_with_wirelessconfig_no_sec_dns_field(self):
        config = configparser.ConfigParser()
        config['WirelessConfig'] = {
            'DHCPEnable':'0',
            'IP':'192.168.7.158',
            'SubNetmask':'255.255.255.0',
            'Gateway':'192.168.7.1',
            'PrimaryDNS':'192.168.6.1',
        }
        result = carelink_unit_test.network.import_network_configs(config)
        assert result == Strings.NET_SET_EMPTY
    
    def test_12_62_import_network_configs_with_wirelessconfig_all_fields(self):
        config = configparser.ConfigParser()
        config['WirelessConfig'] = {
            'DHCPEnable':'0',
            'IP':'192.168.7.158',
            'SubNetmask':'255.255.255.0',
            'Gateway':'192.168.7.1',
            'PrimaryDNS':'192.168.6.1',
            'SecondaryDNS':'192.168.6.4'
        }
        result = carelink_unit_test.network.import_network_configs(config)
        assert result == Strings.NET_SET_IMPORTED
    
    def test_12_63_import_network_configs_with_wired_dhcp(self):
        config = configparser.ConfigParser()
        config['WiredConfig'] = {
            'DHCPEnable':'1'
        }
        result = carelink_unit_test.network.import_network_configs(config)
        assert result == Strings.NET_SET_IMPORTED
    
    def test_12_64_import_network_configs_with_wirelessconfig_dhcp(self):
        config = configparser.ConfigParser()
        config['WirelessConfig'] = {
            'DHCPEnable':'1'
        }
        result = carelink_unit_test.network.import_network_configs(config)
        assert result == Strings.NET_SET_IMPORTED