from helper_file import Unit_test_object
carelink_unit_test = Unit_test_object()

class TestToolsLiftCalibration():
        """TestToolsLiftCalibration  
        
        Description:This class contains the Unittest cases functions for lift calibration module.      
        
        """

        # verify onLiftSetParameters with no input
        def test_17_01_on_lift_set_parameters_input_none(self):
            result = carelink_unit_test.tools_lift_calibration.onLiftSetParameters(None)
            assert result == False