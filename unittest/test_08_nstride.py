from helper_file import Unit_test_object
carelink_unit_test = Unit_test_object()
from utilities.common_enums import CanResponseType
class TestNStride():
     
    # verify nstride processing response with locking caster unsolicited state
    def test_8_01_nstride_processing_response_with_locking_caster_unsolicited_state(self):
        carelink_unit_test.nstride.processing_response_message(CanResponseType.LOCKING_CASTER_UNSOLICITED_STATE_RESPONSE,10,[1,0,0,0,0,0,0,0,0,0])
        result =carelink_unit_test.nstride._nstride_locked 
        assert result== True
        
    # verify nstride processing response with nstride status response    
    def test_8_02_nstride_processing_response_with_nstride_status_response(self):
        carelink_unit_test.nstride.processing_response_message(CanResponseType.NSTRIDE_STATUS_RESPONSE,11,[0,1,0,0,0,0,0,0,0,0])
        result = carelink_unit_test.nstride._nstride_locked  
        assert result== True 
    
    # verify nstride processing response with locking castor error response    
    def test_8_03_nstride_processing_response_with_locking_caster_error_response(self):
        carelink_unit_test.nstride.processing_response_message(CanResponseType.LOCKING_CASTER_ERROR_RESPONSE,10,[0,0,0,0,0,0,0,0,0,0])
        result = carelink_unit_test.nstride._nstride_locked  
        assert result== False          
        
    # verify nstride available local variable when nstride device presence with true input     
    def test_8_04_nstride_device_presence(self):
        carelink_unit_test.nstride.nstride_device_presence(True)
        assert carelink_unit_test.nstride.nstride_available == True
    
    # verify nstride available local variable when nstride device presence with false input    
    def test_8_05_nstride_device_presence(self):
        carelink_unit_test.nstride.nstride_device_presence(False)
        assert carelink_unit_test.nstride.nstride_available == False           