from helper_file import Unit_test_object
carelink_unit_test = Unit_test_object()
from utilities.common_configs import LoginOptionsKey, LoginOptionsValue, LockOptionsKey, LockOptionsValue

class TestSettings():
        """TestSettings  
        
        Description:This class contains the Unittest cases functions for Settings module.      
        
        """

        # verify init_settings
        def test_15_01_init_settings(self):
            result = carelink_unit_test.settings_presenter.init_settings()
            assert result == True

        # verify change_display_brightness by incrementing when brightness value is 20
        def test_15_02_on_change_display_brightness_increment(self):
            carelink_unit_test.display_brightness.set_display_brightness(20)
            carelink_unit_test.settings_presenter.OnChangeDisplayBrightness(True)
            assert carelink_unit_test.display_brightness.get_display_brightness() == 30

        # verify change_display_brightness by incrementing when brightness value is 100
        def test_15_03_on_change_display_brightness_increment_max(self):
            carelink_unit_test.display_brightness.set_display_brightness(100)
            carelink_unit_test.settings_presenter.OnChangeDisplayBrightness(True)
            assert carelink_unit_test.display_brightness.get_display_brightness() == 100
        
        # verify change_display_brightness by decrementing when brightness value is 50
        def test_15_04_on_change_display_brightness_decrement(self):
            carelink_unit_test.display_brightness.set_display_brightness(50)
            carelink_unit_test.settings_presenter.OnChangeDisplayBrightness(False)
            assert carelink_unit_test.display_brightness.get_display_brightness() == 40

        # verify change_display_brightness by decrementing when brightness value is 10
        def test_15_05_on_change_display_brightness_decrement_min(self):
            carelink_unit_test.display_brightness.set_display_brightness(10)
            carelink_unit_test.settings_presenter.OnChangeDisplayBrightness(False)
            assert carelink_unit_test.display_brightness.get_display_brightness() == 10

        # verify change_login_timeout by incrementing the timeout value
        def test_15_06_on_change_login_timeout_increment(self):
            carelink_unit_test.settings_presenter._max_login_timeout = LoginOptionsKey[6]
            carelink_unit_test.settings_presenter._current_login_timeout = LoginOptionsKey[3]
            carelink_unit_test.settings_presenter.onChangeLoginTimeout(True)
            assert carelink_unit_test.settings_presenter._current_login_timeout == LoginOptionsKey[4]

        # verify change_login_timeout by incrementing the timeout value even after the maximum value is reached
        def test_15_07_on_change_login_timeout_increment_max(self):
            carelink_unit_test.settings_presenter._max_login_timeout = LoginOptionsKey[6]
            carelink_unit_test.settings_presenter._current_login_timeout = LoginOptionsKey[6]
            carelink_unit_test.settings_presenter.onChangeLoginTimeout(True)
            assert carelink_unit_test.settings_presenter._current_login_timeout == LoginOptionsKey[6]

        # verify change_login_timeout by decrementing the timeout value
        def test_15_08_on_change_login_timeout_decrement(self):
            carelink_unit_test.settings_presenter._max_login_timeout = LoginOptionsKey[6]
            carelink_unit_test.settings_presenter._current_login_timeout = LoginOptionsKey[6]
            carelink_unit_test.settings_presenter.onChangeLoginTimeout(False)
            assert carelink_unit_test.settings_presenter._current_login_timeout == LoginOptionsKey[5]

        # verify change_login_timeout by decrementing the timeout value even after the minimum value is reached
        def test_15_09_on_change_login_timeout_decrement_min(self):
            carelink_unit_test.settings_presenter._max_login_timeout = LoginOptionsKey[6]
            carelink_unit_test.settings_presenter._current_login_timeout = LoginOptionsKey[0]
            carelink_unit_test.settings_presenter.onChangeLoginTimeout(False)
            assert carelink_unit_test.settings_presenter._current_login_timeout == LoginOptionsKey[0]

        # verify change_lock_timeout by incrementing the timeout value
        def test_15_10_on_change_lock_timeout_increment(self):
            carelink_unit_test.settings_presenter._max_lock_timeout = LockOptionsKey[6]
            carelink_unit_test.settings_presenter._current_lock_timeout = LockOptionsKey[3]
            carelink_unit_test.settings_presenter.onChangeLockTimeout(True)
            assert carelink_unit_test.settings_presenter._current_lock_timeout == LockOptionsKey[4]

        # verify change_lock_timeout by incrementing the timeout value even after the maximum value is reached
        def test_15_11_on_change_lock_timeout_increment_max(self):
            carelink_unit_test.settings_presenter._max_lock_timeout = LockOptionsKey[6]
            carelink_unit_test.settings_presenter._current_lock_timeout = LockOptionsKey[6]
            carelink_unit_test.settings_presenter.onChangeLockTimeout(True)
            assert carelink_unit_test.settings_presenter._current_lock_timeout == LockOptionsKey[6]

        # verify change_lock_timeout by decrementing the timeout value
        def test_15_12_on_change_lock_timeout_decrement(self):
            carelink_unit_test.settings_presenter._max_lock_timeout = LockOptionsKey[6]
            carelink_unit_test.settings_presenter._current_lock_timeout = LockOptionsKey[6]
            carelink_unit_test.settings_presenter.onChangeLockTimeout(False)
            assert carelink_unit_test.settings_presenter._current_lock_timeout == LockOptionsKey[5]

        # verify change_lock_timeout by decrementing the timeout value even after the minimum value is reached
        def test_15_13_on_change_lock_timeout_decrement_min(self):
            carelink_unit_test.settings_presenter._max_lock_timeout = LockOptionsKey[6]
            carelink_unit_test.settings_presenter._current_lock_timeout = LockOptionsKey[0]
            carelink_unit_test.settings_presenter.onChangeLockTimeout(False)
            assert carelink_unit_test.settings_presenter._current_lock_timeout == LockOptionsKey[0]

        # Verify toggle_mute with mute enabled
        def test_15_14_on_toggle_mute_muted(self):
            carelink_unit_test.settings_presenter.onToggleMute(True)
            assert carelink_unit_test.volume.get_is_mute() == True

        # Verify toggle_mute with mute disabled
        def test_15_15_on_toggle_mute_muted(self):
            carelink_unit_test.settings_presenter.onToggleMute(False)
            assert carelink_unit_test.volume.get_is_mute() == False

        # verify volume_change with volume value 10
        def test_15_16_on_volume_change(self):
            carelink_unit_test.settings_presenter.onVolumeChange(10)
            assert carelink_unit_test.volume.get_speaker_volume() == 10
            assert carelink_unit_test.volume.get_is_mute() == False

        # verify volume_change with volume value 1000
        def test_15_17_on_volume_change_value_1000(self):
            carelink_unit_test.volume.set_speaker_volume(20)
            carelink_unit_test.settings_presenter.onVolumeChange(1000)
            assert carelink_unit_test.volume.get_speaker_volume() == 20
            assert carelink_unit_test.volume.get_is_mute() == False

        # verify volume_change with volume value -10
        def test_15_18_on_volume_change_value_negative(self):
            carelink_unit_test.volume.set_speaker_volume(20)
            carelink_unit_test.settings_presenter.onVolumeChange(-10)
            assert carelink_unit_test.volume.get_speaker_volume() == 20
            assert carelink_unit_test.volume.get_is_mute() == False

        # verify volume_change with volume value 100
        def test_15_19_on_volume_change_max(self):
            carelink_unit_test.settings_presenter.onVolumeChange(100)
            assert carelink_unit_test.volume.get_speaker_volume() == 100
            assert carelink_unit_test.volume.get_is_mute() == False
            
        # verify volume_change with volume value 0
        def test_15_20_on_volume_change_min(self):
            carelink_unit_test.settings_presenter.onVolumeChange(0)
            assert carelink_unit_test.volume.get_speaker_volume() == 0
            assert carelink_unit_test.volume.get_is_mute() == True

        # verify save_settings without user logged in
        def test_15_21_save_settings(self):
            result = carelink_unit_test.settings_presenter.save_settings(0)
            assert result == True

        # verify save_settings with user logged in
        def test_15_22_save_settings(self):
            result = carelink_unit_test.settings_presenter.save_settings(1)
            assert result == True