import sys 
import os
import shutil
sys.path.append('..')
sys.path.append('.')
from diagnostics.logging.logger import Log
from utilities.common_enums import LogLabels, LogLevels
from utilities.common_configs import log_file_path, log_file_major_name,    \
    log_file_name, serial_port

log_object = Log()

log_file = log_file_path + log_file_name
print(log_file)

#Get the number of lines from the file
def line_count(log_file_name):
    num_lines = 0
    with open(log_file_name, 'r') as f:
        for line in f:
            num_lines += 1

    print(num_lines)
    return num_lines

class TestLogger():
    """TestLogger
    
    Description : This class contains the unittest case functions for the logger module.
    """

    # Validate the file write when usb is not available
    def test_4_01_file_write_log_folder_not_available(self):
        result = log_object.write_to_file("Test log\n")
        assert result == False

    # Validate close the log file when its not open
    def test_4_02_file_close_without_open(self):
        result = log_object.close_log_file()
        assert result == False

    # Validate write the log file when its not open
    def test_4_03_file_write_without_open(self):
        result = log_object.write_to_file("Test log when file write with out open\n")
        assert result == False
        
    # Validate to check usb folder is exists
    def test_4_04_log_folder_exists(self):
        log_object.open_log_file()
        result = log_object.check_file_path()
        assert result == True

    # Validate opne the log file
    def test_4_05_file_open(self):#Get the number of lines from the file
        result = log_object.open_log_file()
        assert result == True

    # Validate to write the log file
    def test_4_06_file_write(self):
        result = log_object.write_to_file("Test log\n")
        assert result == True

    # Validate the file write append
    def test_4_07_file_write_append(self):
        result_before = line_count(log_file)
        log_object.write_to_file("Test log for append\n")
        log_object.write_to_file("Test log for append\n")
        log_object.write_to_file("Test log for append\n")
        log_object.write_to_file("Test log for append\n")
        result_after = line_count(log_file)
        assert result_before != result_after

    # Validate close the log file
    def test_4_08_file_close(self):
        result = log_object.close_log_file()
        assert result == True        

    def test_4_09_write_file_after_close(self):
        result = log_object.write_to_file("Test log\n")            
        assert result == False

    # Validate write the log into uart when its not opened
    def test_4_10_write_uart_without_open(self):
        result = log_object.write_to_serial("log without open uart\n")
        assert result == False

    # Validate open uart
    def test_4_11_open_uart(self):
        result = log_object.open_serial_port()
        assert result == True

    # Validate write the log to uart
    def test_4_12_write_uart(self):
        result = log_object.write_to_serial("Write Serial log\n")
        assert result == True

    # Validate close uart
    def test_4_13_close_uart(self):
        result = log_object.close_serial_port()
        assert result == True

    # Validate write the log after close the uart
    def test_4_14_write_uart_after_close(self):
        result = log_object.write_to_serial("Serial log without open\n")        
        assert result == False
            
    # Write log file when log label is INFO log level is NOLOG    
    def test_4_15_write_logfile_log_label_info_log_level_nolog(self):
        log_object.open_log_file()
        log_object.log_level = LogLevels.NOLOG
        result = log_object.write_log("Test log No Log",LogLabels.INFO)
        assert result == False
      
    # Write log file when log label is Exception log level is NOLOG   
    def test_4_16_write_logfile_log_label_exception_log_level_nolog(self):
        log_object.log_level = LogLevels.NOLOG
        result = log_object.write_log("Test log No Log",LogLabels.EXCEPTION)
        assert result == False
    
    # Write log file when log label is Exception log level is NOLOG   
    def test_4_17_write_logfile_log_label_error_log_level_nolog(self):
        log_object.log_level = LogLevels.NOLOG 
        result = log_object.write_log("Test log No Log",LogLabels.ERROR)
        assert result == False
    
    # Write log file when log label is info log level is informational 
    def test_4_18_write_logfile_log_label_info_log_level_informational(self):          
        log_object.log_level = LogLevels.INFORMATIONAL 
        result_before = line_count(log_file)
        result = log_object.write_log("Test log information",LogLabels.INFO)
        assert result == True
        if result:
            result_after = line_count(log_file)
            assert result_before != result_after
    
    #Write log file when log label is exception log level is informational 
    def test_4_19_write_logfile_log_label_exception_log_level_informational(self):
        log_object.log_level = LogLevels.INFORMATIONAL
        result = log_object.write_log("Test log No Log",LogLabels.EXCEPTION)
        assert result == False
        
    # Write log file when log label is error log level is informational 
    def test_4_20_write_logfile_log_label_error_log_level_informational(self):
        log_object.log_level = LogLevels.INFORMATIONAL
        result = log_object.write_log("Test log No Log",LogLabels.ERROR)
        assert result == False
        
    # Write log file when log label is info log level is error_exception    
    def test_4_21_write_logfile_log_label_info_log_level_error_exception(self):
        log_object.log_level = LogLevels.ERROR_EXCEPTION
        result = log_object.write_log("Test log No Log",LogLabels.INFO)
        assert result == False
    
    # Write log file when log label is exception log level is error_exception  
    def test_4_22_write_logfile_log_label_exception_log_level_error_exception(self):
        log_object.log_level = LogLevels.ERROR_EXCEPTION 
        result_before = line_count(log_file)
        result = log_object.write_log("Test log Exception",LogLabels.EXCEPTION)
        assert result == True
        if result:
            result_after = line_count(log_file)
            assert result_before != result_after
    
    # Write log file when log label is error log level is error_exception  
    def test_4_23_write_logfile_log_label_error_log_level_error_exception(self):
        log_object.log_level = LogLevels.ERROR_EXCEPTION
        result_before = line_count(log_file)
        result = log_object.write_log("Test log Error",LogLabels.ERROR)
        assert result == True
        if result:
            result_after = line_count(log_file)
            assert result_before != result_after
    
    # Write log file when log label is info log level is verbose  
    def test_4_24_write_logfile_log_label_info_log_level_verbose(self):
        log_object.log_level = LogLevels.VERBOSE 
        result_before = line_count(log_file)
        result = log_object.write_log("Test log verbose",LogLabels.INFO)
        assert result == True
        if result:
            result_after = line_count(log_file)
            assert result_before != result_after
    
    # Write log file when log label is exception log level is verbose  
    def test_4_25_write_logfile_log_label_exception_log_level_verbose(self):
        log_object.log_level = LogLevels.VERBOSE 
        result_before = line_count(log_file)
        result = log_object.write_log("Test log verbose",LogLabels.EXCEPTION)
        assert result == True
        if result:
            result_after = line_count(log_file)
            assert result_before != result_after
    
    # Write log file when log label is error log level is verbose  
    def test_4_26_write_logfile_log_label_error_log_level_verbose(self):
        log_object.log_level = LogLevels.VERBOSE
        result_before = line_count(log_file)
        result = log_object.write_log("Test log verbose",LogLabels.ERROR)
        assert result == True
        if result:
            result_after = line_count(log_file)
            assert result_before != result_after
    
    # Write log file when number of log files is 6 and logfile size is 9 bytes
    def test_4_27_verify_number_of_log_files(self): 
        log_object.log_file_size = 9
        log_object.no_of_log_files = 6
        assert log_object.check_file_size() == True
        log_object.write_log("Test Log 1")
        log_object.write_log("Test Log 2")
        log_object.write_log("Test Log 3")
        log_object.write_log("Test Log 4")
        log_object.write_log("Test Log 5")
        log_object.write_log("Test Log 6")
        log_object.write_log("Test Log 7")
        log_object.write_log("Test Log 8")
        log_object.write_log("Test Log 9")
        assert len(os.listdir(log_file_path)) == 6
        
    # Write log file when number of log files is 6 and logfile size is 9 bytes
    def test_4_28_verify_log_file_size(self):
        statinfo = os.stat(log_file)
        assert statinfo.st_size <= 60
        
        
    # Write logfile when number of log files is 0 and logfile size is 0
    def test_4_29_verify_file_size_zero_and_number_of_log_files_zero(self):
        log_object.log_file_size = 0
        log_object.no_of_log_files = 0
        result = log_object.write_log("Test log")
        shutil.rmtree(log_file_path)
        #Need to check the number of log files in the LOgs dir after the use case confirmation
        assert result == False        
